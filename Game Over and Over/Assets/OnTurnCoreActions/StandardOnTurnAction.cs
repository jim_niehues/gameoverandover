﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StandardOnTurnAction : IFunc<OnTurnArgs,IEnumerator> {
	public List<ICombatAbility> abilities = new List<ICombatAbility>();

	public IEnumerator Invoke(OnTurnArgs args){
		OnUseArgs onUseArgs = new OnUseArgs(args);
		System.Random rand = new System.Random(args.seed);
		ICombatAbility[] abilitiesFiltered = abilities.Where( a=>a!=null&&a.CanBeUsedNow.IsSafeToInvoke && a.CanBeUsedNow.Invoke(onUseArgs) && a.OnUse.IsSafeToInvoke ).ToArray();
		if(abilitiesFiltered.Length == 0){
			Debug.LogWarning("no valid actions... waiting instead");
			args.combatant.Entity.next_action_time+=Tick.length;
		}
		IEnumerator onUse = abilitiesFiltered[rand.Next(abilitiesFiltered.Length)].OnUse.Invoke(onUseArgs);
		while(onUse.MoveNext()){
			yield return null;
		}

	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}

}
