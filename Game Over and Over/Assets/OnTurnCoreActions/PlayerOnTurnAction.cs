﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerOnTurnAction : IFunc<OnTurnArgs,IEnumerator> {
	public List<ICombatAbility> abilities = new List<ICombatAbility>();
	
	public IEnumerator Invoke(OnTurnArgs args){
		if(args.combatant != Save.LoadedSave.ActiveCharacter){
			if(!(args.combatant.Entity is Player)) throw new System.Exception("PlayerOnTurnAction cannot be used on npcs" );
			args.combatant.Entity.next_action_time+=Tick.length;
		}
		OnUseArgs onUseArgs = new OnUseArgs(args);
		Save.LoadedSave.ActiveCharacter.Entity.onUseArgs = onUseArgs;
		//System.Random rand = new System.Random(args.seed);
		ICombatAbility[] abilitiesFiltered = abilities.Where( a=>a!=null&&a.CanBeUsedNow.IsSafeToInvoke && a.CanBeUsedNow.Invoke(onUseArgs) && a.OnUse.IsSafeToInvoke ).ToArray();
		if(abilitiesFiltered.Length == 0){
			Debug.LogWarning("no valid actions... waiting instead");
			args.combatant.Entity.next_action_time+=Tick.length;
		}
		while(args.combatant.Entity.WarmUp==0 && args.combatant.Entity.CoolDown == 0){
			yield return null;
		}
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}
