public interface IAction
{
	void Invoke();
	bool IsSafeToInvoke{get;}
}
public interface IAction<TParam1>
{
	void Invoke(TParam1 param1);
	bool IsSafeToInvoke{get;}
}
public interface IAction<TParam1, TParam2>
{
	void Invoke(TParam1 param1, TParam2 param2);
	bool IsSafeToInvoke{get;}
}
public interface IAction<TParam1, TParam2, TParam3>
{
	void Invoke(TParam1 param1, TParam2 param2, TParam3 param3);
	bool IsSafeToInvoke{get;}
}
public interface IAction<TParam1, TParam2, TParam3, TParam4>
{
	void Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4);
	bool IsSafeToInvoke{get;}
}
