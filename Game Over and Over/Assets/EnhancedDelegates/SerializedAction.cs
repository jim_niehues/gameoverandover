using FullInspector.Modules.SerializableDelegates;
public class SerializedAction
	:
		BaseSerializedAction,
		IAction
{
	public void Invoke()
	{
		DoInvoke();
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedAction<TParam1>
	:
		BaseSerializedAction,
		IAction<TParam1>
{
	public void Invoke(TParam1 param1)
	{
		DoInvoke(param1);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedAction<TParam1, TParam2>
	:
		BaseSerializedAction,
		IAction<TParam1, TParam2>
{
	public void Invoke(TParam1 param1, TParam2 param2)
	{
		DoInvoke(param1, param2);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedAction<TParam1, TParam2, TParam3>
	:
		BaseSerializedAction,
		IAction<TParam1, TParam2, TParam3>
{
	public void Invoke(TParam1 param1, TParam2 param2, TParam3 param3)
	{
		DoInvoke(param1, param2, param3);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedAction<TParam1, TParam2, TParam3, TParam4>
	:
		BaseSerializedAction,
		IAction<TParam1, TParam2, TParam3, TParam4>
{
	public void Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4)
	{
		DoInvoke(param1, param2, param3, param4);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
