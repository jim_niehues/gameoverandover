using System;
public class ScriptedFunc<TResult>
	:
		ScriptedDelegate<Func<TResult>>,
		IFunc<TResult>
{
	public TResult Invoke()
	{
		return (TResult)Invoke();
	}
}
public class ScriptedFunc<TParam1, TResult>
	:
		ScriptedDelegate<Func<TParam1, TResult>>,
		IFunc<TParam1, TResult>
{
	public TResult Invoke(TParam1 param1)
	{
		return (TResult)Invoke(param1);
	}
}
public class ScriptedFunc<TParam1, TParam2, TResult>
	:
		ScriptedDelegate<Func<TParam1, TParam2, TResult>>,
		IFunc<TParam1, TParam2, TResult>
{
	public TResult Invoke(TParam1 param1, TParam2 param2)
	{
		return (TResult)Invoke(param1, param2);
	}
}
public class ScriptedFunc<TParam1, TParam2, TParam3, TResult>
	:
		ScriptedDelegate<Func<TParam1, TParam2, TParam3, TResult>>,
		IFunc<TParam1, TParam2, TParam3, TResult>
{
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3)
	{
		return (TResult)Invoke(param1, param2, param3);
	}
}
public class ScriptedFunc<TParam1, TParam2, TParam3, TParam4, TResult>
	:
		ScriptedDelegate<Func<TParam1, TParam2, TParam3, TParam4, TResult>>,
		IFunc<TParam1, TParam2, TParam3, TParam4, TResult>
{
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4)
	{
		return (TResult)Invoke(param1, param2, param3, param4);
	}
}
