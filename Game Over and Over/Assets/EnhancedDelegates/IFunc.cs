public interface IFunc<TResult>
{
	TResult Invoke();
	bool IsSafeToInvoke{get;}
}
public interface IFunc<TParam1, TResult>
{
	TResult Invoke(TParam1 param1);
	bool IsSafeToInvoke{get;}
}
public interface IFunc<TParam1, TParam2, TResult>
{
	TResult Invoke(TParam1 param1, TParam2 param2);
	bool IsSafeToInvoke{get;}
}
public interface IFunc<TParam1, TParam2, TParam3, TResult>
{
	TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3);
	bool IsSafeToInvoke{get;}
}
public interface IFunc<TParam1, TParam2, TParam3, TParam4, TResult>
{
	TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4);
	bool IsSafeToInvoke{get;}
}
