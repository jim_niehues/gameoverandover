using System;
using FullInspector;

public abstract class Delegate<T>{
	[UnityEngine.HideInInspector]
	public readonly T _delegate;
	public Delegate(T _delegate){
		this._delegate = _delegate;
	}
}

public class RawAction
	:
		Delegate<Action>,
		IAction
{
	public void Invoke()
	{
		_delegate.Invoke();
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawAction(Action action):base(action){}
	public static implicit operator RawAction(System.Action action){
		return new RawAction(action);
	}
}
public class RawAction<TParam1>
	:
		Delegate<Action<TParam1>>,
		IAction<TParam1>
{
	public void Invoke(TParam1 param1)
	{
		_delegate.Invoke(param1);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawAction(Action<TParam1> action):base(action){}

	public static implicit operator RawAction<TParam1>(System.Action<TParam1> action){
		return new RawAction<TParam1>(action);
	}
}

public class RawAction<TParam1,TParam2>
	:
		Delegate<Action<TParam1,TParam2>>,
		IAction<TParam1,TParam2>
{
	public void Invoke(TParam1 param1,TParam2 param2)
	{
		_delegate.Invoke(param1,param2);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawAction(Action<TParam1,TParam2> action):base(action){}

	public static implicit operator RawAction<TParam1,TParam2>(System.Action<TParam1,TParam2> action){
		return new RawAction<TParam1,TParam2>(action);
	}
}

public class RawAction<TParam1,TParam2,TParam3>
	:
		Delegate<Action<TParam1,TParam2,TParam3>>,
		IAction<TParam1,TParam2,TParam3>
{
	public void Invoke(TParam1 param1,TParam2 param2,TParam3 param3)
	{
		_delegate.Invoke(param1,param2,param3);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawAction(Action<TParam1,TParam2,TParam3> action):base(action){}

	public static implicit operator RawAction<TParam1,TParam2,TParam3>(System.Action<TParam1,TParam2,TParam3> action){
		return new RawAction<TParam1,TParam2,TParam3>(action);
	}
}

public class RawAction<TParam1,TParam2,TParam3,TParam4>
	:
		Delegate<Action<TParam1,TParam2,TParam3,TParam4>>,
		IAction<TParam1,TParam2,TParam3,TParam4>
{
	public void Invoke(TParam1 param1,TParam2 param2,TParam3 param3,TParam4 param4)
	{
		_delegate.Invoke(param1,param2,param3,param4);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawAction(Action<TParam1,TParam2,TParam3,TParam4> action):base(action){}

	public static implicit operator RawAction<TParam1,TParam2,TParam3,TParam4>(System.Action<TParam1,TParam2,TParam3,TParam4> action){
		return new RawAction<TParam1,TParam2,TParam3,TParam4>(action);
	}
}
