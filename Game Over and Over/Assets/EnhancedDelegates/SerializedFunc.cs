using FullInspector.Modules.SerializableDelegates;
public class SerializedFunc<TResult>
	:
		BaseSerializedAction,
		IFunc<TResult>
{
	public TResult Invoke()
	{
		return (TResult)DoInvoke();
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedFunc<TParam1, TResult>
	:
		BaseSerializedAction,
		IFunc<TParam1, TResult>
{
	public TResult Invoke(TParam1 param1)
	{
		return (TResult)DoInvoke(param1);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedFunc<TParam1, TParam2, TResult>
	:
		BaseSerializedAction,
		IFunc<TParam1, TParam2, TResult>
{
	public TResult Invoke(TParam1 param1, TParam2 param2)
	{
		return (TResult)DoInvoke(param1, param2);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedFunc<TParam1, TParam2, TParam3, TResult>
	:
		BaseSerializedAction,
		IFunc<TParam1, TParam2, TParam3, TResult>
{
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3)
	{
		return (TResult)DoInvoke(param1, param2, param3);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
public class SerializedFunc<TParam1, TParam2, TParam3, TParam4, TResult>
	:
		BaseSerializedAction,
		IFunc<TParam1, TParam2, TParam3, TParam4, TResult>
{
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4)
	{
		return (TResult)DoInvoke(param1, param2, param3, param4);
	}
	public bool IsSafeToInvoke
	{
		get{
			return CanInvoke;
		}
	}
}
