using System;
public class ScriptedAction
	:
		ScriptedDelegate<Action>,
		IAction
{
	public void Invoke()
	{
		Invoke();
	}
}
public class ScriptedAction<TParam1>
	:
		ScriptedDelegate<Action<TParam1>>,
		IAction<TParam1>
{
	public void Invoke(TParam1 param1)
	{
		Invoke(param1);
	}
}
public class ScriptedAction<TParam1, TParam2>
	:
		ScriptedDelegate<Action<TParam1, TParam2>>,
		IAction<TParam1, TParam2>
{
	public void Invoke(TParam1 param1, TParam2 param2)
	{
		Invoke(param1, param2);
	}
}
public class ScriptedAction<TParam1, TParam2, TParam3>
	:
		ScriptedDelegate<Action<TParam1, TParam2, TParam3>>,
		IAction<TParam1, TParam2, TParam3>
{
	public void Invoke(TParam1 param1, TParam2 param2, TParam3 param3)
	{
		Invoke(param1, param2, param3);
	}
}
public class ScriptedAction<TParam1, TParam2, TParam3, TParam4>
	:
		ScriptedDelegate<Action<TParam1, TParam2, TParam3, TParam4>>,
		IAction<TParam1, TParam2, TParam3, TParam4>
{
	public void Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4)
	{
		Invoke(param1, param2, param3, param4);
	}
}
