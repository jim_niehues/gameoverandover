using System;
using FullInspector;


public class RawFunc<TResult>
	:
		Delegate<Func<TResult>>,
		IFunc<TResult>
{
	public TResult Invoke()
	{
		return _delegate.Invoke();
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawFunc(Func<TResult> Func):base(Func){}
	public static implicit operator RawFunc<TResult>(System.Func<TResult> Func){
		return new RawFunc<TResult>(Func);
	}
}
public class RawFunc<TParam1,TResult>
	:
		Delegate<Func<TParam1,TResult>>,
		IFunc<TParam1,TResult>
{
	public TResult Invoke(TParam1 param1)
	{
		return _delegate.Invoke(param1);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawFunc(Func<TParam1,TResult> Func):base(Func){}

	public static implicit operator RawFunc<TParam1,TResult>(System.Func<TParam1,TResult> Func){
		return new RawFunc<TParam1,TResult>(Func);
	}
}

public class RawFunc<TParam1,TParam2,TResult>
	:
		Delegate<Func<TParam1,TParam2,TResult>>,
		IFunc<TParam1,TParam2,TResult>
{
	public TResult Invoke(TParam1 param1,TParam2 param2)
	{
		return _delegate.Invoke(param1,param2);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawFunc(Func<TParam1,TParam2,TResult> Func):base(Func){}

	public static implicit operator RawFunc<TParam1,TParam2,TResult>(System.Func<TParam1,TParam2,TResult> Func){
		return new RawFunc<TParam1,TParam2,TResult>(Func);
	}
}

public class RawFunc<TParam1,TParam2,TParam3,TResult>
	:
		Delegate<Func<TParam1,TParam2,TParam3,TResult>>,
		IFunc<TParam1,TParam2,TParam3,TResult>
{
	public TResult Invoke(TParam1 param1,TParam2 param2,TParam3 param3)
	{
		return _delegate.Invoke(param1,param2,param3);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawFunc(Func<TParam1,TParam2,TParam3,TResult> Func):base(Func){}

	public static implicit operator RawFunc<TParam1,TParam2,TParam3,TResult>(System.Func<TParam1,TParam2,TParam3,TResult> Func){
		return new RawFunc<TParam1,TParam2,TParam3,TResult>(Func);
	}
}

public class RawFunc<TParam1,TParam2,TParam3,TParam4,TResult>
	:
		Delegate<Func<TParam1,TParam2,TParam3,TParam4,TResult>>,
		IFunc<TParam1,TParam2,TParam3,TParam4,TResult>
{
	public TResult Invoke(TParam1 param1,TParam2 param2,TParam3 param3,TParam4 param4)
	{
		return _delegate.Invoke(param1,param2,param3,param4);
	}
	public bool IsSafeToInvoke
	{
		get{
			return _delegate!=null;
		}
	}
	public RawFunc(Func<TParam1,TParam2,TParam3,TParam4,TResult> Func):base(Func){}

	public static implicit operator RawFunc<TParam1,TParam2,TParam3,TParam4,TResult>(System.Func<TParam1,TParam2,TParam3,TParam4,TResult> Func){
		return new RawFunc<TParam1,TParam2,TParam3,TParam4,TResult>(Func);
	}
}
