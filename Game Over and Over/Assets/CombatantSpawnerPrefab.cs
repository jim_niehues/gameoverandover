using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using FullInspector;

public class CombatantSpawnerPrefab : SpawnerPrefab<Combatant>, ISpawns<ILocated>{
	ILocated ISpawns<ILocated>.Spawn(int seed){
		return ((ISpawns<Combatant>)this).Spawn (seed);
	}
	void ISpawns<ILocated>.Populate(ILocated spawn){
		((ISpawns<Combatant>)this).Populate ((Combatant)spawn);
	}

}
