
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

public class TilePrefab : BaseScriptableObject,ISpawns<ITile>,IMapDrawable{
	public Texture2D MapTexture;
	public GameObject prefab;

	public bool IsPassableByDefault;
	public IFunc<IsPassableArgs,bool?> IsPassableByTest;
	
	public PseudoEvent<OnEnterArgs> OnEnterEvent = new PseudoEvent<OnEnterArgs>();
	public PseudoEvent<OnStayArgs>  OnStayEvent  = new PseudoEvent<OnStayArgs>();
	public PseudoEvent<OnExitArgs>  OnExitEvent  = new PseudoEvent<OnExitArgs>();

	public virtual void IMapDraw(Rect rect){
		GUI.Label(rect,MapTexture);
	}

	public ITile Spawn(int seed){
		Debug.Log("tile spawn start");
		Tile spawn = new Tile();
		
		Debug.Log("tile spawn complete");
		return spawn;
	}
	public void Populate(ITile spawn){
		Debug.Log("tile populated");
		Tile tile = (Tile)spawn;
		tile.MapTexture = MapTexture;
		//spawn.Gameobjectreference = new GameObjectReference();
		tile.Gameobjectreference.prefab = prefab;
		
		tile.IsPassableByDefault = IsPassableByDefault;
		tile.IsPassableByTest = IsPassableByTest;
		tile.OnEnterEvent = new PseudoEvent<OnEnterArgs>(OnEnterEvent);
		tile.OnStayEvent = new PseudoEvent<OnStayArgs>(OnStayEvent);
		tile.OnExitEvent = new PseudoEvent<OnExitArgs>(OnExitEvent);

	}
}

//public class OpenSpace: Tile2{
//	public abstract void IMapDraw(Rect rect){
//		GUI.
//	}
//	public override Color? Color{
//		get{
//			return new Color(.5f,.5f,.5f);
//		}
//	}
//	
//	public override bool IsPassableBy(object o){
//		return true;
//	}
//}
//public class Wall: Tile2{
//	public override Color? Color{
//		get{
//			return new Color(1,1,1);
//		}
//	}
//	
//	public override bool IsPassableBy(object o){
//		return false;
//	}
//}
//public class Door: Tile2{
//	Location destination;
//	public override Color? Color{
//		get{
//			return new Color(0,1,0);
//		}
//	}
//	public Door(Location destination){
//		this.destination = destination;
//	}
//	public override bool IsPassableBy(object o){
//		return true;
//	}
//	public override void OnEnter(Guid id){
//		Save ls = Save.loadedSave;
//		//if(Destination.Tile is Door) Debug.LogError("Potential Door loop");
//		
//		/*ls.Add(
//				ls.CurrentT, 
//				new GameEvent.CharacterSetLocation(
//					characterIndex,
// 					ls.Characters[characterIndex].Location,
//					destination,
//					true
//					)
//			);*/
//	}
//}
//public class EnemyTile: Tile2{
//	public Action Reset;
//
//	EnemyInstance[] enemies;
//
//	public override Color? Color{
//		get{
//			return new Color(0,.5f,0);
//		}
//	}
//	public EnemyTile(){
//		/*Enemy[] enemies = new GreyRat[]{new GreyRat(),new GreyRat(),};
//		Reset = delegate() {
//			foreach(Enemy e in enemies) e.OnResetDelegate();
//			this.enemies = enemies;
//		};
//		Reset();
//		Save.loadedSave.OnReset += OnReset;
//		*/
//	}
//
//	void OnReset (object sender, EventArgs e)
//	{
//		Reset();
//	}
//	public override bool IsPassableBy(object o){
//		return true;
//	}
//	public override void OnEnter(Guid id){
//		Save ls = Save.loadedSave;
//		bool allDead = true;
//		foreach(EnemyInstance enemy in enemies){
//			if(!enemy.IsDead){
//				allDead = false;
//				break;
//			}
//		}
//
//		if(!allDead){
//		/*	ls.Add(
//				ls.CurrentT,
//			    new GameEvent.EnterCombatEvent(
//					characterIndex,
//					enemies,
//					true
//					)
//				);*/
//		}
//	}
//}
/*
public class CustomPassability{
	bool defaultPassability;
	object[] exceptions;

	public bool IsPassableBy(object o){
		return false;
	}
}
*/
