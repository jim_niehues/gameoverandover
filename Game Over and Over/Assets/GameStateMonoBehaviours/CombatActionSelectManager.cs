﻿using UnityEngine;
using System.Collections;

public class CombatActionSelectManager : MonoBehaviour {
	public static CombatActionSelectManager inst = null;
	public GameObject OptionPrefab;
	void Awake(){
		if(!(inst==null||inst==this)){
			Debug.LogWarning("CombatActionSelectManager already exists, Destroying new instance.");
			Destroy(this);
			return;
		}
		inst = this;
	}
	
	private bool allowInput;
	public bool AllowInput{
		get{
			return allowInput;
		}
		set{
			allowInput = value;
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
