﻿using UnityEngine;
using System.Collections;

public class CombatManager : MonoBehaviour {

	public static CombatManager inst = null;
	
	void Awake(){
		if(!(inst==null||inst==this)){
			Debug.LogWarning("CombatManager already exists, Destroying new instance.");
			Destroy(this);
			return;
		}
		inst = this;
		inst.gameObject.SetActive(false);
	}
	
	private bool allowInput;
	public bool AllowInput{
		get{
			return allowInput;
		}
		set{
			allowInput = value;
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
