﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MainMenuManager : MonoBehaviour {
	public static MainMenuManager inst = null;
	public Button NewAdventureButton;
	void Awake(){
		if(!(inst==null||inst==this)){
			Debug.LogWarning("MainMenuManager already exists, Destroying new instance.");
			Destroy(this);
			return;
		}
		inst = this;
		gameObject.SetActive(false);
		if(NewAdventureButton==null) throw new UnityException("MainMenuManager must have NewAdventureButton assigned");

	}
	public void Start(){
		NewAdventureButton.onClick.RemoveAllListeners();
		NewAdventureButton.onClick.AddListener(OnNewAdventureButtonClicked);
	}
	void OnNewAdventureButtonClicked(){
		GameState.MainMenu mainMenuGameState = Game.Inst.GameStateStack.Current as GameState.MainMenu;
		if(mainMenuGameState == null){

			Debug.LogWarning("Current GameState("+Game.Inst.GameStateStack.Current.GetType().ToString()+") is not GameState.MainMenu");
			return;
		}
		if(!mainMenuGameState.AllowsInput){
			Debug.LogWarning("Current GameState.MainMenu does not allow Input");
			return;
		}
		mainMenuGameState.LoadNewSave();
	}
	public bool IsVisible{
		get{
			return gameObject.activeSelf;
		}
		set{
			gameObject.SetActive(value);
		}
	}
	public bool AllowsInput{get;set;}


	
	// Update is called once per frame
	void Update () {
	
	}
}
