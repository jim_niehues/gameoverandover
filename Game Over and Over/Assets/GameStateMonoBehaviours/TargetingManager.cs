﻿using UnityEngine;
using System.Collections;

public class TargetingManager : MonoBehaviour {
	public static TargetingManager inst = null;

	public GameState.Combat.Targeting CurrentTargetingState{
		get{
			return Game.Inst.GameStateStack.Current as GameState.Combat.Targeting;
		}
	}
	void Awake(){
		if(!(inst==null||inst==this)){
			Debug.LogWarning("TargetingManager already exists, Destroying new instance.");
			Destroy(this);
			return;
		}
		inst = this;
	}
	
	private bool allowInput;
	public bool AllowInput{
		get{
			return allowInput;
		}
		set{
			allowInput = value;
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
