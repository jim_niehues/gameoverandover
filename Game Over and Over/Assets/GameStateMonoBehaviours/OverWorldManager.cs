﻿using UnityEngine;
using System.Collections;
using FullInspector;

public class OverWorldManager : BaseBehavior {
	public static OverWorldManager inst = null;
	
	protected override void Awake(){
		base.Awake();
		if(!(inst==null||inst==this)){
			Debug.LogWarning("OverWorldManager already exists, Destroying new instance.");
			Destroy(this);
			return;
		}
		inst = this;
		this.gameObject.SetActive(false);
	}
	[ShowInInspector]
	public static LevelMapAsset ActiveMapAsset{
		get{
			return LevelMap.ActiveMapAsset;
		}
	}
	public static LevelMap ActiveMap{
		get{
			return LevelMap.ActiveMap;
		}
		/*set{
			LevelMap.ActiveMap=value;
		}*/
	}
	public bool IsVisible{
		get{
			return gameObject.activeSelf;
		}
		set{
			gameObject.SetActive(value);
		}
	}
	private bool allowInput;
	[ShowInInspector]
	public bool AllowInput{
		get{
			return allowInput;
		}
		set{
			allowInput = value;
		}
	}



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(AllowInput){

			if(Save.LoadedSave.ActiveCharacter!=null && Save.LoadedSave.ActiveCharacter.Exists){
				if(Save.LoadedSave.ActiveCharacter.Entity.IsTurn(Save.LoadedSave.CurrentT)){
					if(Input.GetKeyDown(KeyCode.K)){
						TryOutOfCombatAction(OutOfCombatActions.Suicide);
						return;
					}






					float h = 0;//Input.GetAxis("Horizontal");
					float v = 0;//Input.GetAxis("Vertical");
					if(Input.GetKeyDown(KeyCode.A)) h--;
					if(Input.GetKeyDown(KeyCode.D)) h++;
					if(Input.GetKeyDown(KeyCode.W)) v++;
					if(Input.GetKeyDown(KeyCode.S)) v--;


					if(Mathf.Abs(h)>0.5f){
						if(h<0){
							if(TryOutOfCombatAction(OutOfCombatActions.MoveLeft))return;
							else Debug.Log("Can't move Left");
						}else{
							if(TryOutOfCombatAction(OutOfCombatActions.MoveRight))return;
							else Debug.Log("Can't move Right");
						}
					}
					if(Mathf.Abs(v)>0.5f){
						if(v<0){
							if(TryOutOfCombatAction(OutOfCombatActions.MoveDown))return;
							else Debug.Log("Can't move Down");
						}else{
							if(TryOutOfCombatAction(OutOfCombatActions.MoveUp))return;
							else Debug.Log("Can't move Up");
						}
					}
				}
				Location playerLocation =  Save.LoadedSave.ActiveCharacter.ToInstanceOf<ILocated>().GetLocation();
				Camera.main.transform.position = playerLocation.WorldPosition +new Vector3(0,0,Camera.main.transform.position.z);

			}
		}
	}
	bool TryOutOfCombatAction(OutOfCombatActions action){
		
		Instance<Player> character = Save.LoadedSave.ActiveCharacter;
		if(character.Entity == null) return false;
		/*for(int i=1;i<=Save.loadedSave.OnLifeNumber;i++){
			c = Save.loadedSave.characters[i];
				Debug.Log(c.location.coord.x+","+c.location.coord.y);
		}*/
		//if(location.room.CanPlayerMoveHere(IntCoord)
		IntCoord movement = new IntCoord(0,0);
		switch (action){
		case OutOfCombatActions.MoveLeft:
			movement.x=-1;
			break;
		case OutOfCombatActions.MoveRight:
			movement.x=1;
			break;
		case OutOfCombatActions.MoveUp:
			movement.y=-1;
			break;
		case OutOfCombatActions.MoveDown:
			movement.y=1;
			break;
		case OutOfCombatActions.Suicide:
			Save.LoadedSave.OnPlayerDeath();
			return true;
		}
		
		
		//Debug.Log("player attempting to move to ["+newCoord.x+","+newCoord.y+"]");
		Instance<ILocated> ILocatedCharacterInstance = character.ToInstanceOf<ILocated>();
		Location characterLocation = ILocatedCharacterInstance.GetLocation();
		//Debug.Log (characterLocation);
		if(characterLocation == Location.Null) throw new System.Exception("player location is null");
		Location destination = characterLocation+movement;
		//Debug.Log ("Moving "+movement.ToString()+" to "+destination.ToString());
		if(!destination.LevelTileExists)return false;
		if(destination.LevelTile.CanMoveHereFrom(new IsPassableArgs(destination,ILocatedCharacterInstance,characterLocation))){
			
			Save s =  Save.LoadedSave;
			int i = s.OnLifeNumber;
			
			/*s.AddAction(
					new CharacterWalk(
						Save.loadedSave.NextPlayerTurn,
						i,
						s.ActiveCharacter.Location,
						newCoord,
						true
					)
				);*/
			//Location newLocation = new Location(s.ActiveCharacter.Location.Dungeon,newCoord);
			s.Add(
				s.CurrentT,
				TurnStage.Turn,
				new GameEvent.CharacterWalk(
				characterLocation,
				movement,
				true
				)
				);
			Save.LoadedSave.NextPlayerTurn+=1;
			//character.Location.dungeon.isDirty = true;
			//Debug.Log("life "+i+" moved to ["+newCoord.x+","+newCoord.y+"]");
			return true;
		}
		return false;
	}
}
