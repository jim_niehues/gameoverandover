using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

public class Poolable : MonoBehaviour
{
	static GameObject _poolRoot;
	public static GameObject PoolRoot{
		get{
			if(_poolRoot==null){
				_poolRoot = new GameObject("PoolRoot");
			}
			return _poolRoot;
		}
	}

	private Guid UsageId;
	[SerializeField]
	Poolable prefab;
	public Poolable Prefab{
		get{
			return prefab;
		}
	}

	void Awake(){
		if(PoolContainsUnused(this.prefab)){
			Debug.LogWarning("Poolable will not be reused (to prevent uncontrolled pool growth) as it was not created via Poolable.GetOrCreate or it is a child in prefab which caused it to not be created by Poolable.GetOrCreate");
		}else{
			allPoolables.Add(this.gameObject);
		}
	}

	static Dictionary<Poolable,Stack<GameObject>> pool = new Dictionary<Poolable, Stack<GameObject>>();
	static HashSet<GameObject> allPoolables = new HashSet<GameObject>();

	static Dictionary<GameObject,Guid> UsageIds = new Dictionary<GameObject,Guid>();


	public static GameObject GetOrCreate(

		GameObject prefab,
		Vector3 position, 
		Quaternion rotation,
		out Guid UsageID
		){
		GameObject instance = GetOrCreate(prefab,prefab.GetPoolable(),out UsageID);
		instance.transform.parent = LevelMap.MapRoot.transform;
		instance.transform.position = position;
		instance.transform.rotation = rotation;
		return instance;
	}
	public static bool PoolContainsUnused(Poolable poolablePrefab){
		return (pool.ContainsKey(poolablePrefab)&&pool[poolablePrefab].Count>0);
	}
	/*
	public static GameObject GetOrCreate(GameObject prefab,out Guid UsageID){
		return GetOrCreate(prefab,prefab.GetPoolable(),out UsageID);
	}
	public static GameObject GetOrCreate(Poolable poolablePrefab,out Guid UsageID){
		return GetOrCreate(poolablePrefab.prefab,poolablePrefab,out UsageID);
	}*/
	private static GameObject GetOrCreate(
		GameObject prefab,
		Poolable poolablePrefab,
		out Guid UsageID
		){
		if(prefab == null){
			Debug.LogWarning("null prefab detected");
			prefab = new GameObject();
		}
		GameObject instance = null;
		if(poolablePrefab != null){
			if(poolablePrefab!= poolablePrefab.Prefab) 
				throw new System.Exception("to be Poolable prefab, Prefab property must return a self reference");
			//poolablePrefab = poolablePrefab.Prefab;
			if(!pool.ContainsKey(poolablePrefab)) pool.Add(poolablePrefab,new Stack<GameObject>());
			while(pool[poolablePrefab].Count>0){
				instance = pool[poolablePrefab].Pop();
				if(instance!=null) break;
			}
		}
		if(instance==null){
			instance = (GameObject)Instantiate(prefab);
		}
		if(poolablePrefab != null){
			UsageID = Guid.NewGuid();
			UsageIds[instance] = UsageID;
		}else{
			UsageID = Guid.Empty;
		}
		instance.transform.SetParent(null);
		instance.SetActive(prefab.activeSelf);
		return instance;
	}

	public void MarkForReuse(){
		if(allPoolables.Contains(this.gameObject)){
			gameObject.SetActive(false);
			gameObject.transform.SetParent(PoolRoot.transform);
			//if(UsageIds.ContainsKey(gameObject)){
				UsageIds[gameObject] = Guid.Empty;
			//}else{
			//	Debug.LogWarning("poolable not found in UsageIds")
			//}
			if(!pool.ContainsKey(Prefab)) pool.Add(Prefab,new Stack<GameObject>());
			pool[Prefab].Push(gameObject);
		}else{
			Destroy(gameObject);
		}
	}

	public static bool HasBeenReclaimed(GameObject go,Guid usageID){
		if(go==null)return true;
		if(usageID == Guid.Empty) return false;
		return !(UsageIds.ContainsKey(go)&& UsageIds[go]==usageID);
	}

}
static class PoolableExtensions{
	public static Poolable GetPoolable(this GameObject go){
		/*if(!IsPoolableLookup.ContainsKey(go)){
			IsPoolableLookup[go] = (go.GetComponent<Poolable>()!=null);
		}
		return IsPoolableLookup[go];*/
		return go.GetComponent<Poolable>();
	}

	public static void Unload(this GameObject go){
		Poolable[] poolableObjects = go.GetComponentsInChildren<Poolable>();
		foreach(Poolable poolableObject in poolableObjects){
			poolableObject.MarkForReuse();
		}
		GameObject.Destroy(go);
	}

}

public class GameObjectReference{
	public GameObject prefab;

	[HideInInspector]
	[NotSerialized]
	private GameObject _gameobject;
	
	[HideInInspector]
	[NotSerialized]
	private Guid UsageID = Guid.Empty;



	public GameObject GetOrCreateGameObject(Vector3 position,Quaternion rotation){
		if(GetGameObject()==null){
			_gameobject = Poolable.GetOrCreate(prefab,position,rotation,out UsageID);
		}else{
			_gameobject.transform.position = position;
			_gameobject.transform.rotation = rotation;
		}
		return _gameobject;
	}
	public GameObject GetGameObject(){
		if(Poolable.HasBeenReclaimed(_gameobject,UsageID)) return null;
		return _gameobject;
	}
}

