using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FullInspector;
using UnityEngine.UI;

enum OutOfCombatActions{MoveLeft,MoveRight,MoveUp,MoveDown,Suicide};



public abstract class GameState{
	public static Exception NotCurrentGameStateException = new Exception("Not Current GameState Exception");
	public virtual void OnLoad(){}
	public virtual void OnUnload(){}
	public abstract bool IsVisible{get;set;}
	public abstract bool AllowsInput{get;set;}

	//public override void OnLoad();
	//public override void OnUnload();
	//public override bool IsVisible{get{}set{}}
	//public override bool AllowsInput{get{}set{}}

	public class MainMenu:GameState{
		//button "Load Adventure" to enter substate Load Adventure
		//button "New Adventure" to create new Save and enter substate Overworld
		//button "Settings" to enter substate Settings
		//button "Quit" to Quit

		
		public override void OnLoad(){
			IsVisible = true;
			AllowsInput = true;
		}
		public override void OnUnload(){
			Application.Quit();
		}
		public override bool IsVisible{
			get{
				return MainMenuManager.inst.IsVisible;
			}
			set{
				MainMenuManager.inst.IsVisible = value;
			}
		}
		public override bool AllowsInput{
			get{
				return MainMenuManager.inst.AllowsInput;
			}
			set{
				MainMenuManager.inst.AllowsInput = value;
			}
		}

		
		public void LoadNewSave(){
			//if(Game.Inst.GameStateStack.Current!=this) throw NotCurrentGameStateException;
			Save.LoadedSave = new Save();
			Game.Inst.GameStateStack.Push(new GameState.OverWorld());
			AllowsInput = false;
			IsVisible = false;
		}

		/*public class LoadAdventure:GameState{
			public void Cancel(){
				Game.Inst.GameStateStack.Pop();

			}
			public void Load(Save save){
				if(Game.Inst.GameStateStack.Current!=this) throw NotCurrentGameStateException;
				//go back to MainMenu state (just in case loading from inside another save)
				while(!(Game.Inst.GameStateStack.Current is MainMenu)){
					Game.Inst.GameStateStack.Pop();
				}
				Save.loadedSave = save;
				Game.Inst.GameStateStack.Push(new GameState.OverWorld());
			}
			//public override void OnLoad();
			//public override void OnUnload();
			//public override bool IsVisible{get{}set{}}
			//public override bool AllowsInput{get{}set{}}
		}*/
	}
	public class OverWorld:GameState{

		//allow walking
		//allow inventory changes
		//allow skill changes
		//allow inventory changes

		public override void OnLoad(){
			IsVisible = true;
			AllowsInput = true;
		}
		public override void OnUnload(){
			LevelMap.UnloadAll();
		}
		public override bool IsVisible{
			get{
				return OverWorldManager.inst.IsVisible;
			}
			set{
				OverWorldManager.inst.IsVisible = value;
			}
		}
		public override bool AllowsInput{
			get{
				return OverWorldManager.inst.AllowInput;
			}
			set{
				OverWorldManager.inst.AllowInput = value;
			}
		}
	}
	
	public class Combat:GameState{
		//pick ability to use


		public override void OnLoad(){
			IsVisible = true;
			AllowsInput = true;
		}
		public override void OnUnload(){
			IsVisible = false;
			AllowsInput = false;
		}
		public override bool IsVisible{
			get{
				return CombatManager.inst.gameObject.activeSelf;
			}
			set{
				CombatManager.inst.gameObject.SetActive(value);
			}
		}
		public override bool AllowsInput{
			get{
				return CombatManager.inst.AllowInput;
			}
			set{
				CombatManager.inst.AllowInput = value;
			}
		}
	
		public class Targeting:GameState{
			public TargetCriteria criteria;
			public List<Instance<Combatant>> targetsSelected = new List<Instance<Combatant>>();
			Action<IEnumerable<Instance<Combatant>>> Callback;
			Action Abort;
			public Targeting(Action<IEnumerable<Instance<Combatant>>> Callback,Action Abort,TargetCriteria criteria){
				this.Callback = Callback;
				this.criteria = criteria;
			}
			public class TargetCriteria{
				public string description;
				public bool requiresUserSelection;
				public IFunc<Instance<Combatant>,IEnumerable<Instance<Combatant>>> GetDefaultSelection;
				public IEnumerator GetTargets(Instance<Combatant> combatant, Action<IEnumerable<Instance<Combatant>>> Callback,Action Abort){
					IEnumerable<Instance<Combatant>> targets = null;
					bool abort = false;
					if(requiresUserSelection){
						if(combatant!=Save.LoadedSave.ActiveCharacter) throw new System.Exception();
						Targeting targetingState = new Targeting(delegate(IEnumerable<Instance<Combatant>> confirmedTargets){targets=confirmedTargets;},delegate(){abort = true;},this);
						targetingState.targetsSelected = new List<Instance<Combatant>>(GetDefaultSelection.Invoke(combatant));
						Game.Inst.GameStateStack.Push(targetingState);
						while(!abort && targets == null){
							yield return null;
						}
						Game.Inst.GameStateStack.Pop();
						if(abort){
							Abort();
							yield break;
						}
					}else{
						targets = GetDefaultSelection.Invoke(combatant);
					}
					Callback.Invoke(targets);
				}
				public IFunc<Instance<Combatant>,IEnumerable<Instance<Combatant>>,bool> IsValidTarget;
				public IFunc<IEnumerable<Instance<Combatant>>,bool> AllTargetingConditionsMet;
				
				public class SelectActivePlayer:IFunc<Instance<Combatant>,IEnumerable<Instance<Combatant>>>{
					public IEnumerable<Instance<Combatant>> Invoke(Instance<Combatant> combatant){
						if(Save.LoadedSave.ActiveCharacter.Entity.Foes.Contains(combatant.Entity)){
							return new List<Instance<Combatant>>(){Save.LoadedSave.ActiveCharacter.ToInstanceOf<Combatant>()};
						}else 
							return new List<Instance<Combatant>>();
					}
					public bool IsSafeToInvoke{
						get{
							return true;
						}
					}
				}
				public class SelectSelf:IFunc<Instance<Combatant>,IEnumerable<Instance<Combatant>>>{
					public IEnumerable<Instance<Combatant>> Invoke(Instance<Combatant> combatant){
						return new Instance<Combatant>[]{combatant,};
					}
					public bool IsSafeToInvoke{
						get{
							return true;
						}
					}
				}
			}
			public override void OnLoad(){
				IsVisible = true;
				AllowsInput = true;
			}
			public override void OnUnload(){
				IsVisible = false;
				AllowsInput = false;
			}
			public override bool IsVisible{
				get{
					return TargetingManager.inst.gameObject.activeSelf;
				}
				set{
					TargetingManager.inst.gameObject.SetActive(value);
				}
			}
			public override bool AllowsInput{
				get{
					return TargetingManager.inst.AllowInput;
				}
				set{
					TargetingManager.inst.AllowInput = value;
				}
			}

			//public override void OnLoad();
			//public override void OnUnload();
			//public override bool IsVisible{get{}set{}}
			//public override bool AllowsInput{get{}set{}}
		}
		public class ActionSelect:GameState{
			//IAction<IEnumerable<Instance<Combatant>>> Confirm;
			public ActionSelect(){

			}
			public override void OnLoad(){
				Update();
				IsVisible = true;
				AllowsInput = true;

			}
			public override void OnUnload(){
				IsVisible = false;
				AllowsInput = false;
				foreach(Transform child in CombatActionSelectManager.inst.transform){
					child.gameObject.Unload();
				}
			}
			public void Update(){


				foreach(Transform child in CombatActionSelectManager.inst.transform){
					child.gameObject.Unload();
				}
				if(Save.LoadedSave.ActiveCharacter.Exists&&Save.LoadedSave.ActiveCharacter.Entity.onUseArgs.HasValue){

					OnUseArgs args = Save.LoadedSave.ActiveCharacter.Entity.onUseArgs.Value;
					foreach(ICombatAbility ability in Save.LoadedSave.ActiveCharacter.Entity.combatAbilities){
						Guid id;
						GameObject optionGO = Poolable.GetOrCreate(CombatActionSelectManager.inst.OptionPrefab,Vector3.zero,Quaternion.identity,out id);
						optionGO.transform.parent = CombatActionSelectManager.inst.transform;
						CombatOptionButton cob = optionGO.GetComponent<CombatOptionButton>();
						cob.ResetTo(ability,args);
					}
				}


			}
			public override bool IsVisible{
				get{
					return CombatActionSelectManager.inst.gameObject.activeSelf;
				}
				set{
					CombatActionSelectManager.inst.gameObject.SetActive(value);
				}
			}
			public override bool AllowsInput{
				get{
					return CombatActionSelectManager.inst.AllowInput;
				}
				set{
					CombatActionSelectManager.inst.AllowInput = value;
				}
			}
			
			//public override void OnLoad();
			//public override void OnUnload();
			//public override bool IsVisible{get{}set{}}
			//public override bool AllowsInput{get{}set{}}
		}
	}
}


/*public abstract class GameState {

	public bool AllowMovementInput;

	public GameState lastState;


	 public void EnterCombat(){
		_InCombat = true;
		CombatView.Inst.enabled = true;
	}
	public void ExitCombat(){
		_InCombat = false;
		CombatView.Inst.enabled = false;
	}




	public class InCombat:GameState{
		public override void OnEnterState ()
		{
			CombatView.Inst.enabled = true;
			OutOfCombatView.Inst.enabled = false;
			base.OnEnterState ();
		}
	}

	public class Menu:GameState{
	}


	
}*/
[Serializable]
public class GameStateStack:IEnumerable<GameState>{
	[ShowInInspector]
	readonly List<GameState> stack = new List<GameState>();
	public IEnumerator<GameState> GetEnumerator(){
		return stack.GetEnumerator();
	}
	IEnumerator IEnumerable.GetEnumerator(){
		return stack.GetEnumerator();
	}

	int LastIndex{
		get{
			return stack.Count-1;
		}
	}

	public GameState Current{
		get{
			return stack[LastIndex];
		}
	}
	public void Push(GameState state){
		stack.Add(state);
		state.OnLoad();
		Debug.Log(""+state.GetType().ToString() +" has been loaded");
	}
	public GameState Pop(){
		int lastIndex = LastIndex;
		GameState last = stack[lastIndex];
		stack.RemoveAt(lastIndex);
		last.OnUnload();
		Current.AllowsInput = true;
		Current.IsVisible = true;
		return last;
	}
	public bool Contains(GameState gameState){
		return stack.Contains(gameState);
	}
	public bool ContainsType(Type gameStateType){
		return stack.Any(gs=>gs.GetType().IsAssignableFrom(gameStateType.GetType()));
	}
	public bool ContainsType<T>(){
		return this.ContainsType(typeof(T));
	}
}



public class Game : BaseBehavior {
	public LocationMutable StartingLocation;
	public static Game Inst;
	[ShowInInspector]
	[NonSerialized,NotSerialized]
	public Save currentSave;
	[HideInInspector]
	public LevelMap ActiveMap{
		get{
			return LevelMap.ActiveMap;
		}
		set{

		}
	}
	[ShowInInspector]
	public readonly GameStateStack GameStateStack = new GameStateStack();
	public PlayerPrefab playerPrefab = new PlayerPrefab();

	protected override void Awake(){
		base.Awake();

		if(!(Game.Inst==null||Game.Inst==this)){
			Destroy(this);
			return;
		}
		currentSave = null;
		DontDestroyOnLoad(this);
		Game.Inst = this;
		//state = new GameState();
		//if(Save.LoadedSave == null) new Save().Load();
	}
	void Start(){
		GameStateStack.Push(new GameState.MainMenu());
	}
	void Update(){

		if(currentSave!=null){
			if(currentSave.MoveNext()){
			}
		}

	}

	/*static void MoveCharacter(int i , int xOffset, int yOffset){
		Character c = Save.loadedSave.characters[i];
		c.Location = new Location(c.Location.room, new IntCoord(
			c.Location.coord.x+xOffset,
			c.Location.coord.y+yOffset
			));
	}*/
	
	/*
	public void AddActionAtTurnEnd(GameAction action){
		int i = nextAction;
		float t = actions[i].t;
		while(i<actions.Count&&actions[i].t<=t){
			actions[i].Action();
			i++;
			//return;
		}

		if(nextAction==actions.Count){
			actions.Add(action);
		}else{
			actions.Insert(nextAction,action);
		}
	}*/

}
