using System;
using System.Collections.Generic;
namespace GameAction
{
	public interface ICombatAction{
		decimal DelaysNextActionBy{get;}
	}
	public interface IOutOfCombatAction{
		decimal DelaysNextActionBy{get;}
	}

	public class GameAction{
		//public decimal DelaysNextActionBy{get; set;}
		//public GameAction(decimal delaysNextActionBy){
		//	this.DelaysNextActionBy = delaysNextActionBy;
		//}
	}
	/*
	//public static Dictionary<string,GameAction> CombatActions
*/

	public class CombatOption : GameAction
	{
		public string name;
		public string description;
		public System.Func<bool,Args> CanUseNow;
		public System.Action<Args> Use;

		public class Args{
			public Combatant combatant;
			public Combatant[] othersInBattle;
		}

	}
	//public static Dictionary<string,GameAction> CombatActions

	public class Defend : GameAction
	{
		/*
		public Defend (decimal delaysNextActionBy):base(delaysNextActionBy)
		{
		}
		*/
	}
}

