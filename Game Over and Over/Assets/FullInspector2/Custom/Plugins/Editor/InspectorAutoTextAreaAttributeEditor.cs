using UnityEditor;
using UnityEngine;
using System;

namespace FullInspector.Modules.Common {
    [CustomAttributePropertyEditor(typeof(InspectorAutoTextAreaAttribute), ReplaceOthers = true)]
	public class InspectorAutoTextAreaAttributeEditor : AttributePropertyEditor<string, InspectorAutoTextAreaAttribute> {
		float lastWidth=0;
		GUIStyle style;
		public InspectorAutoTextAreaAttributeEditor(){
			style = new GUIStyle(EditorStyles.textArea);

		}
		protected override string Edit(Rect region, GUIContent label, string element, InspectorAutoTextAreaAttribute attribute, fiGraphMetadata metadata) {
            // note: Unity does *not* provide a label override for TextArea, so we have to handle it ourselves.
			style.wordWrap = attribute.wrapText;
            // We don't have an empty label
            if (string.IsNullOrEmpty(label.text) == false || label.image != null) {
                region = EditorGUI.PrefixLabel(region, label);
            }
			lastWidth = region.width;
            // No label
			return EditorGUI.TextArea(region, element,style);
        }

		protected override float GetElementHeight(GUIContent label, string element, InspectorAutoTextAreaAttribute attribute, fiGraphMetadata metadata) {
			/*int lineCount = 0;
			if(element != null && element != string.Empty){
				lineCount = element.Split(new string[] { "\n", "\r\n", }, StringSplitOptions.None).Length;
			}*/

			return style.CalcHeight(new GUIContent(element),lastWidth)+style.lineHeight /** (lineCount + 1)*/;
        }
    }
}