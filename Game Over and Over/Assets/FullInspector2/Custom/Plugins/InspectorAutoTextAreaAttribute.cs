using System;
namespace FullInspector {
    /// <summary>
    /// Show a text-area instead of a text-field for a string.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public sealed class InspectorAutoTextAreaAttribute : Attribute {
		public bool wrapText;
		public InspectorAutoTextAreaAttribute()  { }
    }
}