using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using FullInspector; 
using FullSerializer.Internal;

[CustomPropertyEditor(typeof(IMap<>),true)]
public class IMapPropertyEditor<T> : PropertyEditor<IMap<T>> where T:class{
	public static Dictionary<IMap<T>,IntCoord> selected =new Dictionary<IMap<T>, IntCoord>();

	static Dictionary<IMap<T>,Vector2> _mapOffset =new Dictionary<IMap<T>, Vector2>();
	static Dictionary<IMap<T>,Vector2> _mapDragStart =new Dictionary<IMap<T>, Vector2>();


	public static IntCoord? GetSelected(IMap<T> map){
		if(map == null) return null;
		else if(!selected.ContainsKey(map)) return null;
		else return selected[map];
	}
	public static void SetSelected(IMap<T> map, IntCoord? coord){
		if(!coord.HasValue) selected.Remove(map);
		else selected[map] = coord.Value;
	}
	int mapHeight = 300;
	float zoom = 10;
	Vector2 MapOffset(IMap<T> map){
			Vector2 mapOffset = Vector2.zero;
			if(!_mapOffset.ContainsKey(map)){
				_mapOffset[map]=mapOffset;
			}else{
				mapOffset = _mapOffset[map];
			}
		if(_mapDragStart.ContainsKey(map)) return mapOffset+  Event.current.mousePosition - _mapDragStart[map];
		return mapOffset;
	}
	Vector2 MapCenter(IMap<T> map){
		return Vector2.zero-MapOffset(map);
	}
	static Dictionary<System.Type,IPropertyEditor> IPropertyEditors = new Dictionary<System.Type, IPropertyEditor>();

	IPropertyEditor GetPropertyEditor(System.Type type){
		if(!IPropertyEditors.ContainsKey(type)){
			IPropertyEditors[type] = PropertyEditor.Get(type,null).FirstEditor;
		}
		return IPropertyEditors[type];
	}

	GUIStyle boxStyle = "box";

	static ISpawns<ITile> tileSpawnerClipboard;

	public override IMap<T> Edit(Rect region, GUIContent label, IMap<T> element, fiGraphMetadata metadata) {
		if(element==null){
			GUI.Label(region,"null IMap<T>");
			return null;
		}
		/*if(element!=null){
			if(Event.current.type == EventType.MouseDown){
				_mapDragStart[element] = Event.current.mousePosition;
				if(!_mapOffset.ContainsKey(element)){
					_mapOffset[element] = Vector2.zero;
				}
			}else if(_mapDragStart.ContainsKey(element)){
				if(Event.current.type == EventType.MouseDrag){
					_mapOffset[element] += Event.current.mousePosition - _mapDragStart[element];
					_mapDragStart.Remove(element);
				}else
				if( Event.current.type == EventType.mouseUp){
					_mapOffset[element] += Event.current.mousePosition - _mapDragStart[element];
					_mapDragStart.Remove(element);
				}
				//Debug.Log(Event.current.type.ToString()+"\n"+(-MapOffset(element)).ToString());
			}
		}*/
		IntCoord min;
		IntCoord max;
		Vector2 average;
		{
			Vector2 sum = Vector2.zero;
			int count = 0;
			int? minX = null;
			int? minY = null;
			int? maxX = null;
			int? maxY = null;
			foreach(IntCoord coord in ((IDictionary<IntCoord,T>)element).Keys){
				sum += coord.AsVector2;
				count++;
				if(!minX.HasValue){
					minX = coord.x;
				}
				if(!minY.HasValue){
					minY = coord.y;
				}
				if(!maxX.HasValue){
					maxX = coord.x;
				}
				if(!maxY.HasValue){
					maxY = coord.y;
				}
				if(coord.x<minX.Value) minX = coord.x;
				if(coord.x>maxX.Value) maxX = coord.x;
				if(coord.y<minY.Value) minY = coord.y;
				if(coord.y>maxY.Value) maxY = coord.y;
			}
			if(!minX.HasValue){
				minX = 0;
			}
			if(!minY.HasValue){
				minY = 0;
			}
			if(!maxX.HasValue){
				maxX = 0;
			}
			if(!maxY.HasValue){
				maxY = 0;
			}
			min = new IntCoord(minX.Value-0,minY.Value-0);
			max = new IntCoord(maxX.Value+0,maxY.Value+0);
			if(count>0){
				sum/=(float)count;
			}
			average = sum;
		}
		//Debug.Log(min);
		//Debug.Log(max);
		Rect RemainingSpace = new Rect(region);
		float currentY = 0;
		Rect mapRect = new Rect(region.x,region.y,region.width-20,mapHeight-20);
		float mapElementSize = mapHeight/zoom;
		//GUILayout.BeginArea(mapRect,"","box");
		if(!_mapOffset.ContainsKey(element)){
			_mapOffset[element] = average;
		}
		float vScroll = _mapOffset[element].y;
		float hScroll = _mapOffset[element].x;
		vScroll = GUI.VerticalScrollbar(new Rect(mapRect.x+mapRect.width,mapRect.y,20,mapRect.height),vScroll,0,min.y,max.y);
		hScroll = GUI.HorizontalScrollbar(new Rect(mapRect.x,mapRect.y+mapRect.height,mapRect.width,20),hScroll,0,min.x,max.x);
		/*if(_mapOffset[element]!=new Vector2(hScroll,vScroll)){
			Debug.Log (""+Event.current.type.ToString()+"  " + new Vector2(hScroll,vScroll).ToString()+" "+min.AsVector2.ToString()+" "+max.AsVector2.ToString());
		}*/
		//if( (!_mapOffset.ContainsKey(element)) || Event.current.type == EventType.Repaint ){
			_mapOffset[element] = new Vector2(hScroll,vScroll);
		//}
		//Debug.Log(_mapOffset[element]);

		//Rect viewRect = Rect.MinMaxRect(min.x*mapElementSize,min.y*mapElementSize,max.x*mapElementSize,max.y*mapElementSize);

		//Vector2 scroll = _mapOffset[element];
		//scroll-=min.AsVector2
		//_mapOffset[element] = GUI.BeginScrollView(mapRect,_mapOffset[element],viewRect,true,true);
		//Debug.Log (_mapOffset[element]);

		
		GUI.BeginGroup(mapRect);
		//Rect MapRectInScreenSpace = GUILayoutUtility.GetLastRect();
		//GUI.BeginGroup(new Rect());
		/*GUI.BeginGroup(
			new Rect(
			-_mapOffset[element].x*mapElementSize,//(Mathf.FloorToInt(_mapOffset[element].x)-(_mapOffset[element].x) * mapElementSize),
			-_mapOffset[element].y*mapElementSize,//(Mathf.FloorToInt(_mapOffset[element].y)-(_mapOffset[element].y) * mapElementSize),
			mapRect.width  + mapElementSize,
			mapRect.height + mapElementSize))*/;
		int y = -Mathf.CeilToInt(mapRect.height/mapElementSize/2)+Mathf.FloorToInt(_mapOffset[element].y);
		while( y<Mathf.CeilToInt(mapRect.height/mapElementSize/2)+1+Mathf.CeilToInt(_mapOffset[element].y)){
			int x = -Mathf.CeilToInt(mapRect.width/mapElementSize/2)+Mathf.FloorToInt(_mapOffset[element].x);
			while( x<Mathf.CeilToInt(mapRect.width/mapElementSize/2f)+1+Mathf.CeilToInt(_mapOffset[element].x)){
				/*if (Event.current.type == EventType.Repaint &&
				    GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
				{
					usingSlider=true;
				}*/
				IntCoord coord = new IntCoord(x,y);
				Rect mapElementRect = new Rect((x)*mapElementSize+mapRect.width/2-_mapOffset[element].x*mapElementSize, (y)*mapElementSize+mapRect.height/2-_mapOffset[element].y*mapElementSize,mapElementSize,mapElementSize);
				string tooltip = coord.ToString();
				T mapElement = null;
				if(element!=null&&element.ContainsKey(coord)){
					mapElement = element[coord];
				}

				if( mapElement!=null ){
					tooltip += "\n"+mapElement.ToString();
				}


				if(mapElementRect.Contains(Event.current.mousePosition)){

				}
				if(mapElement!=null){
					GUI.color = new Color(1,1,1,.5f);
					GUI.Label(mapElementRect,"","button");
					GUI.color = new Color(1,1,1,1);
					IMapDrawable drawableMapElement = mapElement as IMapDrawable;
					if(drawableMapElement != null){
						//Color colorToRestore = GUI.color;
						//GUI.color = drawableMapElement.IMapDrawableColor;
						drawableMapElement.IMapDraw(mapElementRect);
						//GUI.Label(,new GUIContent(drawableMapElement.IMapDrawableText,drawableMapElement.IMapDrawableIcon,drawableMapElement.IMapDrawableStyle);
						//GUI.color = colorToRestore;
					}else{
						GUI.Box(mapElementRect,mapElement.ToString());
					}
				}else{
					
					GUI.color = new Color(1,1,1,.1f);
					GUI.Label(mapElementRect,"","button");
					GUI.color = new Color(1,1,1,1);
				}
				
				if(mapElementRect.Contains(Event.current.mousePosition)){
					
					GUI.Label(mapElementRect,"","box");
					GUI.Label(mapElementRect,"","box");
				}
				GUI.color = new Color(1,1,1,0f);

				if(GUI.Button(mapElementRect,new GUIContent("",tooltip) )){
					SetSelected(element,coord);
				}
				GUI.color = new Color(1,1,1,1);
				if(GetSelected(element) == coord){
					GUI.Label(mapElementRect,"","box");
					GUI.Label(mapElementRect,"","box");
					GUI.Label(mapElementRect,"","box");
				}
				x++;
			}
			y++;
		}
		GUI.EndGroup();
		//GUI.EndScrollView();
		/*Vector2 mousePosition = Event.current.mousePosition;
		GUIStyle boxStyle = "box";
		Vector2 tooltipSize	= boxStyle.CalcSize(new GUIContent(GUI.tooltip));
		GUI.Box(new Rect(mousePosition.x,mousePosition.y,tooltipSize.x,tooltipSize.y),GUI.tooltip);*/
		//GUILayout.EndArea();
		currentY +=mapHeight;

		RemainingSpace.height-=mapHeight;
		RemainingSpace.y+=mapHeight;
		//center map button

		IntCoord? selectedCoord = GetSelected(element);
		if(selectedCoord.HasValue){
			element[selectedCoord.Value] = (T)GetPropertyEditor(typeof(T)).Edit(RemainingSpace, new GUIContent("Selected"), element[selectedCoord.Value],metadata.Enter("asdf"));
			LevelTilePrefab ltp = element[selectedCoord.Value] as LevelTilePrefab;
			if(ltp!=null){
				if(Event.current.control && Event.current.keyCode == KeyCode.C ){
					tileSpawnerClipboard = ltp.tileSpawner;
					Debug.Log("Copied");
				}
				
				if(Event.current.control && Event.current.keyCode == KeyCode.V ){
					ltp.tileSpawner = tileSpawnerClipboard;
					element[selectedCoord.Value] = ltp as T;
					Debug.Log("Pasted");
				}
			}
		}

		/*var orderedMembers = _metadata.GetMembers(InspectedMemberFilters.InspectableMembers);
		for (int i = 0; i < orderedMembers.Count; ++i) {
			InspectedMember member = orderedMembers[i];
			
			// requested skip
			if (ReflectedPropertyEditor.ShouldShowMemberDynamic(element, member.MemberInfo) == false) {
				continue;
			}
			
			if (member.IsMethod) {
				ReflectedPropertyEditor.EditButton(ref region, element, member.Method);
			}
			else {
				ReflectedPropertyEditor.EditProperty(ref region, element, member.Property, metadata);
			}
			
			region.y += DividerHeight;
		}*/
		/*string serializedString = SerializationHelpers.SerializeToContent<IMap<T>,FullSerializerSerializer>(element);

		element = SerializationHelpers.DeserializeFromContent<IMap<T>,FullSerializerSerializer>(serializedString);
		*/
		return element;
	}
	/*
	/// <summary>
	/// A helper method that draws the inspector for a field/property at the given location.
	/// </summary>
	private void EditProperty(ref Rect region, object element, InspectedProperty property, fiGraphMetadata metadata) {
		bool hasPrefabDiff = fiPrefabTools.HasPrefabDiff(element, property);
		if (hasPrefabDiff) UnityInternalReflection.SetBoldDefaultFont(true);
		
		// edit the property
		{
			Rect propertyRect = region;
			float propertyHeight = fiEditorGUI.EditPropertyHeight(element, property, metadata.Enter(property.Name));
			propertyRect.height = propertyHeight;
			
			fiEditorGUI.EditProperty(propertyRect, element, property, metadata.Enter(property.Name));
			
			region.y += propertyHeight;
		}
		
		if (hasPrefabDiff) UnityInternalReflection.SetBoldDefaultFont(false);
	}
	*/
	public override float GetElementHeight(GUIContent label, IMap<T> element, fiGraphMetadata metadata) {
		if(element==null){
			return 30;
		}
		float height = mapHeight;


		IntCoord? selectedCoord = GetSelected(element);
		if(selectedCoord.HasValue){
			//var editorChain = PropertyEditor.Get(element[selectedCoord.Value].GetType(), null);
			height += GetPropertyEditor(typeof(T)).GetElementHeight(new GUIContent("Selected"),element[selectedCoord.Value],metadata.Enter("asdf"));
		}
		/*

		height += (ButtonHeight + DividerHeight) * _metadata.GetMethods(InspectedMemberFilters.ButtonMembers).Count;
		
		var inspectedProperties = _metadata.GetProperties(InspectedMemberFilters.InspectableMembers);
		for (int i = 0; i < inspectedProperties.Count; ++i) {
			var property = inspectedProperties[i];
			
			// requested skip
			if (ReflectedPropertyEditor.ShouldShowMemberDynamic(element, property.MemberInfo) == false) {
				continue;
			}
			
			height += fiEditorGUI.EditPropertyHeight(element, property, metadata.Enter(property.Name));
			height += DividerHeight;
		}
		
		// Remove the last divider
		if (inspectedProperties.Count > 0) height -= DividerHeight;*/
		return height;
	}

	public void LevelTileDrop (Rect rect, LevelMapPrefab levelmap, IntCoord coord)
	{
		Event evt = Event.current;
		Rect drop_area = GUILayoutUtility.GetRect (0.0f, 50.0f, GUILayout.ExpandWidth (true));
		GUI.Box (drop_area, "Add Trigger");
		
		switch (evt.type) {
		case EventType.DragUpdated:
		case EventType.DragPerform:
			if (!drop_area.Contains (evt.mousePosition))
				return;
			
			DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
			
			if (evt.type == EventType.DragPerform) {
				DragAndDrop.AcceptDrag ();

				LevelTilePrefab ltp = null;
				ltp = levelmap[coord];
				foreach (Object dragged_object in DragAndDrop.objectReferences) {

					if(dragged_object is TilePrefab){
						ltp.tileSpawner = (ISpawns<ITile>)dragged_object;
					}
					if(dragged_object is ISpawns<ITile>){
						ltp.tileSpawner = (ISpawns<ITile>)dragged_object;
					}
					if(dragged_object is IAction<OnSpawnArgs>){
						ltp.OnSpawn.Add((IAction<OnSpawnArgs>)dragged_object);
					}
					if(dragged_object is IAction<OnEnterArgs>){
						ltp.OnEnter.Add((IAction<OnEnterArgs>)dragged_object);
					}
					if(dragged_object is IAction<OnStayArgs>){
						ltp.OnStay.Add((IAction<OnStayArgs>)dragged_object);
					}
					if(dragged_object is IAction<OnExitArgs>){
						ltp.OnExit.Add((IAction<OnExitArgs>)dragged_object);
					}
					/*if(dragged_object is IReference<ILocated>){
						ltp.entities.Add((IReference<ILocated>)dragged_object);
					}*/
					/*if(dragged_object is ILocated){
						ltp.entities.Add(new Reference<ILocated>((IReference<ILocated>)dragged_object));
					}*/
					if(dragged_object is SharedInstance<ISpawns<ILocated>>){
						ltp.entities.Add(new SharedPrefabReference<ISpawns<ILocated>, ILocated>((SharedInstance<ISpawns<ILocated>>)dragged_object,System.Guid.NewGuid()));
					}
				}
				levelmap[coord] = ltp;
			}
			break;
		}
	}
}
