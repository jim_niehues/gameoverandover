using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

static class PseudoEventExtensions{
	/*public static void InvokeAll(this IEnumerable<Action> actions){
		foreach(Action action in actions.ToArray()){
			if(action!=null){
				action();
			}
		}
	}
	public static void InvokeAll<T>(this IEnumerable<Action<T>> actions,T arg){
		foreach(Action<T> action in actions.ToArray()){
			if(action!=null){
				action(arg);
			}
		}
	}
	public static void InvokeAll<T1,T2>(this IEnumerable<Action<T1,T2>> actions,T1 arg1,T2 arg2){
		foreach(Action<T1,T2> action in actions.ToArray()){
			if(action!=null){
				action(arg1,arg2);
			}
		}
	}
	public static void InvokeAll<T1,T2,T3>(this IEnumerable<Action<T1,T2,T3>> actions,T1 arg1,T2 arg2,T3 arg3){
		foreach(Action<T1,T2,T3> action in actions.ToArray()){
			if(action!=null){
				action(arg1,arg2,arg3);
			}
		}
	}





	public static void InvokeAll<T1,T2,T3,T4>(this IEnumerable< Action<T1,T2,T3,T4>> actions,T1 arg1,T2 arg2,T3 arg3,T4 arg4){
		foreach(Action<T1,T2,T3,T4> action in actions.ToArray()){
			if(action!=null){
				action(arg1,arg2,arg3,arg4);
			}
		}
	}
	public static void InvokeAll(this IEnumerable<TaggedValue<Action>> actions){
		foreach(TaggedValue<Action> action in actions.ToArray()){
			if(action.value!=null){
				action.value();
			}
		}
	}
	public static void InvokeAll<T>(this IEnumerable<TaggedValue<Action<T>>> actions,T arg){
		foreach(TaggedValue<Action<T>> action in actions.ToArray()){
			if(action.value!=null){
				action.value(arg);
			}
		}
	}
	public static void InvokeAll<T1,T2>(this IEnumerable<TaggedValue<Action<T1,T2>>> actions,T1 arg1,T2 arg2){
		foreach(TaggedValue<Action<T1,T2>> action in actions.ToArray()){
			if(action.value!=null){
				action.value(arg1,arg2);
			}
		}
	}
	public static void InvokeAll<T1,T2,T3>(this IEnumerable<TaggedValue<Action<T1,T2,T3>>> actions,T1 arg1,T2 arg2,T3 arg3){
		foreach(TaggedValue<Action<T1,T2,T3>> action in actions.ToArray()){
			if(action.value!=null){
				action.value(arg1,arg2,arg3);
			}
		}
	}
	public static void InvokeAll<T1,T2,T3,T4>(this IEnumerable<TaggedValue<Action<T1,T2,T3,T4>>> actions,T1 arg1,T2 arg2,T3 arg3,T4 arg4){
		foreach(TaggedValue<Action<T1,T2,T3,T4>> action in actions.ToArray()){
			if(action.value!=null){
				action.value(arg1,arg2,arg3,arg4);
			}
		}
	}*/
}
public interface IPseudoEvent:IEnumerable,ICollection,IList{
}


public class PseudoEvent : List<IAction>,IPseudoEvent
{

	public PseudoEvent(){}
	public PseudoEvent(IEnumerable<IAction> collection):base(collection){}
	public void InvokeAll(){
		foreach(IAction action in this.ToArray()){
			if(action!=null&&action.IsSafeToInvoke){
				action.Invoke();
			}
		}
	}
	
	public void Add(Action action){
		this.Add(new RawAction(action));
	}
}
public class PseudoEvent<T> : List<IAction<T>>,IPseudoEvent
{
	public PseudoEvent(){}
	public PseudoEvent(IEnumerable<IAction<T>> collection):base(collection){}
	public void InvokeAll(T arg){
		if(arg is IPseudoEventArg) ((IPseudoEventArg)arg).SetPseudoEvent(this);
		foreach(IAction<T> action in this.ToArray()){
			if(action!=null&&action.IsSafeToInvoke){
				action.Invoke(arg);
			}
		}
	}
	
	public void Add(Action<T> action){
		this.Add(new RawAction<T>(action));
	}
}
public class PseudoEvent<T1,T2> : List<IAction<T1,T2>>,IPseudoEvent
{
	public PseudoEvent(){}
	public PseudoEvent(IEnumerable<IAction<T1,T2>> collection):base(collection){}
	public void InvokeAll(T1 arg1,T2 arg2){
		if(arg1 is IPseudoEventArg) ((IPseudoEventArg)arg1).SetPseudoEvent(this);
		if(arg2 is IPseudoEventArg) ((IPseudoEventArg)arg2).SetPseudoEvent(this);
		foreach(IAction<T1,T2> action in this.ToArray()){
			if(action!=null&&action.IsSafeToInvoke){
				action.Invoke(arg1,arg2);
			}
		}
	}
	public void Add(Action<T1,T2> action){
		this.Add(new RawAction<T1,T2>(action));
	}
}
public class PseudoEvent<T1,T2,T3> : List<IAction<T1,T2,T3>>,IPseudoEvent
{
	public PseudoEvent(){}
	public PseudoEvent(IEnumerable<IAction<T1,T2,T3>> collection):base(collection){}
	public void InvokeAll(T1 arg1,T2 arg2,T3 arg3){
		if(arg1 is IPseudoEventArg) ((IPseudoEventArg)arg1).SetPseudoEvent(this);
		if(arg2 is IPseudoEventArg) ((IPseudoEventArg)arg2).SetPseudoEvent(this);
		if(arg3 is IPseudoEventArg) ((IPseudoEventArg)arg3).SetPseudoEvent(this);
		foreach(IAction<T1,T2,T3> action in this.ToArray()){
			if(action!=null&&action.IsSafeToInvoke){
				action.Invoke(arg1,arg2,arg3);
			}
		}
	}
	public void Add(Action<T1,T2,T3> action){
		this.Add(new RawAction<T1,T2,T3>(action));
	}
}
public class PseudoEvent<T1,T2,T3,T4> : List<IAction<T1,T2,T3,T4>>,IPseudoEvent
{
	public PseudoEvent(){}
	public PseudoEvent(IEnumerable<IAction<T1,T2,T3,T4>> collection):base(collection){}
	public void Invoke(T1 arg1,T2 arg2,T3 arg3,T4 arg4){
		if(arg1 is IPseudoEventArg) ((IPseudoEventArg)arg1).SetPseudoEvent(this);
		if(arg2 is IPseudoEventArg) ((IPseudoEventArg)arg2).SetPseudoEvent(this);
		if(arg3 is IPseudoEventArg) ((IPseudoEventArg)arg3).SetPseudoEvent(this);
		if(arg4 is IPseudoEventArg) ((IPseudoEventArg)arg4).SetPseudoEvent(this);
		foreach(IAction<T1,T2,T3,T4> action in this.ToArray()){
			if(action!=null&&action.IsSafeToInvoke){
				action.Invoke(arg1,arg2,arg3,arg4);
			}
		}
	}
	
	public void Add(Action<T1,T2,T3,T4> action){
		this.Add(new RawAction<T1,T2,T3,T4>(action));
	}
}



public class TaggedPseudoEvent : List<TaggedValue<IAction>>,IPseudoEvent
{
	public TaggedPseudoEvent(){}
	public TaggedPseudoEvent(IEnumerable<TaggedValue<IAction>> collection):base(collection){}
	public void InvokeAll(){
		foreach(TaggedValue<IAction> action in this.ToArray()){
			if(action.value!=null&&action.value.IsSafeToInvoke){
				action.value.Invoke();
			}
		}
	}
	public void Add(IAction action){
		this.Add(new TaggedValue<IAction>(action));
	}
	public void Add(Action action){
		this.Add(new TaggedValue<IAction>(new RawAction(action)));
	}
}
public class TaggedPseudoEvent<T> : List<TaggedValue<IAction<T>>>,IPseudoEvent
{
	public TaggedPseudoEvent(){}
	public TaggedPseudoEvent(IEnumerable<TaggedValue<IAction<T>>> collection):base(collection){}
	public void InvokeAll(T arg){
		if(arg is IPseudoEventArg) ((IPseudoEventArg)arg).SetPseudoEvent(this);
		foreach(TaggedValue<IAction<T>> action in this.ToArray()){

			if(action.value!=null&&action.value.IsSafeToInvoke){
				action.value.Invoke(arg);
			}
		}
	}
	public void Add(IAction<T> action){
		this.Add(new TaggedValue<IAction<T>>(action));
	}
	public void Add(Action<T> action){
		this.Add(new TaggedValue<IAction<T>>(new RawAction<T>(action)));
	}
}
public class TaggedPseudoEvent<T1,T2> : List<TaggedValue<IAction<T1,T2>>>,IPseudoEvent
{
	public TaggedPseudoEvent(){}
	public TaggedPseudoEvent(IEnumerable<TaggedValue<IAction<T1,T2>>> collection):base(collection){}
	public void InvokeAll(T1 arg1,T2 arg2){
		if(arg1 is IPseudoEventArg) ((IPseudoEventArg)arg1).SetPseudoEvent(this);
		if(arg2 is IPseudoEventArg) ((IPseudoEventArg)arg2).SetPseudoEvent(this);
		foreach(TaggedValue<IAction<T1,T2>> action in this.ToArray()){
			if(action.value!=null&&action.value.IsSafeToInvoke){
				action.value.Invoke(arg1,arg2);
			}
		}
	}
	public void Add(IAction<T1,T2> action){
		this.Add(new TaggedValue<IAction<T1,T2>>(action));
	}
	public void Add(Action<T1,T2> action){
		this.Add(new TaggedValue<IAction<T1,T2>>(new RawAction<T1,T2>(action)));
	}
}
public class TaggedPseudoEvent<T1,T2,T3> : List<TaggedValue<IAction<T1,T2,T3>>>,IPseudoEvent
{
	public TaggedPseudoEvent(){}
	public TaggedPseudoEvent(IEnumerable<TaggedValue<IAction<T1,T2,T3>>> collection):base(collection){}
	public void InvokeAll(T1 arg1,T2 arg2,T3 arg3){
		if(arg1 is IPseudoEventArg) ((IPseudoEventArg)arg1).SetPseudoEvent(this);
		if(arg2 is IPseudoEventArg) ((IPseudoEventArg)arg2).SetPseudoEvent(this);
		if(arg3 is IPseudoEventArg) ((IPseudoEventArg)arg3).SetPseudoEvent(this);
		foreach(TaggedValue<IAction<T1,T2,T3>> action in this.ToArray()){
			if(action.value!=null&&action.value.IsSafeToInvoke){
				action.value.Invoke(arg1,arg2,arg3);
			}
		}
	}
	public void Add(IAction<T1,T2,T3> action){
		this.Add(new TaggedValue<IAction<T1,T2,T3>>(action));
	}
	public void Add(Action<T1,T2,T3> action){
		this.Add(new TaggedValue<IAction<T1,T2,T3>>(new RawAction<T1,T2,T3>(action)));
	}
}
public class TaggedPseudoEvent<T1,T2,T3,T4> : List<TaggedValue<IAction<T1,T2,T3,T4>>>,IPseudoEvent
{
	public TaggedPseudoEvent(){}
	public TaggedPseudoEvent(IEnumerable<TaggedValue<IAction<T1,T2,T3,T4>>> collection):base(collection){}
	public void InvokeAll(T1 arg1,T2 arg2,T3 arg3,T4 arg4){
		if(arg1 is IPseudoEventArg) ((IPseudoEventArg)arg1).SetPseudoEvent(this);
		if(arg2 is IPseudoEventArg) ((IPseudoEventArg)arg2).SetPseudoEvent(this);
		if(arg3 is IPseudoEventArg) ((IPseudoEventArg)arg3).SetPseudoEvent(this);
		if(arg4 is IPseudoEventArg) ((IPseudoEventArg)arg4).SetPseudoEvent(this);
		foreach(TaggedValue<IAction<T1,T2,T3,T4>> action in this.ToArray()){
			if(action.value!=null&&action.value.IsSafeToInvoke){
				action.value.Invoke(arg1,arg2,arg3,arg4);
			}
		}
	}
	public void Add(IAction<T1,T2,T3,T4> action){
		this.Add(new TaggedValue<IAction<T1,T2,T3,T4>>(action));
	}
	public void Add(Action<T1,T2,T3,T4> action){
		this.Add(new TaggedValue<IAction<T1,T2,T3,T4>>(new RawAction<T1,T2,T3,T4>(action)));
	}
}
