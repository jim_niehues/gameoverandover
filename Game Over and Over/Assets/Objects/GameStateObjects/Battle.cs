using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GamePlayEffect;


public class BattleScopedCombatantData{
	public int Heat;
}

public class Battle {

	public Dictionary<Combatant,BattleScopedCombatantData> CombatantData = new Dictionary<Combatant, BattleScopedCombatantData>();
	private List<Combatant> combatants = new List<Combatant>();
	
	//public List<BattleWideEffects> battleWideEffects = List<BattleWideEffects>();

		
	
	public IEnumerable<Combatant> GetAllCombatants(){
		return combatants;
	}
	public IEnumerable<Combatant> GetEnemiesOf(Combatant combatant){
		List<Combatant> enemies = new List<Combatant>();
		foreach(Combatant potentialEnemy in GetAllCombatants()){
			if(potentialEnemy.IsHostileTo(combatant)){
				enemies.Add(potentialEnemy);
			}
		}
		return enemies;
	}
	public IEnumerable<Player> GetAllPlayers(){
		List<Player> players = new List<Player>();
		foreach(Combatant potentialPlayer in GetAllCombatants()){
			if(potentialPlayer is Player){
				players.Add(potentialPlayer as Player);
			}
		}
		return players;
	}

	public void JoinBattle(Combatant combatant){
		combatants.Add(combatant);
		/*
		if(combatant is EnemyInstance){
			enemies.Add(combatant as EnemyInstance);
		}else if (combatant is PlayerInstance){
			players.Add(combatant as PlayerInstance);
		}else{
			throw new System.Exception("unhandled combatant type");
		}
		*/
	}
	public static Battle Merge(IEnumerable<Battle> battles){
		Battle mergedBattle = new Battle();
		foreach(Battle battle in battles){
			mergedBattle.combatants.AddRange(battle.combatants);
			foreach(KeyValuePair<Combatant, BattleScopedCombatantData> pair in mergedBattle.CombatantData){
				mergedBattle.CombatantData[pair.Key] = pair.Value;
			}
		}
		return mergedBattle;
	}
	public class Collection{
		private Dictionary<Combatant,Battle> dataByCombatant;
		private Dictionary<Battle,HashSet<Combatant>> dataByBattle;
		public readonly BattleHeatLookupClass BattleHeatLookup = new BattleHeatLookupClass();
		
		
		public void ExitBattle(Combatant combatant){
			Battle battle = GetBattleContaining(combatant);
			if(battle!=null) dataByBattle[battle].Remove(combatant);
			dataByCombatant.Remove(combatant);
			BattleHeatLookup.Remove(combatant);
		}
		public void EndBattle(Battle battle){
			if(battle != null){
				foreach(Combatant combatant in dataByBattle[battle]){
					dataByCombatant.Remove(combatant);
					BattleHeatLookup.Remove(combatant);
				}
				dataByBattle.Remove(battle);
			}
		}
		public void StartBattleWith( HashSet<Combatant> combatants){
			Battle battle = null;
			foreach(Combatant combatant in combatants){
				Battle battleCombatantIsEngagedIn = GetBattleContaining(combatant);
				if(battleCombatantIsEngagedIn!=null){
					if(battle==null){
						battle = battleCombatantIsEngagedIn;
					}else if(battle != battleCombatantIsEngagedIn){
						battle = MergeBattles(battle,battleCombatantIsEngagedIn);
					}
				}
			}
			if(battle == null){
				battle = new Battle();
				dataByBattle[battle] = combatants;
			}else{
				dataByBattle[battle].UnionWith(combatants);
			}
			foreach(Combatant combatant in combatants){
				dataByCombatant[combatant] = battle;
			}
		}
		public Battle MergeBattles(Battle a, Battle b){
			Battle battle = new Battle();
			dataByBattle[battle] = new HashSet<Combatant>();
			dataByBattle[battle].UnionWith(dataByBattle[b]);
			dataByBattle[battle].UnionWith(dataByBattle[a]);

			foreach(Combatant combatant in dataByBattle[battle]){
				dataByCombatant[combatant] = battle;
			}
			dataByBattle.Remove(a);
			dataByBattle.Remove(b);
			return battle;
		}
		/*public void SplitBattle(Battle){
		
	}*/
		public Battle GetBattleContaining(Combatant combatant){
			Battle battle;
			if(dataByCombatant.TryGetValue(combatant, out battle)){
				return battle;
			}
			return null;
		}
	}
	public class BattleHeatLookupClass{
		private Dictionary<Combatant,int> data = new Dictionary<Combatant, int>();
		public int this [Combatant combatant]{
			get{
				if(!data.ContainsKey(combatant)){
					return 0;
				}else{
					return data[combatant];
				}
			}
			set{
				data[combatant] = value;
			}
		}
		public void Remove(Combatant combatant){
			data.Remove(combatant);
		}
	}
}
