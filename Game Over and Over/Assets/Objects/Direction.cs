﻿using UnityEngine;
using System.Collections;

public enum Direction{Up=0,Right=1,Down=2,Left=3}

public static class DirectionExtensions{
	public static Direction GetOpposite(this Direction dir){
		switch (dir){
		case Direction.Up:
			return Direction.Down;
		case Direction.Down:
			return Direction.Up;
		case Direction.Left:
			return Direction.Right;
		case Direction.Right:
			return Direction.Left;
		}
		throw new System.NotImplementedException("Direction."+dir.ToString() + " was not implemented in DirectionExtensions.GetOpposite()");
	}
	public static IntCoord ToIntCoord(this Direction dir){
		switch (dir){
		case Direction.Up:
			return new IntCoord(0,1);
		case Direction.Down:
			return new IntCoord(0,-1);
		case Direction.Left:
			return new IntCoord(-1,0);
		case Direction.Right:
			return new IntCoord(1,0);
		}
		throw new System.NotImplementedException("Direction."+dir.ToString() + " was not implemented in DirectionExtensions.ToIntCoord()");
	}
	/*
	public static readonly Direction Up = new IntCoordDirection(Enum.Up);
	public static readonly Direction Down = new IntCoordDirection(Enum.Down);
	public static readonly Direction Left = new IntCoordDirection(Enum.Left);
	public static readonly Direction Right = new IntCoordDirection(Enum.Right);
	
	static Direction(){



		
		Up.direction = Enum.Down;
		Down.direction = Enum.Up;
		Left.direction = Enum.Right;
		Right.direction = Enum.Left;


		Up.Opposite = Down;
		Down.Opposite = Up;
		Left.Opposite = Right;
		Right.Opposite = Left;

		Up.coord = new IntCoord(0,1);
		Down.coord = new IntCoord(0,-1);
		Left.coord = new IntCoord(-1,0);
		Right.coord = new IntCoord(1,0);



	}
	*/
	/*
	private Direction(){
	}

	public static implicit operator IntCoord(Direction a){
	{
		return a.coord;
	}
	public class Serializable{
		
	}
*/
}
