using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using System.Linq;

[Serializable]
public class LevelMap : Map<LevelTile>{
	[SerializeField]
	private LevelMapAsset asset;
	public LevelMapAsset Asset{
		get{
			return asset;
		}

	}
	
	public static LevelMap CreateInstance(LevelMapPrefab prefab){
		LevelMap instance = new LevelMap();
		if(prefab==null) throw new System.ArgumentNullException();
		if(prefab.Asset ==null) throw new System.ArgumentNullException("prefab.Asset cannot be null");

		instance.asset = prefab.Asset;
		return instance;
	}
	public static void Populate(LevelMapPrefab prefab,LevelMap spawn){
		foreach(KeyValuePair<IntCoord,LevelTilePrefab> kvp in prefab){
			LevelTile lt = kvp.Value.Spawn(unchecked( kvp.GetHashCode()+prefab.Asset.LevelMapReference.Instance.ID.GetHashCode()));
			//Debug.Log(kvp.Key.ToString());
			kvp.Value.Populate(lt);
			spawn[kvp.Key] = lt;
		}
		foreach(LevelTile lt in spawn.Values){
			lt.OnSpawn.InvokeAll(new OnSpawnArgs(lt));
		}
		/*foreach(LevelTile lt in instance.Values){
			foreach(Instance<ILocated> entity in lt.Entities){
				entity.OnSpawn.InvokeAll(new OnSpawnArgs(entity));
			}
     		}*/
	}
	public LevelMap(){
	}
	/*public LevelMap(){
		
	}*/
	static GameObject _mapRoot;
	public static GameObject MapRoot{
		get{
			if(_mapRoot==null){
				_mapRoot = new GameObject("LevelMapRoot");
				_mapRoot.transform.parent = OverWorldManager.inst.transform;
			}
			return _mapRoot;
		}
	}
	static LevelMapAsset activeMapAsset;
	[ShowInInspector]
	public static LevelMapAsset ActiveMapAsset{
		get{
			if(activeMapAsset !=null){
				return activeMapAsset;
			}else return null;
		}
	}
	public static LevelMap ActiveMap{
		get{
			if(ActiveMapAsset !=null){
				return activeMapAsset.LevelMapReference.Instance.Entity;
			}else return null;
		}
		/*set{
			if(activeMap!=value){
				if(value!=null){
					Debug.Log("changing Map to "+value.ToString());

					if(value.Asset!=null){
						Debug.Log("changing Map to "+value.Asset.name);
					}
				}
				UnloadAll();
				activeMap=value;
				if(activeMap!=null){
					activeMap.LoadIntoScene();
				}
			}
		}*/
	}
	public static void SetActiveMap(LevelMapAsset asset){
		if(activeMapAsset!=asset){
			if(activeMapAsset!=null) UnloadAll();
			if(asset == null) {
				activeMapAsset = null;
			}else{
				activeMapAsset = asset;
				
				asset.LevelMapReference.Instance.Entity.LoadIntoScene();
			}
		}
	}
	public override LevelTile this[IntCoord coord]
	{
		get{
			return base[coord];
		}
		set{
			if( !Application.isPlaying && (value == null || ( (value.Entities == null || value.Entities.Length == 0) && (value.tile==null) ))){
				if( base.ContainsKey(coord)) base.Remove(coord);
			}
			else{
				base[coord] = value;
				if(Asset == null){
					throw new System.Exception("Asset can't be null");
				}else{
					Location thisLocation = new Location(this,coord);
					if(value.Location != thisLocation){
						value.Location = thisLocation;
					}
					if(value.Location!=thisLocation){
						throw new System.Exception("Error while adding "+value.GetType()+" to this Location it still belongs to another Location");
					}
				}
			}
		}
	}
	public static void UnloadAll(){
		Debug.Log("Unloading Map");
		if(_mapRoot!=null){
			foreach(Transform child in _mapRoot.transform){
				child.gameObject.Unload();
			}
		}
		activeMapAsset = null; 
	}
	void LoadIntoScene(){
		Debug.Log("Loading Map");
		foreach(LevelTile lt in this.Values){
			lt.LoadIntoScene();
		}
	}
}
	/*
	[Serializable]
	Dictionary<IntCoord,ITile> iTiles = new Dictionary<IntCoord, ITile>();
	[Serializable]
	Dictionary<IntCoord,IEntityOnTile[]> iEntitiesOnTiles = new Dictionary<IntCoord, IEntityOnTile[]>();

	public bool ContainsKey(IntCoord coord){
		return !this[coord].;
	}
	public LevelTile this[IntCoord coord]
	{
		get{
			LevelTile lt = new LevelTile();
			if(iTiles.ContainsKey(coord)) lt.tile = iTiles[coord];
			else lt.tile = null;

			if(iEntitiesOnTiles.ContainsKey(coord))lt.entities = iEntitiesOnTiles[coord];
			else lt.tile = null;
			return lt;
		}
		set{
			if(value.tile == null){
				if(iTiles.ContainsKey(coord))  iTiles.Remove(coord);
			}else{
				iTiles[coord] = value.tile;
			}
			if(value.entities == null){
				if(iEntitiesOnTiles.ContainsKey(coord))  iEntitiesOnTiles.Remove(coord);
			}else{
				iEntitiesOnTiles[coord] = value.entities;
			}
		}
	}*/

/*
#if UNITY_EDITOR
	[NotSerialized]
	IntCoord? selected = null;

	bool SelectionMade{
		get{return selected.HasValue;}
	}

	[ShowInInspector]
	[InspectorShowIf("SelectionMade")]
	Tile SelectedTile{
		get{
			if(selected.HasValue){
				if(this.ContainsKey(selected.Value)){
					return this[selected.Value];
				}else{
					return null;
				}
			}else{
				return null;
			}
		}
		set{
			if(selected.HasValue){
				this[selected.Value] = value;
			}
		}
	}
#endif
	//IEntityInRoom 

}
*/