using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class AttackDamage : Dictionary<ElementType, int>{
	public new int this[ElementType type]{
		get{
			if(base.ContainsKey(type)){
				return base[type];
			}else{
				return 0;
			}
		}
		set{
			if(value <=0){
				if(base.ContainsKey(type)){
					base.Remove(type);
				}
			}else{
				base[type] = value;
			}
		}
	}
	public AttackDamage():base(){
	}

	public AttackDamage(ElementType type,int amount):base(){
		this.Add(type,amount);
		this[type] = amount;
	}
	public new void Add(ElementType type, int amount){
		this[type] = amount;
		Debug.Log("attack contains "+amount +" "+Enum.GetName(typeof(ElementType),type)+" damage");
	}
	public static AttackDamage operator +(AttackDamage attackDamage1, AttackDamage attackDamage2){
		AttackDamage attackDamageResult = new AttackDamage();
		foreach(ElementType type in Enum.GetValues(typeof(ElementType))){
			attackDamageResult[type] = attackDamage1[type] + attackDamage2[type];
		}
		return attackDamageResult;
	}
}

