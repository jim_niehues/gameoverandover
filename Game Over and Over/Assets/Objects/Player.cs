using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using System.Linq;

public class PlayerPrefab {
	public GameObject prefab;
}
[System.Serializable]
public class PlayerSpawner : ISpawns<Player>{
	[HideInInspector]
	[SerializeField]
	int lifeNumber;

	[HideInInspector]
	[SerializeField]
	int seed;

	public PlayerSpawner(int lifeNumber, int seed){
		this.lifeNumber = lifeNumber;
		this.seed = seed;
	}
	public Player Spawn(int seed){
		Player p = new Player(lifeNumber,10,seed);
		p.gameobjectreference.prefab =  Game.Inst.playerPrefab.prefab;
		return p;
	}
	public void Populate(Player spawn){

	}
}

public class Player : Combatant,IHasGameObject{
	public int lifeNumber;
	public int EXP_Used;
	public int heat;
	
	public readonly List<ICombatAbility> combatAbilities = new List<ICombatAbility>();
	public readonly List<ICombatPassive> combatPassives = new List<ICombatPassive>();

	public IEquipmentGauntletLeft 		LeftGauntlet{get;set;}
	public IEquipmentGauntletRight 		RightGauntlet{get;set;}
	public IEquipmentArmor 				Armor{get;set;}
	public IEquipmentCastingRifle 		CastingRifle{get;set;}
	public IEquipmentPotionBelt 		PotionBelt{get;set;}
	public IEquipmentBackpack 			Backpack{get;set;}
	public IEquipmentRunes 				Runes{get;set;}

	/*
	public int BattleTemp{
		get{
			return _heat;
		}
		set{

		}
	}
	*/
	public int Charges;
	/*
	public void AdjustBattleTemp(int adjustBy){
		
	}
	public void SetBattleTemp( int setTo){

		_heat = setTo
	}*/

	public bool IsActiveCharacter{
		get{
			Save s = Save.LoadedSave;
			return (this.lifeNumber == s.OnLifeNumber);
		}
	}
	/*public override Location Location{
		get{ return base.Location;}
		set{
			base.Location = value;
			if(this == Save.LoadedSave.ActiveCharacter.Entity) Camera.main.transform.position = new Vector3(value.coord.x,value.coord.y,Camera.main.transform.position.z);

			//((IMovable)this).MoveTo(value);
		}
	}*/
	public Player(int lifeNumber, int hp, int seed){
		this.RNG_Seed = seed;
		this.lifeNumber = lifeNumber;
		dead = false;
		HP = hp;
		NameOverride = "Life "+lifeNumber;
	}
	public override void UpdateGameObject ()
	{
		base.UpdateGameObject ();
	}
	public override void OnDeath (OnDeathArgs args){
		base.OnDeath (args);
		if(IsActiveCharacter){
			Save.LoadedSave.OnPlayerDeath();
		}
	}
	
	public OnTurnArgs? onTurnArgs;
	public OnUseArgs? onUseArgs;/*{
		get{
			if(onTurnArgs.HasValue){
				return new OnUseArgs(onTurnArgs.Value.time,onTurnArgs.Value.turnNumber,onTurnArgs.Value.combatant,onTurnArgs.Value.seed);
			}else return null;
		}
	}*/

	public override IEnumerator OnTurn (decimal currentTime){
		if(Save.LoadedSave.ActiveCharacter != Instance.Get(this)){
			//CoolDown+=Tick.length;
			yield break;
		}

		OnTurnArgs args = new OnTurnArgs(currentTime,turnNumber,Instance.Get<Combatant>(this),turnSeed);
		onTurnArgs = args;
		if(Game.Inst.GameStateStack.ContainsType<GameState.Combat>()){


			onUseArgs = new OnUseArgs(args);
			ICombatAbility[] abilitiesFiltered = combatAbilities.Where( a=>a!=null&&a.CanBeUsedNow.IsSafeToInvoke && a.CanBeUsedNow.Invoke(onUseArgs.Value) && a.OnUse.IsSafeToInvoke ).ToArray();
			if(abilitiesFiltered.Length == 0){
				Debug.LogWarning("no valid actions... waiting instead");
				args.combatant.Entity.CoolDown+=Tick.length;
				yield break;
			}

		}else{
			OverWorldManager.inst.AllowInput = true;
			
		}
		/*while(Save.LoadedSave.ActiveCharacter.Entity.IsTurn(Save.LoadedSave.CurrentT)){
			yield return null;
		}*/
		//OverWorldManager.inst.AllowInput = false;
	}
}

