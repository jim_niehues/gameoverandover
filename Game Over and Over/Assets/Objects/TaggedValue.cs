﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public struct TaggedValue<T>{
	public readonly List<System.Object> tags;
	public readonly T value;
	public TaggedValue(T value, params System.Object[] tags){
		this.value = value;
		this.tags = new List<System.Object>();
		if(tags!=null){
			this.tags.AddRange(tags);
		}
	}
	public static implicit operator TaggedValue<T>(T value)
	{
		return new TaggedValue<T>(value);
	}
	/*public static IEnumerable<T> CastTaggedTo(IEnumerable<TaggedValue<T>> collection)
	{
		IEnumerable<T> castCollection = collection.Select<TaggedValue<T>,T>(tv=>tv.value);
		return castCollection;
	}*/

}
