﻿using UnityEngine;
using System;
using System.Collections;

public class ValueOrReturnsValue<T> {
		
	private Func<T> GetValue;
	private T value;
	//public readonly bool allowReturnValuesToVary = false;
	//bool valueSet = false;
	public T Value{
		get {
			return GetValue();
			//T valueGotten = GetValue();
			//if (!valueSet){
			//	value = valueGotten;
			//	valueSet = true;
			//}
			//if (!allowReturnValuesToVary && !valueGotten.Equals(value)) throw new Exception("return Values vary over time");
			//return value;
		}
	}
	public ValueOrReturnsValue(Func<T> ReturnsValue) {
		GetValue = ReturnsValue;
	}
	public ValueOrReturnsValue(T value) {
		GetValue = delegate(){ return value; };
	}
	//failsafe incase value changes but is never called on
	/*~ValueOrReturnsValue() {
		T valueGotten = GetValue();
		if (!valueSet){
			value = valueGotten;
			valueSet = true;
		}
		if (!valueGotten.Equals(value)) throw new Exception("return Values vary over time");
	}*/
	public static implicit operator ValueOrReturnsValue<T>(T t) {
		return new ValueOrReturnsValue<T>(t);
	}
	public static implicit operator ValueOrReturnsValue<T>(Func<T> t) {
		
		return new ValueOrReturnsValue<T>(t);
	}
	public static implicit operator T(ValueOrReturnsValue<T> t) {
		
		return t.Value;
	}
}
