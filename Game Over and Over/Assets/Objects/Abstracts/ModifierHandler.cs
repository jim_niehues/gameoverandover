
using System;
using System.Collections.Generic;
public class ModifierHandler<T>
{

	Dictionary<Modifier.Priority, Dictionary<Modifier<T>,int>> modifiers = new Dictionary<Modifier.Priority, Dictionary<Modifier<T>,int>>();

	public ModifierHandler ()
	{


	}
	public void Add(Modifier<T> modifier){
		if(!modifiers.ContainsKey(modifier.priority)){
			modifiers[modifier.priority] = new Dictionary<Modifier<T>, int>();
		}
		if(!modifiers[modifier.priority].ContainsKey(modifier)){
			modifiers[modifier.priority].Add(modifier,0);
		}
		modifiers[modifier.priority][modifier]++;
	}
	public void Remove(Modifier<T> modifier){
		if(modifiers[modifier.priority].ContainsKey(modifier)){
			if(modifiers[modifier.priority][modifier]>1){
				modifiers[modifier.priority][modifier]--;
			}else{
				modifiers[modifier.priority].Remove(modifier);
			}
		}
	}

	public T ApplyTo(T value){
		foreach(Modifier.Priority priority in Enum.GetValues(typeof(Modifier.Priority))){
			if(modifiers.ContainsKey(priority)){
				foreach(KeyValuePair<Modifier<T>,int> kvp in modifiers[priority]){
					for(int i=0;i<kvp.Value;i++){
						value = kvp.Key.ApplyTo(value);
					}
				}
			}
		}
		return value;
	}

	/*
	// Helper Methods
	public static ModifierHandler operator+(XY a, XY b)
	{
		return new XY(a.x+b.x, a.y+b.y);
	}
	
	public static ModifierHandler operator-(XY a, XY b)
	{
		return new XY(a.x-b.x, a.y-b.y);
	}
	*/

}

