using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class Modifier{
	public enum Priority{Add=1,Mult=2}
}
public abstract class Modifier<T>{
	
	public readonly Modifier.Priority priority;
	public readonly object AddedBy;
	public Modifier(object AddedBy, Modifier.Priority priority){
		this.priority = priority;
		this.AddedBy = AddedBy;
	}
	
	public abstract T ApplyTo(T value);
	public abstract bool ShouldRemoveNow();
}
public class ModifierDelegate<T>:Modifier<T>{
	
	public readonly Func<T,T> modifierMethod;
	public readonly Func<bool> removalTrigger;

	public ModifierDelegate(object AddedBy, Modifier.Priority priority, Func<T,T> modifierMethod, Action<bool> shouldRemoveNowMethod):base(AddedBy,priority){
		this.modifierMethod = modifierMethod;
	}
	
	public override T ApplyTo(T value){
		if(modifierMethod!=null) value = modifierMethod(value);
		return value;
	}
	
	public override bool ShouldRemoveNow(){
		if(removalTrigger != null) return removalTrigger();
		else return false;
	}
}
/*
public class MultModifier<T>: Modifier<T> {
	Modifier.Priority priority;
	
	public MultModifier(Func<T,T> modifierMethod):base(Modifier.Priority.Mult,modifierMethod){
		this.priority = priority;
	}
	
	public virtual T ApplyTo(T @value){
		return @value;
	}
}
*/

