
using System;
using System.Reflection;
//using System.Reflection.Emit;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public interface IResettable{
	//void SetReset();
	//void Reset();

	//object Value{get;set;}
}
[System.AttributeUsage(System.AttributeTargets.Field |
                       System.AttributeTargets.Property
                       //|
                       //System.AttributeTargets.Method
                       )
 ]
public class Resettable : System.Attribute
{
}
	
[System.AttributeUsage(System.AttributeTargets.Method)]
public class ResettableExtensionMockProperty : System.Attribute
{
	string key;

	public string Key{
		get{
			return key;
		}
		private set{
			key = value;
		}
	}

	public ResettableExtensionMockProperty(string key):base(){
		this.key = key;
	}

	public class MockProperty{
		public readonly MethodInfo get;
		public readonly MethodInfo set;

		public MockProperty(MethodInfo get,MethodInfo set){
			StringBuilder errorString = new StringBuilder();

			//getter validation
			if(get.GetParameters().Length>0) errorString.AppendLine("Getter must have no parameters");
			Type returnType = get.ReturnType;
			if(returnType == typeof(void)) errorString.AppendLine("Getter must return a value");
			
			//setter validation
			ParameterInfo[] parameters = set.GetParameters();
			if(parameters.Length !=1) errorString.AppendLine("Setter must have exactly one parameter");
			
			if(parameters.Length<1 || parameters[0].ParameterType != returnType) errorString.AppendLine("Setter method's first(and only)parameter must be of the same type as the return value of getter method");
			if(errorString.Length>0){
				throw new Exception(errorString.ToString());
			}

			this.get = get;
			this.set = set;
		}

	}
}
	
	
public static class ResettableExtensions
{
		
	public static Dictionary<IResettable,Dictionary<string,object>> values = new Dictionary<IResettable, Dictionary<string, object>>();
	public static Dictionary<IResettable,Dictionary<string,object>> mockValues = new Dictionary<IResettable, Dictionary<string, object>>();

	public static Dictionary<Type,Dictionary<string,ResettableExtensionMockProperty.MockProperty>> mockProperties = new Dictionary<Type, Dictionary<string, ResettableExtensionMockProperty.MockProperty>>();

	public static void SetResetValues(this IResettable resettableObject){
		if(!values.ContainsKey(resettableObject)){
			values[resettableObject] = new Dictionary<string, object>();
		}
		foreach(MemberInfo mInfo in resettableObject.GetType().GetMembers()){
			if(mInfo.GetCustomAttributes(typeof(Resettable),true).Length>0){
				FieldInfo fieldInfo = mInfo as FieldInfo;
				if(fieldInfo != null){
					try{
						object o = fieldInfo.GetValue(resettableObject);
						values[resettableObject][fieldInfo.Name] = o;
					}
					catch{}
				}
				PropertyInfo propInfo = mInfo as PropertyInfo;
				if(propInfo != null){
					try{
						object o = propInfo.GetValue(resettableObject,null);
						values[resettableObject][propInfo.Name] = o;
					}
					catch{}
				}
			}
		}
		/*
		//ExtensionMethods
		if(!mockProperties.ContainsKey(resettableObject.GetType())){
			mockProperties[resettableObject.GetType()] = new Dictionary<string, ResettableExtensionMockProperty.MockProperty>();
			Dictionary<string,MethodInfo> getters = new Dictionary<string, MethodInfo>();
			Dictionary<string,MethodInfo> setters = new Dictionary<string, MethodInfo>();

			IEnumerable<MethodInfo> extensionMethods = Extension.GetExtensionMethods(typeof(IResettable).Assembly,resettableObject.GetType());
			foreach(MethodInfo extMethod in extensionMethods){
				Debug.Log(extMethod.Name);
			}
			foreach (MethodInfo extensionMethod in extensionMethods){
				object[] attributes = extensionMethod.GetCustomAttributes(typeof(ResettableExtensionMockProperty),true);
				foreach(object attributeObject in attributes){
					ResettableExtensionMockProperty attribute = attributeObject as ResettableExtensionMockProperty;
					if(IsGetter(extensionMethod)){
						if(getters.ContainsKey(attribute.Key)){
							throw new Exception(attribute.Key + " already has a Getter");
						}
						if(setters.ContainsKey(attribute.Key)){
							if(setters[attribute.Key].GetParameters()[1].ParameterType != extensionMethod.ReturnType){
								throw new Exception(attribute.Key + " has a type mismatch issue");
							}
						}
						getters[attribute.Key] = extensionMethod;
					}
					if(IsSetter(extensionMethod)){
						if(setters.ContainsKey(attribute.Key)){
							throw new Exception(attribute.Key + " already has a Setter");
						}
						if(getters.ContainsKey(attribute.Key)){
							if(getters[attribute.Key].ReturnType != extensionMethod.GetParameters()[1].ParameterType){
								throw new Exception(attribute.Key + " has a type mismatch issue");
							}
						}
						setters[attribute.Key] = extensionMethod;
					}
				}
			}
			HashSet<string> goodKeys = new HashSet<string>(getters.Keys);
			goodKeys.IntersectWith(setters.Keys);
			HashSet<string> badKeys = new HashSet<string>(getters.Keys);
			badKeys.SymmetricExceptWith(setters.Keys);

			foreach(string key in goodKeys){
				mockProperties[resettableObject.GetType()][key] = new ResettableExtensionMockProperty.MockProperty(getters[key],setters[key]);
			}
			foreach(string key in badKeys){
				throw new Exception(key + " lacks a valid 2nd method");
			}
		}
		foreach(string key in mockProperties[resettableObject.GetType()].Keys){
			ResettableExtensionMockProperty.MockProperty mockProperty = mockProperties[resettableObject.GetType()][key];
			mockValues[resettableObject][key] = mockProperty.get.Invoke(resettableObject,new object[]{resettableObject});
		}*/

	}
	static bool IsGetter(MethodInfo methodInfo){
		return (methodInfo.GetParameters().Length ==1 && methodInfo.ReturnType != typeof(void));
	}
	static bool IsSetter(MethodInfo methodInfo){
		return (methodInfo.GetParameters().Length==2 && methodInfo.ReturnType == typeof(void));
	}

	public static void ResetAllValues(){
		foreach(IResettable resettableObject in values.Keys){
			resettableObject.ResetValues();
		}
		
		//Debug.Log("Reseting Complete");
	}

	public static void ResetValues(this IResettable resettableObject){
		Debug.Log("Reseting object "+resettableObject.GetType().FullName);
		foreach(MemberInfo mInfo in resettableObject.GetType().GetMembers()){
			if(mInfo.GetCustomAttributes(typeof(Resettable),true).Length>0){
				
				Debug.Log("Attempting to reset "+mInfo.Name);
				FieldInfo fieldInfo = mInfo as FieldInfo;
				if(fieldInfo != null){
					
					Debug.Log("Reseting "+fieldInfo.Name);
					try{
						fieldInfo.SetValue(resettableObject,values[resettableObject][fieldInfo.Name]);
					}
					catch{}
				}
				
				PropertyInfo propInfo = mInfo as PropertyInfo;
				if(propInfo != null){
					Debug.Log("Reseting "+propInfo.Name);
					try{
						propInfo.SetValue(resettableObject,values[resettableObject][propInfo.Name],null);
					}
					catch{}
				}
				/*
	EventInfo eventInfo = mInfo as EventInfo;
	if(eventInfo != null){
	try{
		eventInfo.
		propInfo.SetValue(resettableObject,values[resettableObject][propInfo.Name],null);
	}
	catch{}
	}
	*/
				
			}
		}
		/*
		foreach(string key in mockProperties[resettableObject.GetType()].Keys){
			ResettableExtensionMockProperty.MockProperty mockProperty = mockProperties[resettableObject.GetType()][key];
			mockProperty.set.Invoke(resettableObject,new object[]{resettableObject,mockValues[resettableObject][key]});
			//Debug.Log("Reseting "+key);
		}*/
		//Debug.Log("Reseting Complete");
	}
}


	

