using UnityEngine;
using System.Collections;
using System.Collections.Generic;




public interface IEntityOnTile{
	Color Color{get;}
}
/*
public class Dungeon {
	public static Dungeon ActiveDungeon;
	public int xSize = 20;
	public int ySize = 20;
	private ITile[,] tiles;
	
	public ITile this[int x, int y] {
		get {
			if(tiles.GetLength(0)<=x || x<0||tiles.GetLength(1)<=y || y<0) Debug.LogError("invalid tile IntCoord");
			return tiles[x,y];
		}
	}

	public ITile this[IntCoord coord]{
		get{
			return this[ coord.x, coord.y];
		}
	}

	//Dictionary<IntCoord,IEntityInRoom> objectsInRoom = new Dictionary<IntCoord, IEntityInRoom>();
	//Dictionary<IntCoord,IEntityInRoom> CollidableEntities = new Dictionary<IntCoord, IEntityInRoom>();

	public bool isDirty=true;


	public Dungeon(){
		tiles = new ITile[xSize,ySize];
		IntCoord coord = new IntCoord();
		for(int i=0;i<xSize*ySize;i++){
			coord.x = i%xSize;
			coord.y = Mathf.FloorToInt((float)i/(float)xSize);
			
			//sqeezing in a test room builder construction
			{

				if((coord.x==0||coord.x==xSize-1)||(coord.y==0||coord.y==ySize-1)){
					if(coord.y == 5 && (coord.x==0||coord.x==xSize-1)){
						IntCoord destination = coord;
						if(coord.x==0){
							destination.x = xSize-2;
						}else if(coord.x==xSize-1){
							destination.x = 1;
						}
						tiles[coord.x,coord.y] = new Door(new Location(this,destination));
					}else{
						tiles[coord.x,coord.y] = new Wall();
					}
				}else if(coord.x==5&&coord.y==5){
					tiles[coord.x,coord.y] = new EnemyTile();
				}else{
					tiles[coord.x,coord.y] = new OpenSpace();
				}
			}
		}
	}


}
*/
