﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using FullSerializer;
[Serializable]
public class Level {

	[SerializeField]
	public  LevelMap Map;// = new LevelMap();

}

[Serializable]
public class PositionedTile{
	[SerializeField]
	public IntCoord key;
	[SerializeField]
	public TileInfo value;
	public PositionedTile(IntCoord key, TileInfo value){
		this.key=key;
		this.value = value;
	}
}
[Serializable]
public class TileInfo{
	[SerializeField]
	public int asdf;

}