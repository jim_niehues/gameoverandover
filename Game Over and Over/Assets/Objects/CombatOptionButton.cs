﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;


public class CombatOptionButton : MonoBehaviour {

	
	public Button button;
	public Text text;
	public ICombatAbility ability;


	// Use this for initialization
	void Awake () {
		button = GetComponentInChildren<Button>();
		text = GetComponentInChildren<Text>();
	}

	public void ResetTo(ICombatAbility ability,OnUseArgs args){
		this.ability = ability;
		text.text = ability.Name;
		button.onClick.RemoveAllListeners();
		button.interactable = ability.CanBeUsedNow.Invoke(args);
		//button.onClick.AddListener(ability.OnUse(args)
	}

}
