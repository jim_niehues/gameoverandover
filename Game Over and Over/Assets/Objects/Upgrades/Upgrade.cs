﻿using UnityEngine;
using System.Collections;

public abstract class Upgrade {
	public BranchOfStudy branch;
	public int level;

	//Passive_Effect[] effect;

	public System.Delegate Apply;
	
}
