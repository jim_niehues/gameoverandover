﻿using UnityEngine;
using System.Collections;
using FullInspector;

public class Book : BaseScriptableObject{
	public readonly string name;
	public readonly string description;
	public readonly string lore;
	public readonly int EXP;
}
