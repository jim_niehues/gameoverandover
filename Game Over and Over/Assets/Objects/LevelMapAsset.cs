﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using System.Linq;

public class LevelMapAsset : BaseScriptableObject{
	[SerializeField]
	[HideInInspector]
	private Reference<LevelMapPrefab, LevelMap> _levelMapReference = new Reference<LevelMapPrefab, LevelMap>();
	
	[ShowInInspector]
	public Reference<LevelMapPrefab, LevelMap> LevelMapReference{
		get{
			if(_levelMapReference==null) _levelMapReference = new Reference<LevelMapPrefab, LevelMap>();
			if(_levelMapReference._prefab==null) _levelMapReference._prefab = LevelMapPrefab.CreateNewPrefab(this);
			_levelMapReference._prefab.Asset = this;
			return _levelMapReference;
		}
		set{
		}

	}

	public LevelMap UnsafeLevelMap{
		get{
			if(_levelMapReference==null) _levelMapReference = new Reference<LevelMapPrefab, LevelMap>();
			return _levelMapReference.Instance.Entity;
		}
	}
	public LevelMapPrefab UnsafeLevelMapPrefab{
		get{
			if(_levelMapReference==null) _levelMapReference = new Reference<LevelMapPrefab, LevelMap>();
			return _levelMapReference._prefab as LevelMapPrefab;
		}
	}

}
[Serializable]
public class LevelMapPrefab : Map<LevelTilePrefab>, ISpawns<LevelMap>,IMap<LevelTilePrefab>{
	[SerializeField]
	private LevelMapAsset asset;
	[ShowInInspector]
	public LevelMapAsset Asset{
		get{
			return asset;
		}
		set{
			if(asset==null||asset==value){
				asset = value;
			}else{
				throw new System.Exception("LevelMapPrefab.Asset cannot be changed after it has been set");
			}
		}
	}

	public override LevelTilePrefab this[IntCoord coord]
	{
		get{
			if(dict.ContainsKey(coord)){
				return dict[coord];
			}else{
				return new LevelTilePrefab();
			}
		}
		set{
			if(
				(
					value == null 
					|| 
					( 
				 		(value.entities == null || value.entities.Count == 0) 
						 && (value.tileSpawner == null)
						 && (value.OnSpawn == null || value.OnSpawn.Count == 0)
						 && (value.OnEnter == null || value.OnEnter.Count == 0)
						 && (value.OnExit == null || value.OnExit.Count == 0)
						 && (value.OnStay == null || value.OnStay.Count == 0)
			 		)
				)
				&& !Application.isPlaying
			   ){
				if( base.ContainsKey(coord)) base.Remove(coord);
			}else{
				base[coord] = value;
			}
		}
	}
	public static LevelMapPrefab CreateNewPrefab(LevelMapAsset asset){
		LevelMapPrefab prefab = new LevelMapPrefab();
		if(asset.UnsafeLevelMapPrefab!=null) throw new System.ArgumentException("LevelMapAsset already contains a LevelMapPrefab");
		prefab.asset = asset;
		return prefab;
	}
	public LevelMap Spawn(int seed){
		LevelMap spawn = LevelMap.CreateInstance(this);
		return spawn;
	}
	public void Populate(LevelMap spawn){
		LevelMap.Populate(this,spawn);
	}
}
	/*
	[Serializable]
	Dictionary<IntCoord,ITile> iTiles = new Dictionary<IntCoord, ITile>();
	[Serializable]
	Dictionary<IntCoord,IEntityOnTile[]> iEntitiesOnTiles = new Dictionary<IntCoord, IEntityOnTile[]>();




	public bool ContainsKey(IntCoord coord){
		return !this[coord].;
	}
	public LevelTile this[IntCoord coord]
	{
		get{
			LevelTile lt = new LevelTile();
			if(iTiles.ContainsKey(coord)) lt.tile = iTiles[coord];
			else lt.tile = null;

			if(iEntitiesOnTiles.ContainsKey(coord))lt.entities = iEntitiesOnTiles[coord];
			else lt.tile = null;
			return lt;
		}
		set{
			if(value.tile == null){
				if(iTiles.ContainsKey(coord))  iTiles.Remove(coord);
			}else{
				iTiles[coord] = value.tile;
			}
			if(value.entities == null){
				if(iEntitiesOnTiles.ContainsKey(coord))  iEntitiesOnTiles.Remove(coord);
			}else{
				iEntitiesOnTiles[coord] = value.entities;
			}
		}
	}*/

/*
#if UNITY_EDITOR
	[NotSerialized]
	IntCoord? selected = null;

	bool SelectionMade{
		get{return selected.HasValue;}
	}

	[ShowInInspector]
	[InspectorShowIf("SelectionMade")]
	Tile SelectedTile{
		get{
			if(selected.HasValue){
				if(this.ContainsKey(selected.Value)){
					return this[selected.Value];
				}else{
					return null;
				}
			}else{
				return null;
			}
		}
		set{
			if(selected.HasValue){
				this[selected.Value] = value;
			}
		}
	}
#endif
	//IEntityInRoom 


}
*/