using UnityEngine;
using System.Collections;
using System.Collections.Generic;




namespace GamePlayEffect{
	public class EffectCollection{


	}



	/*
	public class ActiveAbility : ScriptableObject {
		public AbilityNode.Active activeAbilityNode;
	}
	public class PassiveAbility : ScriptableObject {
		public bool hidden;
		public AbilityNode.Passive passiveAbilityNode;
	}


	public abstract class EquipedEffect{
		Equipment equipment;
		CombatantInstance comabatant;
		Effect effect;
	}
	public class BattleWideEffect{
		//Apply

	}
	*/


	/*
	public abstract class EquipmentEffect : Effect{
		public abstract void OnEquip(CombatantInstance combatant);
		public abstract void OnUnEquip(CombatantInstance combatant);
	}

	public abstract class ActiveEquipmentEffect : EquipmentEffect, IActiveEffect{
		public abstract void OnUse();
		public abstract bool CanBeUsedNow();
	}
	*/




	/*public abstract class ActivatedEffect{
		public static EquipedEffect CreateNew<T>(){
			
		}
		
		public class Generic<T>:EquipedEffect{
			
		}
	}*/









/*	public class EffectApplication<T> : T {
		public Dictionary<object,object> applicationData;
		EffectNode effect;
	}*/

	/*public class ExecutionPriorityAttribute : System.Attribute{
		ExecutionPriority priority;
		public ExecutionPriorityAttribute(ExecutionPriority priority){
			this.priority = priority;
		}
	}*/
	namespace OnTick{
		public abstract class Handler{
			public abstract void Handle(Args args);
		}
		
		
		public class Args{
			
		}
	}
	namespace OnTurn{
		public abstract class Handler{
			public abstract void Handle(Args args);
		}
		public class Args{
			
		}
	}
	namespace OnEquip{
		public abstract class Handler{
			public abstract void Handle(Args args);
		}
		public class Args{
			Combatant combatant;
			Equipment equipment; 
		}
	}
	namespace OnUnEquip{
		public abstract class Handler{
			public abstract void Handle(Args args);
		}
		public class Args{
			Combatant combatant;
			Equipment equipment;

		}
	}
	/*namespace OnSetHeat{
		public interface Handler{
			void Handle(Args args);
		}
		
		
		public class Args{
			public int 
		}
    	}*/
	namespace OnAdjustHeat{
		public abstract class Handler{
			public abstract void Handle(Args args);
			
		}
		public class Args{
			Combatant combatant;
			public int adjustBy;
		}
	}
	namespace OnJoinBattle{
		public abstract class Handler{
			public abstract void Handle(Args args);
			
		}
		public class Args{
			Combatant combatant;
		}
	}
	/*
	[System.Serializable]
	public abstract class AbilityNode{
		private static Dictionary<System.Type,ExecutionPriority> ExecutionPriorityLookup = new Dictionary<System.Type, EffectExecutionPriorityAttribute>();
		public enum ExecutionPriority{}

		public virtual List<AbilityNode> Children{
			get{ return null;}
		}

		public abstract string GetEffectDescription();

		[System.Serializable]
		public abstract class Passive:AbilityNode{
			[System.Serializable]
			public class ConditionalCollection:Passive{
				
				public Conditional conditional;

				private List<Passive> children = new List<Passive>();

				public override List<AbilityNode> Children{
					get{ return children;}
				}
				public virtual void OnTurn(){}
				 
				public virtual void OnTick(){}
				public virtual int OnSetHeat(int value){}
				public virtual void OnUseOfActive(){}
				public virtual void OnAttack(){}
				public virtual void OnHitEnemy(){}
				public virtual void OnDamageEnemy(){}
				public virtual void OnInflictStatusEffect(){}
				//IncreaseProjectileNumber
				public virtual void OnAttacked(){}
				public virtual void OnDamaged(){}
				public virtual void OnInflictedWithStatus(){}
				public virtual void OnBattleJoined(CombatantInstance combatant, Battle battle){}
				public virtual void OnBattleEnd(Battle battle){}
				public virtual void OnDroneHitEnemy(){}
				public virtual void OnWouldDie(){}
				public virtual void OnEnemyDefeated(){}
				public virtual void OnGetDamageReduction(){}
				//public AttackDamage value;
				public virtual void OnGetDamageReduction(){}	
			}
		}




		[System.Serializable]
		public abstract class Active:AbilityNode{
			[System.Serializable]
			public class ConditionalCollection:Active{
				
				public Conditional conditional;

				private List<Active> children = new List<Active>();
				
				public override List<AbilityNode> Children{
					get{ return children;}
				}
			}
			
		}
		public abstract class Conditional {
			
			
			public abstract bool ConditionMet();
			
			
			string GetDescription(){
				
			}
			
			
			public class AllConditionals : Conditional{
				public IEnumerable<Conditional> conditionals;
				public override bool ConditionMet(){
					bool met = true;
					foreach(Conditional conditional in conditionals){
						met = conditional.ConditionMet();
						if(!met) break;
					}
					return met;
				}
			}
			public class AnyConditionals : Conditional{
				public IEnumerable<Conditional> conditionals;
				public override bool ConditionMet(){
					bool met = false;
					foreach(Conditional conditional in conditionals){
						met = conditional.ConditionMet();
						if(met) break;
					}
					return met;
				}
			}
			public class NotConditional : Conditional{
				Conditional conditional;
				public override bool ConditionMet(){
					return !conditional.ConditionMet();
				}
			}
			
			public abstract class IfValueIs<T>{
				RelationalOperator relationalOperator;
				T value;
				public abstract T GetValue();
				public override bool ConditionMet(){
					relationalOperator.IsAToB(GetValue(),value);
				}
				
			}
			public class IfHeatValueIs : IfValueIs<int>{
				public override Int GetValue(){
					return Battle.Heat;
				}
			}
			public class IfLifeNumberIs : IfValueIs<int>{
				
			}
		}
	}


	[System.Serializable]
	public class AbilityNode<T>{

		private static Dictionary<System.Type,ExecutionPriority> ExecutionPriorityLookup = new Dictionary<System.Type, EffectExecutionPriorityAttribute>();





		Conditional conditional;
		private T[] _effects;
		public T[] Effects{
			get{
				return _effects;
			}
			set{
				_effects = value;
			}
		}

		
		
		public string GetEffectDescription(){
			
		}

		public IEnumerable<EffectNode> GetOrderedEffectsToApply(){
		}
		ExecutionPriority GetExecutionPriority(){
			System.Type ThisType = this.GetType();
			ExecutionPriority priority; 
			if(ExecutionPriorityLookup.TryGetValue(ThisType, out priority)){
				return priority;
			}else{
				ExecutionPriorityLookup[] attributes = (EffectExecutionPriorityAttribute[])this.GetType().GetCustomAttributes(typeof(EffectExecutionPriorityAttribute), false);
				if(attributes.Length > 0){
					return attributes[0];
				}else{
					return 0;//throw new System.Exception("EffectExecutionPriorityAttribute not set on class "+ThisType.FullName );
				}
			}
		}
		
		

	}
	namespace Passive{
		
		public interface IOnAbilityActivation {
			void OnEffectActivation();
		}
		public interface IOnAbilityDeactivation {
			void OnEffectDeactivation();
		}
		public interface IOnEquip {
			void OnEquip(CombatantInstance combatant, Equipment equipment);
		}
		public interface IOnUnequip {
			void OnUnequip(CombatantInstance combatant, Equipment equipment);
		}

		public interface IOnTurn{
			void OnTurn();
		}
		public interface IOnTick{
			void OnTick();
		}
		public interface IOnSetHeat {
			int OnSetHeat(int value);
		}
		public interface IOnUseOfActive {
			void OnUseOfActive();
		}
		public interface IOnAttack {
			void OnAttack();
		}
		public interface IOnHitEnemy {
			void OnHitEnemy();
		}
		public interface IOnDamageEnemy {
			void OnDamageEnemy();
		}
		public interface IOnInflictStatusEffect {
			void OnInflictStatusEffect();
		}
		//IncreaseProjectileNumber
		public interface IOnAttacked{
			void OnAttacked();
		}
		public interface IOnDamaged{
			void OnDamaged();
		}
		public interface IOnInflictedWithStatus{
			void OnInflictedWithStatus();
		}
		public interface IOnBattleJoined {
			void OnBattleJoined(CombatantInstance combatant, Battle battle);
		}
		public interface IOnBattleEnd {
			void OnBattleEnd(Battle battle);
		}
		public interface IOnDroneHitEnemy {
			void OnDroneHitEnemy();
		}
		public interface IOnWouldDie {
			void OnWouldDie();
		}
		public interface IOnEnemyDefeated {
			void OnEnemyDefeated();
		}
			
		public interface IOnGetDamageReduction {
			void OnGetDamageReduction();
		}

		/*public class IncreaseDamageReductionEffect : EffectNode, IOnGetDamageReduction{
			public AttackDamage value;
			public void OnGetDamageReduction(){
				
			}
		}
		public class IgnoreDamageReductionEffect : EffectNode{
			public AttackDamage value;
		}
*/

/*		public class RegenHealthAtBattleEnd : EffectNode{


			public decimal PercentLostToBeRegenerated;

			public class EquipedEffect : EquipedEffect , IOnBattleJoined, IOnBattleEnd{
				int startingHealth;

				public void OnBattleJoined(CombatantInstance combatant){
					startingHealth = combatant.HP;
				}
				public void OnBattleEnd(){
					
				}
			}
		}*/
		/*public class StormCaller : EquipmentEffect{
			//Storm Caller: You and all future versions gain 2 Charge when joining battle with you.
			public int ChargesToGain = 2;

			public void OnEquip(CombatantInstance combatant, Equipment equipment){

				combatant.ActiveEquipedEffects[new EquipedEffect()] = Equipment;
			}
			
			public void OnUnEquip(CombatantInstance combatant){

			}
			public class EquipedEffect : EquipedEffect , IOnBattleJoined, IOnEquip{

				public void OnEquip(CombatantInstance combatant, Equipment equipment){

				}
				public void OnBattleJoined(CombatantInstance combatant, Battle battle){
					PlayerInstance joiningPlayer = combatant as PlayerInstance;
					if(joiningPlayer == null) return;
					joiningPlayer.Charges += ChargesToGain;

					battle.battleWideEffects.Add(new BattleWideEffect());
				}
			}
			public class BattleWideEffect: BattleWideEffect, IOnBattleJoined{
				public void OnBattleJoined(CombatantInstance combatant, Battle battle){
					PlayerInstance joiningPlayer = combatant as PlayerInstance;
					if(joiningPlayer == null) return;
					joiningPlayer.Charges += ChargesToGain;
				}
			}
		}

		//Acceleration: Every 10 turns, reduce all your warmups by 1, to a minimum of 1.
		public class Acceleration : EquipmentEffect{


		}*/

		/*
		namespace Special{
			//Allows for a final attack if killed in same turn as attack would have occured
			public class InDeathThrows: IOnTick, IOnEffectActivation{
				void IOnTick(){
					//Die //TODO
				}
			}
		}

	}*/
	/*
	namespace Active{
		public interface IOnAttackWithThis{
			void OnAttackWithThis(CombatantInstance combatant, ref AttackDamage attackDamage);
		}
		public interface IOnUseOfThisActive{
			void OnUseOfThisActive(CombatantInstance combatant);
		}
		public interface IOnThisAttackKillsEnemy{
			void OnThisAttackKillsEnemy();
		}

		public class BasicAttack : IOnAttackWithThis{
			public AttackDamage attackDamge;
			public void OnAttackWithThisEffect(CombatantInstance combatant, ref AttackDamage attackDamage){
				attackDamage += this.attackDamge;
			}

		}
		public class AttackWithHeat : IOnAttackWithThis{
			public AttackDamage attackDamge;
			public int heatMultiplier;
			public void OnAttackWithThisEffect(CombatantInstance combatant, ref AttackDamage attackDamage){
				attackDamage += this.attackDamge + heatMultiplier * Heat;
			}
		}

		
		public class AddSubtractHeat:IOnAttackWithThis{
			public int amount;
			public void OnUseOfThisActive(){
				combatant.heat += amount;
			}
			public AddSubtractHeat(){
			}
			public AddSubtractHeat(int amount){
				this.amount = amount;
			}
		}
		public class SetHeat:IOnUseOfThisActive{
			public int amount;
			public void OnUseOfThisActive(CombatantInstance combatant){
				combatant.heat = amount;
			}
			public SetHeat(){
			}
			public SetHeat(int amount){
				this.amount = amount;
			}
		}
	}
	*/
}




