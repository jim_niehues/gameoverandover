using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
/*
public static class CombatantCollection{

	public static CombatantClass[] ToArray(this IEnumerable<CombatantClass> value){
		List<CombatantClass> combatants = new List<CombatantClass>();
		combatants.AddRange(value);
		return combatants.ToArray();
	}
}
*/


[Serializable]
public abstract class NPC_Combatant : Combatant {





	public NPC_Combatant(){
		//Save.loadedSave.NPC_Combatants.Add(this);
	}
	
	/// <summary>
	/// Raises the turn event.
	/// </summary>
	/// <param name="turnNumber">Turn number.</param>
	public virtual void OnTurn(OnTurnArgs args){
		OnTurnEvent.InvokeAll(args);
	}
	public readonly TaggedPseudoEvent<OnTurnArgs> OnTurnEvent = new TaggedPseudoEvent<OnTurnArgs>();

}