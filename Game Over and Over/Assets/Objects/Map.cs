using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using System.Linq;



public interface IMap{
}
public interface IMapDrawable{
	/*Texture2D IMapDrawableIcon{
		get;
	}
	Color? IMapDrawableColor{
		get;
	}
	string? IMapDrawableText{
		get;
	}
	GUIStyle IMapDrawableStyle{
		get;
	}*/
	void IMapDraw(Rect rect);
}

public interface IMap<T>:IMap,IDictionary<IntCoord,T>,IDictionary{
	/*static IMap()
	{
		var type = typeof(T);
		
		if (Nullable.GetUnderlyingType(type) != null)
			return;
		
		if (type.IsClass)
			return;
		
		throw new InvalidOperationException("Type is not nullable or reference type.");
	}*/
	/*bool ContainsKey(IntCoord coord);
	T this[IntCoord coord]
	{
		get;
		set;
	}*/
}



public abstract class MapAbstract<T>:IMap<T>{
	struct CollectionConverter<CollectionType>:ICollection{
		ICollection<CollectionType> genericCollection;
		public CollectionConverter(ICollection<CollectionType> genericCollection){
			this.genericCollection = genericCollection;
		}
		public void CopyTo(
			Array array,
			int index
			){
			throw new NotImplementedException();
		}
		public IEnumerator GetEnumerator(){
			return genericCollection.GetEnumerator();
		}
		public int Count { get{return genericCollection.Count;} }

		bool ICollection.IsSynchronized { get{throw new NotImplementedException();/*return false;*/} }
		System.Object ICollection.SyncRoot { get{throw new NotImplementedException();} }
	}
	System.Object IDictionary.this[System.Object key] { 
		
		get{
			return this[(IntCoord)key];
		}
		set{
			this[(IntCoord)key] = (T)value;
		}
	}
	public abstract T this[IntCoord key] { 
		
		get;
		set;
	}
	
	public abstract bool Remove(IntCoord key);
	public abstract ICollection<IntCoord> Keys {get;}
	public abstract ICollection<T> Values {get;}
	public abstract ICollection<KeyValuePair<IntCoord,T>> Pairs{get;}

	void IDictionary.Remove(System.Object key){
		Remove((IntCoord)key);
	}

	ICollection IDictionary.Keys {
		get{
			return new CollectionConverter<IntCoord>(Keys);
		}
	}
	ICollection IDictionary.Values {
		get{
			return new CollectionConverter<T>(Values);
		}
	}
	bool ICollection.IsSynchronized { get{throw new NotImplementedException();/*return false;*/} }
	System.Object ICollection.SyncRoot { get{throw new NotImplementedException();} }
	void ICollection.CopyTo(Array array,int index){throw new NotImplementedException();}
	public MapAbstract(){
	}
	public MapAbstract(IDictionary<IntCoord,T> dict){
		foreach(KeyValuePair<IntCoord,T> pair in dict){
			this.Add(pair);
		}
	}
	
	
	
	public void Add(IntCoord key, T value){
		if(!ContainsKey(key)){
			this[key] = value;
		}else{
			throw new System.ArgumentException("An element with the same key already exists in the Dictionary<TKey, TValue>.");
		}
	}
	void IDictionary.Add(object key, object value){
		Add((IntCoord)key,(T)value);
	}
	public int Count{
		get{
			return Keys.Count;
		}
	}
	
	public bool IsReadOnly { 
		get{
			return false;
		}
	}
	
	
	public void Add(KeyValuePair<IntCoord,T> item){
		this.Add(item.Key,item.Value);
	}
	
	
	public void Clear(){
		IntCoord[] keys = new IntCoord[Count];
		Keys.CopyTo(keys,0);
		foreach(IntCoord coord in keys){
			Remove(coord);
		}
	}
	bool IDictionary.IsFixedSize
	{
		get
		{
			return false;
		}
	}
	public bool Contains(KeyValuePair<IntCoord,T> item){
		return Pairs.Contains(item);
	}
	bool IDictionary.Contains(System.Object key){
		return Keys.Contains((IntCoord)key);
	}
	public bool ContainsKey(IntCoord key){
		return Keys.Contains(key);
	}
	
	void ICollection<KeyValuePair<IntCoord,T>>.CopyTo(KeyValuePair<IntCoord,T>[] array,int arrayIndex){
		throw new NotImplementedException();
	}
	
	bool ICollection<KeyValuePair<IntCoord,T>>.Remove(KeyValuePair<IntCoord,T> item){
		throw new NotImplementedException();
		//return false;
	}
	
	public bool TryGetValue(IntCoord key, out T value){
		if(ContainsKey(key)){
			value = this[key];
			return true;
		}
		value = default(T);
		return false;
	}
	
	public IEnumerator<System.Collections.Generic.KeyValuePair<IntCoord,T>> GetEnumerator(){
		return new Enumerator(Pairs.GetEnumerator());
	}
	IEnumerator IEnumerable.GetEnumerator(){
		return new Enumerator(Pairs.GetEnumerator());
	}
	IDictionaryEnumerator IDictionary.GetEnumerator(){
		return new Enumerator(Pairs.GetEnumerator());
	}
	[Serializable]
	public struct Enumerator : IEnumerator<KeyValuePair<IntCoord, T>>, IDisposable, IDictionaryEnumerator, IEnumerator
	{
		IEnumerator<System.Collections.Generic.KeyValuePair<IntCoord,T>> baseEnumerator;
		public Enumerator(IEnumerator<System.Collections.Generic.KeyValuePair<IntCoord,T>> baseEnumerator){
			this.baseEnumerator = baseEnumerator;
		}


		public KeyValuePair<IntCoord, T> Current
		{
			get
			{
				return baseEnumerator.Current;
			}
		}
		object IEnumerator.Current
		{
			get
			{
				return baseEnumerator.Current;
			}
		}
		DictionaryEntry IDictionaryEnumerator.Entry
		{
			get
			{
				KeyValuePair<IntCoord, T> pair = this.Current;
				return new DictionaryEntry(pair.Key,pair.Value);
			}
		}
		object IDictionaryEnumerator.Key
		{
			get
			{
				return this.Current.Key;
			}
		}
		object IDictionaryEnumerator.Value
		{
			get
			{
				return this.Current.Value;
			}
		}
		public bool MoveNext ()
		{
			return baseEnumerator.MoveNext();
		}
		public void Dispose ()
		{
		}
		public void Reset(){
			baseEnumerator.Reset();
		}
	}
}


[Serializable]
public class Map<T> : MapAbstract<T>{
	[SerializeField] 
	protected Dictionary<IntCoord,T> dict = new Dictionary<IntCoord, T>();

	public override T this[IntCoord coord]
	{
		get{
			if(dict.ContainsKey(coord)){
				return dict[coord];
			}else{
				return default(T);
			}
		}
		set{
			if(value == null && dict.ContainsKey(coord)) dict.Remove(coord);
			else dict[coord] = value;
		}
	}
	
	public override bool Remove(IntCoord key){
		return dict.Remove(key);
	}
	public override ICollection<IntCoord> Keys {get{return dict.Keys;}}
	public override ICollection<T> Values {get{return dict.Values;}}
	public override ICollection<KeyValuePair<IntCoord,T>> Pairs{get{return dict;}}
}

	/*
	[Serializable]
	Dictionary<IntCoord,ITile> iTiles = new Dictionary<IntCoord, ITile>();
	[Serializable]
	Dictionary<IntCoord,IEntityOnTile[]> iEntitiesOnTiles = new Dictionary<IntCoord, IEntityOnTile[]>();

	public bool ContainsKey(IntCoord coord){
		return !this[coord].;
	}
	public LevelTile this[IntCoord coord]
	{
		get{
			LevelTile lt = new LevelTile();
			if(iTiles.ContainsKey(coord)) lt.tile = iTiles[coord];
			else lt.tile = null;

			if(iEntitiesOnTiles.ContainsKey(coord))lt.entities = iEntitiesOnTiles[coord];
			else lt.tile = null;
			return lt;
		}
		set{
			if(value.tile == null){
				if(iTiles.ContainsKey(coord))  iTiles.Remove(coord);
			}else{
				iTiles[coord] = value.tile;
			}
			if(value.entities == null){
				if(iEntitiesOnTiles.ContainsKey(coord))  iEntitiesOnTiles.Remove(coord);
			}else{
				iEntitiesOnTiles[coord] = value.entities;
			}
		}
	}*/

/*
#if UNITY_EDITOR
	[NotSerialized]
	IntCoord? selected = null;

	bool SelectionMade{
		get{return selected.HasValue;}
	}

	[ShowInInspector]
	[InspectorShowIf("SelectionMade")]
	Tile SelectedTile{
		get{
			if(selected.HasValue){
				if(this.ContainsKey(selected.Value)){
					return this[selected.Value];
				}else{
					return null;
				}
			}else{
				return null;
			}
		}
		set{
			if(selected.HasValue){
				this[selected.Value] = value;
			}
		}
	}
#endif
	//IEntityInRoom 

}
*/