using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using System.Linq;

public interface IHasGameObject{
	GameObject GetOrCreateGameObject();
	GameObject GetGameObject();
	void UpdateGameObject();
}
public interface IResistance{
	ElementType type{get;}
}

public interface IStatusEffect{

}
public interface IDamageable{
	void TakeAttack(OnAttackedArgs args);
	Resistances GetResistances();
}

public interface IQuestComponent{
	object[] Rewards{get;set;}
	object[] Barriers{get;set;}
	object[] Requires{get;set;}
	object[] Costs{get;set;}
}
public class IDReference{

}

public interface ISpawns<T> where T:class{
	//Instance<T> Instance{get;}
	T Spawn(int seed);
	void Populate(T spawn);
}
/*public interface IReference{
	Guid Guid{get;}
	Instance Instance{get;}
}*/
public interface IReference<InstanceT> where InstanceT : class{
	ISpawns<InstanceT> Prefab{
		get;
	}
	InstanceT GetSpawn{get;}
	Instance<InstanceT> Instance{get;}
}
public interface IReference<PrefabT,InstanceT>:IReference<InstanceT> where PrefabT:class,ISpawns<InstanceT> where InstanceT : class{
	PrefabT TypedPrefab{
		get;
	}
}
[Serializable]
public class SharedReference<PrefabT,InstanceT>:IReference<InstanceT>,IReference<PrefabT,InstanceT> where PrefabT:class,ISpawns<InstanceT> where InstanceT:class {
	[SerializeField]
	public SharedInstance<PrefabT> _prefab;
	
	public ISpawns<InstanceT> Prefab{
		get{
			if(_prefab==null)return null;
			return _prefab.Instance;
		}
	}
	public PrefabT TypedPrefab{
		get{
			if(_prefab==null)return null;
			return _prefab.Instance;
		}
	}
	
	public SharedReference(SharedInstance<PrefabT> _prefab){
		this._prefab = _prefab;
	}
	InstanceT IReference<InstanceT>.GetSpawn{
		get{
			return _prefab.Instance.Spawn(_prefab.id.GetHashCode());
		}
	}

	public Instance<InstanceT> Instance{
		get{
			if(!Save.LoadedSave.HasBeenSpawned(_prefab.id)){
				Debug.Log("Spawning SharedReference "+typeof(InstanceT).PrettyName());
				InstanceT spawn = Save.LoadedSave.Spawn(_prefab.Instance, _prefab.id);
				//OnSpawn.InvokeAll(new OnSpawnArgs(spawn));
			}
			return Instance<InstanceT>.Get(_prefab.id);
		}
	}
	//public readonly PsudoEvent<OnSpawnArgs> OnSpawn = new PsudoEvent<OnSpawnArgs>();
}
[Serializable]
public class SharedPrefabReference<PrefabT,InstanceT>:IReference<InstanceT>,IReference<PrefabT,InstanceT> where PrefabT:class,ISpawns<InstanceT> where InstanceT:class{
	[SerializeField]
	public SharedInstance<PrefabT> _prefab;
	
	
	public ISpawns<InstanceT> Prefab{
		get{
			if(_prefab==null)return null;
			return _prefab.Instance;
		}
	}
	public PrefabT TypedPrefab{
		get{
			if(_prefab==null)return null;
			return _prefab.Instance;
		}
	}

	[SerializeField]
	[HideInInspector]
	private Guid id;
	private Guid Id{
		get{
			return id;
		}
	}

	public SharedPrefabReference(){
		_prefab = null;
		id = Guid.NewGuid();
	}
	
	public SharedPrefabReference(SharedInstance<PrefabT> _prefab,Guid id){
		this._prefab = _prefab;
		this.id = id;
	}
	InstanceT IReference<InstanceT>.GetSpawn{
		get{
			return _prefab.Instance.Spawn(id.GetHashCode());
		}
	}

	public Instance<InstanceT> Instance{
		get{
			if(!Save.LoadedSave.HasBeenSpawned(id)){
				Debug.Log("Spawning SharedPrefabReference "+typeof(InstanceT).PrettyName());
				InstanceT spawn = Save.LoadedSave.Spawn(_prefab.Instance, id);
				OnSpawn.InvokeAll(new OnSpawnArgs(spawn));
			}
			return Instance<InstanceT>.Get(id);
		}
	}
	public readonly PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}
[Serializable]
public class Reference<PrefabT,InstanceT>:IReference<InstanceT>, IReference<PrefabT,InstanceT> where PrefabT:class,ISpawns<InstanceT> where InstanceT:class{
	[SerializeField]
	public PrefabT _prefab;
	
	public ISpawns<InstanceT> Prefab{
		get{
			return _prefab;
		}
	}
	public PrefabT TypedPrefab{
		get{
			return _prefab;
		}
	}

	[SerializeField]
	[HideInInspector]
	private Guid id;
	private Guid Id{
		get{
			return id;
		}
	}
	
	public Reference(){
		_prefab = null;
		id = Guid.NewGuid();
	}
	
	public Reference(PrefabT _prefab,Guid id){
		this._prefab = _prefab;
		this.id = id;
	}
	InstanceT IReference<InstanceT>.GetSpawn{
		get{
			return _prefab.Spawn(id.GetHashCode());
		}
	}

	public Instance<InstanceT> Instance{
		get{
			if(!Save.LoadedSave.HasBeenSpawned(id)){
				Debug.Log("Spawning Reference "+typeof(InstanceT).PrettyName());
				InstanceT spawn = Save.LoadedSave.Spawn<InstanceT>(_prefab, id);
				OnSpawn.InvokeAll(new OnSpawnArgs(spawn));
			}
			return Instance<InstanceT>.Get(id);
		}
	}
	
	public readonly PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}
[Serializable]
public class SharedReference<InstanceT>:IReference<InstanceT> where InstanceT:class {
	[SerializeField]
	public SharedInstance<ISpawns<InstanceT>> _prefab;
	
	
	public ISpawns<InstanceT> Prefab{
		get{
			if(_prefab==null)return null;
			return _prefab.Instance;
		}
	}
	
	public SharedReference(SharedInstance<ISpawns<InstanceT>> _prefab){
		this._prefab = _prefab;
	}


	InstanceT IReference<InstanceT>.GetSpawn{get{return _prefab.Instance.Spawn(_prefab.id.GetHashCode());}}
	//[ShowInInspector]
	public Instance<InstanceT> Instance{
		get{
			if(!Save.LoadedSave.HasBeenSpawned(_prefab.id)){
				Debug.Log("Spawning SharedReference "+typeof(InstanceT).PrettyName());
				InstanceT spawn = Save.LoadedSave.Spawn(_prefab.Instance, _prefab.id);
				//OnSpawn.InvokeAll(new OnSpawnArgs(spawn));
			}
			return Instance<InstanceT>.Get(_prefab.id);
		}
	}
	//public readonly PsudoEvent<OnSpawnArgs> OnSpawn = new PsudoEvent<OnSpawnArgs>();
}
[Serializable]
public class SharedPrefabReference<InstanceT>:IReference<InstanceT>  where InstanceT:class{
	[SerializeField]
	public SharedInstance<ISpawns<InstanceT>> _prefab;
	
	public ISpawns<InstanceT> Prefab{
		get{
			if(_prefab==null)return null;
			return _prefab.Instance;
		}
	}

	[SerializeField]
	[HideInInspector]
	private Guid id;
	private Guid Id{
		get{
			return id;
		}
	}
	public SharedPrefabReference(){
		_prefab = null;
		id = Guid.NewGuid();
	}
	public SharedPrefabReference(SharedInstance<ISpawns<InstanceT>> _prefab,Guid id){
		this._prefab = _prefab;
		this.id = id;
	}
	InstanceT IReference<InstanceT>.GetSpawn{get{return _prefab.Instance.Spawn(id.GetHashCode());}}
	
	//[ShowInInspector]
	public Instance<InstanceT> Instance{
		get{
			if(!Save.LoadedSave.HasBeenSpawned(id)){
				Debug.Log("Spawning SharedPrefabReference "+typeof(InstanceT).PrettyName());
				InstanceT spawn = Save.LoadedSave.Spawn(_prefab.Instance, id);
				OnSpawn.InvokeAll(new OnSpawnArgs(spawn));
			}
			return Instance<InstanceT>.Get(id);
		}
	}
	public readonly PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}
[Serializable]
public class Reference<InstanceT>:IReference<InstanceT> where InstanceT:class{
	[SerializeField]
	public ISpawns<InstanceT> _prefab;

	public ISpawns<InstanceT> Prefab{
		get{
			return _prefab;
		}
	}

	[SerializeField]
	[HideInInspector]
	private Guid id;
	private Guid Id{
		get{
			return id;
		}
	}
	
	public Reference(){
		_prefab = null;
		id = Guid.NewGuid();
	}
	
	public Reference(ISpawns<InstanceT> _prefab,Guid id){
		this._prefab = _prefab;
		this.id = id;
	}

	InstanceT IReference<InstanceT>.GetSpawn{get{return _prefab.Spawn(id.GetHashCode());}}
	
	//[ShowInInspector]
	public Instance<InstanceT> Instance{
		get{
			if(!Save.LoadedSave.HasBeenSpawned(id)){
				Debug.Log("Spawning Reference "+typeof(InstanceT).PrettyName());
				InstanceT spawn = Save.LoadedSave.Spawn(_prefab, id);
				OnSpawn.InvokeAll(new OnSpawnArgs(spawn));
			}
			return Instance<InstanceT>.Get(id);
		}
	}
	
	public readonly PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}


/*[Serializable]
public class Original<T> where T : class, ICloneable{
	[SerializeField]
	private T entity;

}*/

/*[Serializable]
public class Instance{
	[Serializable]
	Guid instanceID;
	public Guid ID{
		get{
			return instanceID;
		}
	}
	private System.Object _entity;
	public System.Object entity{
		get{
			if(_entity == null){
				_entity =Save.loadedSave.GetObjectByInstanceId(instanceID);
			}
			return _entity;
		}
	}
}*/

/*public interface IInstance{
	Guid ID{get;}
	bool Exists{get;}
	Instance<T> ToInstanceOf<T>();
}*/


[Serializable]
public class Instance{
	[SerializeField]
	protected Guid instanceID;
	public Guid ID{
		get{
			return instanceID;
		}
	}
	public bool Exists{
		get{
			return Save.LoadedSave.HasBeenSpawned(instanceID);
		}
	}
	public Instance<T> ToInstanceOf<T>() where T: class{
		return Instance<T>.Get(instanceID);
	}
	public override bool Equals (object obj)
	{
		if(!(obj is Instance))return false;
		return this == (Instance)obj;
	}
	public static bool operator ==(Instance a, Instance b)
	{
		// If both are null, or both are same instance, return true.
		if (System.Object.ReferenceEquals(a, b))
		{
			return true;
		}
		
		// If one is null, but not both, return false.
		if (((object)a == null) || ((object)b == null))
		{
			return false;
		}
		
		// Return true if the fields match:
		return a.instanceID == b.instanceID;
	}
	
	public static bool operator !=(Instance a, Instance b)
	{
		return !(a == b);
	}
	public override int GetHashCode ()
	{
		return instanceID.GetHashCode ();
	}
	
	/*public static Instance Get(Guid instanceID){

	}*/
	public static Instance<T> Get<T>(Guid instanceID) where T : class{
		return Instance<T>.Get(instanceID);
	}

	public static Instance<T> Get<T>(T entity) where T : class{
		return Instance<T>.Get(entity);
	}
}
[Serializable]
public class Instance<T> : Instance where T : class{
	//T _typedEntity;
	public T Entity{
		get{
			return Save.LoadedSave.GetObjectByInstanceId<T>(instanceID);
		}
	}

	/*public Instance(T entity){
		_typedEntity = entity;
		instanceID = Save.loadedSave.GetInstanceIdFromObject(entity);

	}*/
	private Instance(Guid instanceID,bool checkCast = true){
		if(checkCast&&! (typeof(T).IsAssignableFrom(Save.LoadedSave.GetTypeByGuid(instanceID)))){
			throw new InvalidCastException("Unable to create Instance<"+typeof(T).PrettyName()+"> from an instance with a base Type of " +
			                               Save.LoadedSave.GetTypeByGuid(instanceID).ToString());
		}
		this.instanceID = instanceID;
	}
	private Instance():base(){}
	/*private Instance(T entity){
		if(entity == null) throw new NullReferenceException();
		this.instanceID = Save.LoadedSave.GetInstanceIdFromObject(entity);
	}*/
	public static Instance<T> Get(Guid id){

		Instance<T> inst = null;
		if(cache.TryGetValue(id, out inst)){
		}else{
			inst = new Instance<T>(id,false);
		}
		return inst;
	}
	public static Instance<T> Get(T entity){
		Guid id = Save.LoadedSave.GetInstanceIdFromObject(entity);
		Instance<T> inst = null;
		if(cache.TryGetValue(id, out inst)){
		}else{
			inst = new Instance<T>(id,false);
		}
		return inst;
	}


	public static Dictionary<Guid,Instance<T>> cache = new Dictionary<Guid, Instance<T>>();
}

/*
public class Located:ILocated{
	[SerializeField]
	Location _location = Location.Null;
	public Location Location{
		get{
			return _location;
		}
		set{
			Instance<ILocated> iLocatedInstance = new Instance<ILocated>(this);
			Location origin = _location;
			Location destination = value;
			if(origin == destination) return;
			if(destination == Location.Null){
				Debug.LogWarning("No Level Tile Exists at Location");
				_location = Location.Null;
			}else{
				_location = destination;
			}

			if(origin!=Location.Null 
			   && origin.LevelTile!=null
			   && origin.LevelTile.Entities.Contains(iLocatedInstance)){
				origin.LevelTile.Remove(iLocatedInstance,destination);
			}
			if(destination!=Location.Null 
			   && destination.LevelTile!=null 
			   && !destination.LevelTile.Entities.Contains(iLocatedInstance)){
				
				destination.LevelTile.Add(iLocatedInstance,origin);
			}
			
			if(iLocatedInstance.Entity != null
			   && origin!=Location.Null
			   && origin.LevelTile!=null ){
				origin.LevelTile.OnExit.InvokeAll(new OnExitArgs(origin,iLocatedInstance,destination));
			}
			if(iLocatedInstance.Entity != null 
			   && destination!=Location.Null
			   && destination.LevelTile!=null ){
				destination.LevelTile.OnEnter.InvokeAll(new OnEnterArgs(destination,iLocatedInstance,origin));
			}
			iLocatedInstance.OnLocationChanged(origin,destination);
				
		}
	}

}
*/
/*
public interface IQuestChunk{
	
	
}
public interface IBarrier{
	
	
}
public interface IBarrier{
	
	
}*/
public interface IItem{	
	Texture2D Icon{get;}
}

public interface IEquipment{}

public interface IEquipmentGauntletLeft:IEquipment{}
public interface IEquipmentGauntletRight:IEquipment{}
public interface IEquipmentArmor:IEquipment{}
public interface IEquipmentCastingRifle:IEquipment{}
public interface IEquipmentPotionBelt:IEquipment{}
public interface IEquipmentBackpack:IEquipment{}
public interface IEquipmentRunes:IEquipment{}

public struct LocationChangedArgs{
	public readonly Instance<ILocated> iLocatedInstance;
	public readonly Location from;
	public readonly Location to;
	public LocationChangedArgs(Instance<ILocated> iLocatedInstance,Location from,Location to){
		if(iLocatedInstance.Entity == null) throw new System.ArgumentNullException("iLocatedInstance.entity can't be null");
		this.iLocatedInstance = iLocatedInstance;
		this.from = from;
		this.to = to;
	}
}
static class ILocatedExtension{
	private static ILocatedExtensionSingleton singleton;
	private static ILocatedExtensionSingleton Singleton{
		get{

			if(singleton==null){
				singleton = GameObject.FindObjectOfType<ILocatedExtensionSingleton>();
				if(singleton==null){
					string name = "ILocatedExtensionSingleton";
					GameObject go = GameObject.Find(name);
					GameObject.DestroyImmediate(go);
					//Debug.Log(String.Join(", ",go.GetComponents<Component>().Select(c=>c.GetType().PrettyName()).ToArray()));
					go = new GameObject(name);
					singleton = go.AddComponent<ILocatedExtensionSingleton>();
					go.hideFlags = HideFlags.HideAndDontSave;
				}
				Clear();
			}
			return singleton;
		}
	}

	private static Dictionary<Instance<ILocated>,Location> Locations{
		get{
			return Singleton.Locations;
		}
	}
	private static Dictionary<Instance<ILocated>,TaggedPseudoEvent<LocationChangedArgs>> OnLocationChangedEvents{
		get{
			return Singleton.OnLocationChangedEvents;
		}
	}
	/*private static Dictionary<ILocated,List<TaggedValue<Action<LocationChangedArgs>>>> OnLocationChangedEvents{

	}
	= new Dictionary<ILocated,List<TaggedValue<Action<LocationChangedArgs>>>>();
*/
	public static Location GetLocation(this Instance<ILocated> instance){
		if(!Singleton.Locations.ContainsKey(instance)){
			Debug.LogWarning("Request made for a unregistered entity");
			return Location.Null;
		}
		else return Singleton.Locations[instance];
	}
	public static Instance<ILocated>[] GetEntities(this Location location){
		if(location==Location.Null || !Singleton.Instances.ContainsKey(location)) return new Instance<ILocated>[0];
		else return Singleton.Instances[location].ToArray();
	}

	public static void Set(Location location, Instance<ILocated> instance){
		Location oldLocation = instance.GetLocation();
		if(Singleton.Locations.ContainsKey(instance)){
			oldLocation = Singleton.Locations[instance];
			if(Singleton.Instances.ContainsKey(oldLocation)){
				Singleton.Instances[oldLocation].Remove(instance);
			}
		}
		Singleton.Locations[instance] = location;
		if(!Singleton.Instances.ContainsKey(location)) Singleton.Instances[location] = new List<Instance<ILocated>>();
		Singleton.Instances[location].Add(instance);

		if(instance.Exists && instance.Entity is IHasGameObject && location.Map == LevelMap.ActiveMap){
			((IHasGameObject)(instance.Entity)).GetOrCreateGameObject().transform.position = location.WorldPosition;
		}

		if(instance.Entity != null
		   && oldLocation!=Location.Null
		   && oldLocation.LevelTile!=null ){
			oldLocation.LevelTile.OnExit.InvokeAll(new OnExitArgs(oldLocation,instance,location));
		}
		if(instance.Entity != null 
		   && location!=Location.Null
		   && location.LevelTile!=null ){
			//Debug.Log(location.LevelTile.OnEnter.Count);
			location.LevelTile.OnEnter.InvokeAll(new OnEnterArgs(location,instance,oldLocation));
		}
		instance.OnLocationChanged(oldLocation,location);
	}
	/*public static Location GetLocation(this ILocated entity){
		return GetLocation(new Instance<ILocated>(entity));
	}
	public static void SetLocation(this ILocated entity, Location destination){
		SetLocation(new Instance<ILocated>(entity),destination);
	}*/
	/*public static void SetLocation(this Instance<ILocated> iLocatedInstance, Location destination){
		Location origin = iLocatedInstance.GetLocation();
		if(origin == destination) return;
		if(destination == Location.Null){
			Debug.LogWarning("No Level Tile Exists at Location");
			Locations[iLocatedInstance] = Location.Null;
		}else{
			Locations[iLocatedInstance] = destination;
		}
		
		if(origin!=Location.Null 
		   && origin.LevelTile!=null
		   && origin.LevelTile.Entities.Contains(iLocatedInstance)){
			origin.LevelTile.Remove(iLocatedInstance,destination);
		}
		if(destination!=Location.Null 
		   && destination.LevelTile!=null 
		   && !destination.LevelTile.Entities.Contains(iLocatedInstance)){
			
			destination.LevelTile.Add(iLocatedInstance,origin);
		}
		
		if(iLocatedInstance.Entity != null
		   && origin!=Location.Null
		   && origin.LevelTile!=null ){
			origin.LevelTile.OnExit.InvokeAll(new OnExitArgs(origin,iLocatedInstance,destination));
		}
		if(iLocatedInstance.Entity != null 
		   && destination!=Location.Null
		   && destination.LevelTile!=null ){
			destination.LevelTile.OnEnter.InvokeAll(new OnEnterArgs(destination,iLocatedInstance,origin));
		}
		iLocatedInstance.OnLocationChanged(origin,destination);
	}*/
	public static TaggedPseudoEvent<LocationChangedArgs> GetOnLocationChangedEvent(this Instance<ILocated> iLocatedInstance){
		if(OnLocationChangedEvents.ContainsKey(iLocatedInstance)) return OnLocationChangedEvents[iLocatedInstance];
		else return new TaggedPseudoEvent<LocationChangedArgs>();
	}
	public static void OnLocationChanged(this Instance<ILocated> iLocatedInstance, Location origin, Location destination){
		GetOnLocationChangedEvent(iLocatedInstance).InvokeAll(new LocationChangedArgs(iLocatedInstance,origin,destination));
	}
	public static void Clear(){
		Singleton.Locations.Clear();
		Singleton.Instances.Clear();
		OnLocationChangedEvents.Clear();
	}

	public static void MoveTo(this Instance<ILocated> iLocatedInstance, Location location){
		Set( location, iLocatedInstance);
	}
	/*
	private static Dictionary<ILocated,GameObject> Models = new Dictionary<ILocated, GameObject>();


	public static void SetModel(this ILocated entity, GameObject model){
		if(Models.ContainsKey(entity)) GameObject.Destroy(Models[entity]);
		Models[entity] = model;
		entity.MoveTo(entity.GetLocation());
	}*/

	public class ILocatedExtensionSingleton:BaseBehavior{
		/*static ILocatedExtensionSingleton inst;
		public static ILocatedExtensionSingleton Inst{
			get{
				if(inst==null){
					GameObject newGameObject = new GameObject("ILocatedeExtensionSingleton",typeof(ILocatedExtensionSingleton));
					newGameObject.hideFlags = HideFlags.HideAndDontSave;
				}
				return inst;
			}
		}
		protected override void Awake ()
		{
			base.Awake ();
			if(Inst!=null&&Inst!=this) Destroy(gameObject);
			else{
				Inst = this;
			}
		} */
		public Dictionary<Instance<ILocated>,Location> Locations = new Dictionary<Instance<ILocated>, Location>();
		public Dictionary<Location,List<Instance<ILocated>>> Instances = new Dictionary<Location,List<Instance<ILocated>>>();



		public Dictionary<Instance<ILocated>,TaggedPseudoEvent<LocationChangedArgs>>OnLocationChangedEvents= new Dictionary<Instance<ILocated>,TaggedPseudoEvent<LocationChangedArgs>>();
	}
}

public interface ILocated{
	Location Location{get;set;}
}


/*
static class IMovableExtension{
	private static Dictionary<ILocated,Location> Locations = new Dictionary<ILocated, Location>();
	private static Dictionary<ILocated,GameObject> Models = new Dictionary<ILocated, GameObject>();

	[ResettableExtensionMockProperty("IMovableLocation")]
	public static Location GetLocation(this ILocated entity){
		if(!Locations.ContainsKey(entity)) Locations[entity] = new Location();
		return Locations[entity];
	}

	[ResettableExtensionMockProperty("IMovableLocation")]
	public static void SetLocation(this ILocated entity, Location location){
		Locations[entity] = location;
		if(Models.ContainsKey(entity) && Models[entity] != null) Models[entity].transform.position = new Vector3(location.coord.x,location.coord.y,1);
	}

}*/
