﻿using UnityEngine;
using System.Collections;

public class EnemyMonoBehaviour : MonoBehaviour {
	public GameObject model;

	// Use this for initialization
	void Start () {
		if(model==null){
			Debug.Log(""+this.ToString() +" is missing a model");
		}else{
			model = (GameObject)Instantiate(model);
			model.transform.parent = this.transform;
			model.transform.localPosition = Vector3.zero;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
