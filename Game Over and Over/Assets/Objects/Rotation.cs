﻿using UnityEngine;
using System.Collections;



public struct Rotation{
	//enum Values {_0=0,_90=1,_180=2,_270=3}
	int _quarterTurns;
	public int QuarterTurns{
		get{
			return _quarterTurns;
		}
		set{
			_quarterTurns = value%4;
		}
	}
	public static readonly Rotation _0 = new Rotation()+0;
	public static readonly Rotation _90 = new Rotation()+1;
	public static readonly Rotation _180 = new Rotation()+2;
	public static readonly Rotation _270 = new Rotation()+3;
	
	public IntCoord North{
		get{
			return Rotate(IntCoord.North);
		}
	}
	public IntCoord South{
		get{
			return Rotate(IntCoord.South);
		}
	}
	public IntCoord East{
		get{
			return Rotate(IntCoord.East);
		}
	}
	public IntCoord West{
		get{
			return Rotate(IntCoord.West);
		}
	}



	/*
				public static readonly Rotation _90 = new Rotation()+1;
				public static readonly Rotation _180 = new Rotation()+2;
				public static readonly Rotation _270 = new Rotation()+3;

				public static Rotation operator +(int quarterTurns){
					Value += quarterTurns;
				}
				public static Rotation operator -(int quarterTurns){
					Value -= quarterTurns;
				}
				*/
	public IntCoord RotateInverse(IntCoord coord){
		switch ((QuarterTurns+2)%4*90){
		case 0:
			return new IntCoord(coord.x,coord.y);
		case 90:
			return new IntCoord(coord.y,-coord.x);
		case 180:
			return new IntCoord(-coord.x,-coord.y);
		case 270:
			return new IntCoord(-coord.y,coord.x);
		}
		throw new System.NotImplementedException();
	}
	public IntCoord Rotate(IntCoord coord){
		switch (QuarterTurns*90){
		case 0:
			return new IntCoord(coord.x,coord.y);
		case 90:
			return new IntCoord(coord.y,-coord.x);
		case 180:
			return new IntCoord(-coord.x,-coord.y);
		case 270:
			return new IntCoord(-coord.y,coord.x);
		}
		throw new System.NotImplementedException();
	}
	public Direction RotateInverse(Direction dir){
		switch ((QuarterTurns+2)*90){
		case 0:
			return dir;
		case 90:
			return (Direction)(((int)dir+1)%4);
		case 180:
			return dir.GetOpposite();
		case 270:
			return (Direction)(((int)dir-1)%4);
		}
		throw new System.NotImplementedException();
	}
	public Direction Rotate(Direction dir){
		switch (QuarterTurns*90){
		case 0:
			return dir;
		case 90:
			return (Direction)(((int)dir+1)%4);
		case 180:
			return dir.GetOpposite();
		case 270:
			return (Direction)(((int)dir-1)%4);
		}
		throw new System.NotImplementedException();
	}
	IntCoord RotateAround(IntCoord coord, Rotation rotation, IntCoord around){
		return rotation.Rotate(coord-around)+around;
	}
	
	
	public static Rotation operator +(Rotation a, int b)
	{
		return new Rotation(){ QuarterTurns = a.QuarterTurns+b };
	}
	public static Rotation operator +(Rotation a, Rotation b)
	{
		return  new Rotation(){ QuarterTurns = a.QuarterTurns+b.QuarterTurns };
	}
	public static Rotation operator ++(Rotation a)
	{
		a.QuarterTurns++;
		return a;
	}


	public static Rotation operator -(Rotation a, int b)
	{
		return new Rotation(){ QuarterTurns = a.QuarterTurns-b};
	}
	public static Rotation operator -(Rotation a, Rotation b)
	{
		return new Rotation(){ QuarterTurns = a.QuarterTurns-b.QuarterTurns};
	}
	public static Rotation operator --(Rotation a)
	{
		a.QuarterTurns--;
		return a;
	}
	
	public override bool Equals(System.Object obj)
	{
		if (!(obj is Rotation))
			return false;
		
		Rotation otherRotation = (Rotation) obj;
		if(QuarterTurns==otherRotation.QuarterTurns){
			return true;
		}else{
			return false;
		}
	}
	
	public bool Equals(Rotation otherRotation)
	{
		if(QuarterTurns==otherRotation.QuarterTurns){
			return true;
		}else{
			return false;
		}
	}
	
	public static bool operator ==(Rotation a, Rotation b)
	{
		if(a.QuarterTurns==b.QuarterTurns){
			return true;
		}else{
			return false;
		}
	}
	public static bool operator !=(Rotation a, Rotation b)
	{
		return !(a.QuarterTurns==b.QuarterTurns);
	}
	
	
	public override int GetHashCode()
	{
		return QuarterTurns;
	}
}


