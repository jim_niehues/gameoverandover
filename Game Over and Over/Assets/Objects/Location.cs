using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

public struct Location{

	public static Location Null = new Location();
	
	[HideInInspector]
	public readonly LevelMapAsset mapAsset;
	
	[ShowInInspector]
	public LevelMapAsset Asset{
		get{return mapAsset;}
	}

	public LevelMap Map{
		get{
			if(mapAsset == null) return null;
			return mapAsset.LevelMapReference.Instance.Entity;
		}
	}

	
	public Vector3 WorldPosition{
		get{
			return new Vector3(coord.x,-coord.y,0);
		}
	}
	[HideInInspector]
	[SerializeField]
	public readonly IntCoord coord;
	
	/*[ShowInInspector]
	public IntCoord Coord{
		get{
			return coord;
		}
	}*/


	/*private Location(Dungeon NullDungeon){
		this.Dungeon = null;
		this.coord = new IntCoord();
	}*/
	
	public Location(LevelMapAsset mapAsset, IntCoord coord){
		if(mapAsset==null) throw new System.ArgumentNullException("mapAsset is null");
		this.mapAsset = mapAsset;
		//this.Room = 
		this.coord = coord;
	}
	public Location(LevelMap map, IntCoord coord){
		if(map==null) throw new System.ArgumentNullException("LevelMap is null");
		if(map.Asset==null) throw new System.ArgumentNullException("LevelMap.Asset is null");
		this.mapAsset = map.Asset;
		//this.Room = 
		this.coord = coord;
	}


	public bool LevelTileExists{
		get{
			if(this == Location.Null) return false;
			return Map.ContainsKey(coord);
		}
	}
	public LevelTile LevelTile{
		get{
			if(this == Location.Null) return null;
			return Map[coord];
		}
		set{
			if(this == Location.Null) return;
			LevelTile oldValue = LevelTile;
			if(value != oldValue){
				Map[coord] = value;
				if(oldValue!=null){
					Debug.LogWarning("LevelTile overwritten");
					oldValue.Location = Location.Null;
				}
				if(value.Location != this){
					value.Location = this;
				}
			}
		}
	}
	
	public static Location operator +(Location a, IntCoord b)
	{
		return new Location(a.mapAsset, a.coord + b);
	}

	public static bool operator ==(Location a, Location b)
	{
		return (a.mapAsset == b.mapAsset) && (a.coord == b.coord);
	}
	public static bool operator !=(Location a, Location b)
	{
		return !(a==b);
	}
	public override bool Equals(object o){
		if(o == null){
			Debug.LogWarning("Location is being compaired to null");
			return false;
		}
		if(!(o is Location)) return false;
		return this==(Location)o;
	}
	public override int GetHashCode(){
		unchecked{
			return mapAsset.GetHashCode()+coord.GetHashCode();
		}
	}
	[ShowInInspector]
	[InspectorName("Map")]
	string MapString{
		get{
			if(mapAsset==null) return "none";
			return mapAsset.ToString();
		}
	}
	[ShowInInspector]
	[InspectorName("Coords")]
	string CoordsString{
		get{
			return coord.ToString();
		}
	}
	public override string ToString(){
		return "Location(mapAsset:"+MapString+", Coord:"+CoordsString+")";
	}
}
/*
public interface ILocationDecorator{
	void OnAwake();

	//void OnLocationChanged(Location _old, Location _new );
}



public static class ILocationDecoratorExtension{
	static Dictionary<ILocationDecorator,Location> lookup = new Dictionary<ILocationDecorator, Location>();
	public static Location GetLocation(this ILocationDecorator locDec){
		Location location;
		if(lookup.TryGetValue(locDec, out location)){
			return location;
		}
		return Location.Null;
	}
	public static void SetLocation(this ILocationDecorator locDec, Location location){
		lookup[locDec] = location;
	}
}
*/
