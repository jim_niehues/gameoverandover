using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using System.Linq;

public class LevelTilePrefab : IMapDrawable, ISpawns<LevelTile>{
	[HideInInspector]
	public int elevation;
	
	public ISpawns<ITile> tileSpawner;
	[SerializeField]
	public List<IReference<ILocated>> entities = new List<IReference<ILocated>>();
	public readonly TaggedPseudoEvent<OnSpawnArgs> OnSpawn = new TaggedPseudoEvent<OnSpawnArgs>();
	public readonly TaggedPseudoEvent<OnEnterArgs> OnEnter = new TaggedPseudoEvent<OnEnterArgs>();
	public readonly TaggedPseudoEvent<OnStayArgs>  OnStay  = new TaggedPseudoEvent<OnStayArgs>();
	public readonly TaggedPseudoEvent<OnExitArgs>  OnExit  = new TaggedPseudoEvent<OnExitArgs>();

	public void IMapDraw(Rect rect){
		IMapDrawable drawable;
		if(tileSpawner!=null){
			drawable = tileSpawner as IMapDrawable;
			if(drawable !=null){
				drawable.IMapDraw(rect);
			}
		}
		foreach(ISpawns<ILocated> entity in entities.Where(e=>e!=null).Select<IReference<ILocated>,ISpawns<ILocated>>(e=> e.Prefab)){
			if((drawable = entity as IMapDrawable)!=null){
				drawable.IMapDraw(rect);
			}
			else{
				Rect smallerRect = new Rect(rect.x+rect.width/4f,rect.y+rect.height/4f,rect.width*2f/4f,rect.height*2f/4f);
				GUI.Box(smallerRect,"");
			}
		}
	}
	public LevelTile Spawn(int seed){
		LevelTile spawn = new LevelTile();
		spawn.tile = tileSpawner.Spawn(seed);
		return spawn;
	}
	public void Populate(LevelTile spawn){
		tileSpawner.Populate(spawn.tile);
		//instance.tile.Location = 
		spawn.OnSpawn.AddRange(OnSpawn);
		spawn.OnEnter.AddRange(OnEnter);
		spawn.OnStay.AddRange(OnStay);
		spawn.OnExit.AddRange(OnExit);
		spawn.OnSpawn.Add(new RawAction<OnSpawnArgs>(delegate(OnSpawnArgs args){
			foreach(Instance<ILocated> entity in entities.Select<IReference<ILocated>,Instance<ILocated>>(e=>e.Instance)){
				
				entity.MoveTo(spawn.Location);
			}
		}));
	}
}

public class LevelTile : IMapDrawable{
	//[SerializeField]
	Location location = Location.Null;
	public Location Location{
		get{
			return location;
		}
		set{
			if(location == value) return;
			if(location == Location.Null){
				location = value;
				tile.Location = value;
				/*if(value.LevelTile != this){
					if(value.LevelTile != null){
						Debug.LogWarning("LevelTile replaced");
					}
					value.LevelTile = this;
				}*/
			}else{
				Debug.Log("existing = "+location.ToString());
				Debug.Log("new = "+value.ToString());
				throw new System.Exception("Cannot be changed after it has been set");
			}
		}
	}

	public ITile tile;

	//[HideInInspector]
	//List<Instance<ILocated>> entities = new List<Instance<ILocated>>();

	[HideInInspector]
	[FullSerializer.fsProperty("Entities")]
	public Instance<ILocated>[] Entities{
		get{
			return location.GetEntities();
		}
		set{
			if(Entities.Length>0)throw new Exception("Entities cannot be \"set\" only added to");
			foreach(Instance<ILocated> inst in value){
				inst.MoveTo(location);
			}
		}
	}
	/*
	public void Add(Instance<ILocated> iLocatedInstance,Location origin){
		if(origin!=this.Location){
			iLocatedInstance.MoveTo(this.Location);
			
			if(iLocatedInstance.Entity is IHasGameObject && Location.Map == LevelMap.ActiveMap){
				((IHasGameObject)(iLocatedInstance.Entity)).GetOrCreateGameObject().transform.position = WorldPosition;
			}

			Location currentLocation = iLocatedInstance.GetLocation();
			if(currentLocation != Location.Null){
				if(currentLocation.LevelTile!=this){
					iLocatedInstance.MoveTo(this.Location);
				}
			}
		}
	}*/
	/*public void Remove(Instance<ILocated> iLocatedInstance,Location destination){
		if(destination != this.Location){
			entities.Remove(iLocatedInstance);
		}
		if(destination == Location.Null || destination.Map == LevelMap.ActiveMap){
			if(iLocatedInstance.Entity is IHasGameObject){
				IHasGameObject entityAsIHasGameObject = (IHasGameObject)(iLocatedInstance.Entity);
				GameObject entitiesGameObject = entityAsIHasGameObject.GetGameObject();
				if(entitiesGameObject!=null){
					entitiesGameObject.Unload();
				}
			}
		}
		if(iLocatedInstance.GetLocation() == destination){
			iLocatedInstance.SetLocation(destination);
		}
	}*/
	/*[SerializeField]
	List<ILocationDecorator> decorators = new List<ILocationDecorator>();
	
	public ILocationDecorator[] Decorators{
		get{return decorators.ToArray();}
	}*/

	

	public void IMapDraw(Rect rect){
		if(tile!=null) tile.IMapDraw(rect);
		IMapDrawable drawable;
		foreach(ILocated entity in Entities){
			if((drawable = entity as IMapDrawable)!=null){
				drawable.IMapDraw(rect);
			}
			else{
				Rect smallerRect = new Rect(rect.x+rect.width/4f,rect.y+rect.height/4f,rect.width*2f/4f,rect.height*2f/4f);
				GUI.Box(smallerRect,"");
			}
		}
	}

	/*public bool IsNull{
		get{
			if(tile!=null) return false;
			if(entities!=null&&entities.Length!=0) return false;
			return true;
		}
	}*/
	public bool CanMoveHereFrom(IsPassableArgs args){
		return tile.IsPassableBy(args);
	}
	/*public void CallOnEnter(OnEnterArgs args){
		if(OnEnter!=null)OnEnter(args);
	}
	public void CallOnStay(IMovable iMovable){
		if(OnEnter!=null)OnStay(this,iMovable,location);
	}
	public void CallOnExit(OnExitArgs args){
		if(OnExit!=null)OnExit(args);
	}
	public void CallOnSpawn(OnExitArgs args){
		if(OnSpawn!=null)OnSpawn(args);
	}*/
	
	public readonly TaggedPseudoEvent<OnSpawnArgs> OnSpawn = new TaggedPseudoEvent<OnSpawnArgs>();
	public readonly TaggedPseudoEvent<OnEnterArgs> OnEnter = new TaggedPseudoEvent<OnEnterArgs>();
	public readonly TaggedPseudoEvent<OnStayArgs>  OnStay  = new TaggedPseudoEvent<OnStayArgs>();
	public readonly TaggedPseudoEvent<OnExitArgs>  OnExit  = new TaggedPseudoEvent<OnExitArgs>();
	public void LoadIntoScene(){
		GameObject tileGameObject = tile.GetOrCreateGameObject();
		if(tileGameObject!=null)tileGameObject.transform.parent = LevelMap.MapRoot.transform;
		Debug.Log("Tile added to scene");
		foreach(ILocated located in Entities.Select(e=>e.Entity)){
			if(located is IHasGameObject){
				GameObject entityGameObject = ((IHasGameObject)located).GetOrCreateGameObject();
				if(entityGameObject!=null)entityGameObject.transform.parent = LevelMap.MapRoot.transform;
			}
		}
	}
}

	/*
	[Serializable]
	Dictionary<IntCoord,ITile> iTiles = new Dictionary<IntCoord, ITile>();
	[Serializable]
	Dictionary<IntCoord,IEntityOnTile[]> iEntitiesOnTiles = new Dictionary<IntCoord, IEntityOnTile[]>();

	public bool ContainsKey(IntCoord coord){
		return !this[coord].;
	}
	public LevelTile this[IntCoord coord]
	{
		get{
			LevelTile lt = new LevelTile();
			if(iTiles.ContainsKey(coord)) lt.tile = iTiles[coord];
			else lt.tile = null;

			if(iEntitiesOnTiles.ContainsKey(coord))lt.entities = iEntitiesOnTiles[coord];
			else lt.tile = null;
			return lt;
		}
		set{
			if(value.tile == null){
				if(iTiles.ContainsKey(coord))  iTiles.Remove(coord);
			}else{
				iTiles[coord] = value.tile;
			}
			if(value.entities == null){
				if(iEntitiesOnTiles.ContainsKey(coord))  iEntitiesOnTiles.Remove(coord);
			}else{
				iEntitiesOnTiles[coord] = value.entities;
			}
		}
	}*/

/*
#if UNITY_EDITOR
	[NotSerialized]
	IntCoord? selected = null;

	bool SelectionMade{
		get{return selected.HasValue;}
	}

	[ShowInInspector]
	[InspectorShowIf("SelectionMade")]
	Tile SelectedTile{
		get{
			if(selected.HasValue){
				if(this.ContainsKey(selected.Value)){
					return this[selected.Value];
				}else{
					return null;
				}
			}else{
				return null;
			}
		}
		set{
			if(selected.HasValue){
				this[selected.Value] = value;
			}
		}
	}
#endif
	//IEntityInRoom 

}
*/