using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

/*
public class PercentageResistance
{
	public decimal multiplier = 1;
	public int flatReduction = 0;
	public readonly ElementType type;
	
	public Resistance(ElementType type){
		this.type = type;
	}
	public Resistance(ElementType type, decimal multiplier ){
		this.type = type;
		this.multiplier = multiplier;
	}
}

public class FlatResistance
{
	public decimal multiplier = 1;
	public int flatReduction = 0;
	public readonly ElementType type;
	
	public Resistance(ElementType type){
		this.type = type;
	}
	public Resistance(ElementType type, int flatReduction, decimal multiplier ){
		this.type = type;
		this.flatReduction = flatReduction;
		this.multiplier = multiplier;
	}
}*/

public class Resistance{
	
	public enum Mult{I,M,S,N,W,E}

	public Mult multiplier = Mult.N;
	public int flatReduction= 0;
	//public ElementType element;
	
	public Resistance(Mult multiplier,int flatReduction){
		this.multiplier = multiplier;
		this.flatReduction = flatReduction;
	}
	public Resistance(){
	}

}
public class Resistances : Dictionary<ElementType,Resistance>{
	public new Resistance this[ElementType type]{
		get{
			if(!base.ContainsKey(type)){
				base[type] = new Resistance();
			}
			return base[type];
		}
		set{
			base[type] = value;
		}
	}

	public new void Add(ElementType type, Resistance resistance){
		this[type] = resistance;
	}
	public Resistances():base(){
		/*foreach(ElementType type in Enum.GetValues(typeof(ElementType))){
			if(!this.ContainsKey(type)){
				this[type] = new Resistance(Resistance.Mult.N,0);
			}
		}*/
	}
	/*public int this[ElementType type]{
		get{
			if(values.ContainsKey(type) return 
			return values[type];
		}
	}*/
	public AttackDamage ApplyTo(AttackDamage attackDamage){
		
		foreach(ElementType type in Enum.GetValues(typeof(ElementType))){
			if(this.ContainsKey(type) && attackDamage.ContainsKey(type)){
				int oldDamage = attackDamage[type];
				decimal mult = 1;
				switch (this[type].multiplier){
				case  Resistance.Mult.I: 
					mult = 0m; 
					break;
				case  Resistance.Mult.M: 
					mult = .01m; 
					break;
				case  Resistance.Mult.S: 
					mult = .1m; 
					break;
				case  Resistance.Mult.N: 
					mult = 1m; 
					break;
				case  Resistance.Mult.W: 
					mult = 2m; 
					break;
				case  Resistance.Mult.E: 
					mult = 5m; 
					break;
				}
				attackDamage[type] = (int)(attackDamage[type] * mult);
				attackDamage[type] -= this[type].flatReduction;
				if(attackDamage[type]!=oldDamage){
					Debug.Log("attack's "+Enum.GetName(typeof(ElementType),type)+" damage has been modified from "+oldDamage+" to " +attackDamage[type]);
				}
			}
		}
		return attackDamage;
	}

	/*
	void Add(AttackDamage attackDamage){
		if(!values.ContainsKey(attackDamage.type)){
			values[attackDamage.type] = attackDamage.amount;
		}else{
			values[attackDamage.type] += attackDamage.amount;
		}
	}*/
	/*void Add(ElementType type,int amount){
		if(!values.ContainsKey(type)){
			values[type] = amount;
		}else{
			values[type] += amount;
		}
		
	}*/
	/*
	public static Resistances operator +(Resistances r1, Resistances r2)
	{
		Resistances rResult= new Resistances();
		foreach(ElementType type in Enum.GetValues(typeof(ElementType))){
			rResult.Percentage[type] = r1.Percentage[type]*r2.Percentage[type];
			rResult.Flat[type] = r1.Flat[type]+r2.Flat[type];
		}
		return rResult;
	}
	*/
}