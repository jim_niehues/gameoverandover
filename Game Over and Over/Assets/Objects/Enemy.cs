using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

public class EnemyPrefab:BaseScriptableObject, ISpawns<Enemy>, ISpawns<NPC_Combatant>, ISpawns<Combatant>, ISpawns<ILocated>{
	EnemySpawner spawner;
	
	Enemy ISpawns<Enemy>.Spawn(int seed){
		return spawner.Spawn (seed);
	}
	NPC_Combatant ISpawns<NPC_Combatant>.Spawn(int seed){
		return spawner.Spawn (seed);
	}
	Combatant ISpawns<Combatant>.Spawn(int seed){
		return spawner.Spawn (seed);
	}
	ILocated ISpawns<ILocated>.Spawn(int seed){
		return spawner.Spawn (seed);
	}
	
	void ISpawns<Enemy>.Populate(Enemy spawn){
		spawner.Populate ((Enemy)spawn);
	}
	void ISpawns<NPC_Combatant>.Populate(NPC_Combatant spawn){
		spawner.Populate ((Enemy)spawn);
	}
	void ISpawns<Combatant>.Populate(Combatant spawn){
		spawner.Populate ((Enemy)spawn);
	}
	void ISpawns<ILocated>.Populate(ILocated spawn){
		spawner.Populate ((Enemy)spawn);
	}
}
public class EnemySpawner:  ISpawns<Enemy>, ISpawns<NPC_Combatant>, ISpawns<Combatant>, ISpawns<ILocated>{
	public GameObject prefab;
	public int exp;

	public virtual Enemy Spawn(int seed){
		return new Enemy();
	}
	
	public virtual void Populate(Enemy spawn){
		
	}

	Enemy ISpawns<Enemy>.Spawn(int seed){
		return Spawn (seed);
	}
	NPC_Combatant ISpawns<NPC_Combatant>.Spawn(int seed){
		return Spawn (seed);
	}
	Combatant ISpawns<Combatant>.Spawn(int seed){
		return Spawn (seed);
	}
	ILocated ISpawns<ILocated>.Spawn(int seed){
		return Spawn (seed);
	}

	void ISpawns<Enemy>.Populate(Enemy spawn){
		Populate ((Enemy)spawn);
	}
	void ISpawns<NPC_Combatant>.Populate(NPC_Combatant spawn){
		Populate ((Enemy)spawn);
	}
	void ISpawns<Combatant>.Populate(Combatant spawn){
		Populate ((Enemy)spawn);
	}
	void ISpawns<ILocated>.Populate(ILocated spawn){
		Populate ((Enemy)spawn);
	}
	public PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}

public class Enemy : NPC_Combatant{
	public static List<Enemy> AllEnemies = new List<Enemy>();
	//public readonly string name;


	protected int _exp;

	public Item[] drops;

	public Enemy(){
		AllEnemies.Add(this);
		/*OnResetDelegate = delegate() {
			this.dead = false;
			this.MaxHP = maxHP;
			this.HP = hp;
			this._exp = exp;
			this.drops = drops;
			OpposingCombatants.Clear();
		};
		OnResetDelegate();*/
	}
	public override void OnDeath (OnDeathArgs args)
	{
		Save.LoadedSave.EXP += _exp;
		base.OnDeath(args);
	}
}
