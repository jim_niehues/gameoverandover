using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[InitializeOnLoad]
public class EditorCoroutines {
	static EditorCoroutines() {
		EditorApplication.update -= UpdateEditorCoroutines;
		EditorApplication.update += UpdateEditorCoroutines;
	}
	public static void Start(IEnumerator editorCoroutine){
		editorCoroutines.Add(editorCoroutine);
	}
	public static void Start(object id, IEnumerator editorCoroutine){
		editorCoroutinesWithIds.Add(id, editorCoroutine);
	}
	public static void Remove(object id){
		editorCoroutinesWithIds.Remove(id);
	}
	public static void Exists(object id){
		editorCoroutinesWithIds.ContainsKey(id);
	}
	static readonly List<IEnumerator> editorCoroutines = new List<IEnumerator>();
	static readonly Dictionary<object,IEnumerator> editorCoroutinesWithIds = new Dictionary<object,IEnumerator>();
	// Update is called once per frame
	static void UpdateEditorCoroutines () {
		EditorApplication.update -= UpdateEditorCoroutines;
		EditorApplication.update += UpdateEditorCoroutines;
		if(editorCoroutines!=null){
			int i=0;
			while(i<editorCoroutines.Count){
				if(!editorCoroutines[i].MoveNext()){
					editorCoroutines.RemoveAt(i);
				}else{
					i++;	
				}
			}
		}
		if(editorCoroutinesWithIds!=null){
			HashSet<object> toRemove = new HashSet<object>();
			foreach(KeyValuePair<object,IEnumerator> pair in editorCoroutinesWithIds){
				if(!pair.Value.MoveNext()){
					toRemove.Add(pair.Key);
				}
			}
			foreach(object key in toRemove){
				editorCoroutinesWithIds.Remove(key);
			}
		}
	}
	public class ObjectStringID : System.Object, IEquatable<ObjectStringID>{
		public object obj;
		public string key;
		public ObjectStringID(object obj,string key){
			this.obj = obj;
			this.key = key;
		}
		public bool Equals(ObjectStringID other){
			return (obj.Equals(other.obj)) && (key.Equals(other.key));
		}
	}
}
