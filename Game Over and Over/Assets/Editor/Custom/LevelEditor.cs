//using UnityEngine;
//using UnityEditor;
//using System.Collections;
//using System.Collections.Generic;
//using FullInspector;
//using System.Reflection;
//
//[CustomBehaviorEditor(typeof(LevelAsset), Inherit = true)]
//public class LevelEditor: BehaviorEditor<LevelAsset>
//{
//	static Dictionary<System.Type,List<System.Type>> attributtes;
//	public static int viewX = 10;
//	public static int viewY = 10;
//	public static float tilePixels = 10;
//	
//	//public IntCoord selection = null;
//	
//	public static Dictionary<object,Vector2> viewerOffsets = new Dictionary<object,Vector2>();
////	public static Dictionary<object,TileBrush> viewerTileBrush = new Dictionary<object,TileBrush>();
//	public static Vector2 ViewerOffsets(object key){
//		if(!viewerOffsets.ContainsKey(key)){
//			viewerOffsets[key] = Vector2.zero;
//		}
//		return viewerOffsets[key];
//	}
//	static IntCoord selection;
//	protected override void OnEdit (Rect rect, LevelAsset behavior, fiGraphMetadata metadata)
//	{
//		if(attributtes == null){
//			attributtes = new Dictionary<System.Type, List<System.Type>>();
//
//			foreach (Assembly a in System.AppDomain.CurrentDomain.GetAssemblies())
//			{
//				if(!a.FullName.Contains("Assembly")) continue;
//				foreach (System.Type type in a.GetTypes())
//				{
//					System.Attribute[] attrs = System.Attribute.GetCustomAttributes(type);  // Reflection. 
//					
//					// Displaying output. 
//					foreach (System.Attribute attr in attrs)
//					{
//						if(!attributtes.ContainsKey(attr.GetType())){
//							attributtes[attr.GetType()] = new List<System.Type>();
//						}
//						attributtes[attr.GetType()].Add(type);
//					}
//					//types += "\n"+type.FullName;
//				}
//
//			}
//
//		}
//
//		//Assembly myAssembly = Assembly.GetExecutingAssembly();
//		string types = "";
//		foreach ( System.Type type in attributtes[typeof(System.SerializableAttribute)]){
//			types += "\n"+type.FullName;
//		}
//		/*
//		foreach (Assembly a in System.AppDomain.CurrentDomain.GetAssemblies())
//		{
//			if(a.FullName.Contains("Assembly-CSharp")){
//				foreach (System.Type type in a.GetTypes())
//				{
//					types += "\n"+type.FullName;
//				}
//			}else{
//				Debug.Log(a.FullName);
//			}
//		}*/
//		Debug.Log(types);
//		
//		float tilePixels = LevelEditor.tilePixels;
//		int viewX = LevelEditor.viewX;
//		int viewY = LevelEditor.viewY;
//		GUI.BeginGroup(rect);
//		{
//
//			rect.x = 0;
//			rect.y = 0;
//			
//			Rect ViewRect = new Rect(0,0,viewX*tilePixels,viewY*tilePixels);
//			GUI.BeginGroup(ViewRect);
//			{
//				//LevelAsset myTarget = (LevelAsset)target;
//				Vector2 viewCenter = new Vector2(150,200);
//				Vector2 viewSize = new Vector2(viewX*tilePixels,viewY*tilePixels);
//				Vector2 viewOffset = ViewerOffsets(1);
//				int minYIndex =  Mathf.FloorToInt(viewOffset.y/tilePixels)-Mathf.CeilToInt((float)viewY/2f);
//				int maxYIndex =  Mathf.CeilToInt(viewOffset.y/tilePixels)+Mathf.CeilToInt((float)viewY/2f);
//				GUI.Box(rect, "");//new Rect(viewCenter.x-viewSize.x/2f,viewCenter.y-viewSize.y/2f,viewSize.y,viewSize.x),""+minYIndex);
//				//GUI.BeginGroup(new Rect(viewCenter.x-viewSize.x/2f,viewCenter.y-viewSize.y/2f,viewSize.y,viewSize.x));
//				for(int y = minYIndex; y<=maxYIndex;y++){
//					float posY = -viewOffset.y + (float)viewY*tilePixels/2 + y*tilePixels; 
//					for(int x = Mathf.FloorToInt(viewOffset.x/tilePixels-(float)viewX/2f); x<=Mathf.CeilToInt(viewOffset.x/tilePixels+(float)viewX/2f);x++){
//						float posX = -viewOffset.x + (float)viewX*tilePixels/2f + x*tilePixels; 
//						//GUI.Box(new Rect(posX,posY,tilePixels,tilePixels),"a");
//						if(GUI.Button(new Rect(posX,posY,tilePixels,tilePixels),"")){
//							selection = new IntCoord(x,y);
//							
//							//SelectedProperty = LevelProperty.FindPropertyRelative("Tiles");
//							//GetArrayElementAtIndex(int index)
//						}
//					}
//				}
//			}
//			GUI.EndGroup();
//			/*
//			GUI.BeginGroup(new Rect(0,viewY*tilePixels,rect.width,rect.height-viewY*tilePixels));
//			{
//				GUILayout.BeginArea(new Rect(0,0,rect.width,rect.height/2);
//				if(pseudoThis.selection!=null){
//					if(!element.Tiles.ContainsKey(pseudoThis.selection)){
//						foreach(Type t in new Type[]{}){
//							if(GUILayout.Button()){
//								element.Tiles[pseudoThis.selection] = 
//							}
//						}
//					}else{
//
//					}
//
//				}
//				GUILayout.EndArea();
//				
//			}
//			GUI.EndGroup();
//			*/
//		}
//		GUI.EndGroup();
//		
//		
//		
//		
//		
//	}
//	protected override void OnSceneGUI (LevelAsset behavior)
//	{
//		return;
//	}
//	protected override float OnGetHeight (LevelAsset behavior, fiGraphMetadata metadata)
//	{
//		return 1000;//EditorStyles.numberField.CalcHeight(label, 1000);
//	}
//}
//
