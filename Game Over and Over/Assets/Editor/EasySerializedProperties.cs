﻿/*using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;
public class EasySerializedObject {
	private SerializedObject serializedObject;
	public SerializedObject SerializedObject{
		get{
			return serializedObject;
		}
		set{
			serializedObject = value;

		}
	}
	SerializedPropertyNode root;
	public EasySerializedObject(SerializedObject serializedObject){
		this.serializedObject = serializedObject;
	}
	public SerializedProperty this[string path]{
		get{

			return root[path].property;
		}
	}
	public EasySerializedProperty GetProperty(string path){
		return root[path].property;
	}
	class SerializedPropertyNode{
		public string path;
		public SerializedProperty property;
		var children = new Dictionary<string,SerializedPropertyNode>();
		public static SerializedPropertyNode FromSerializedObject (SerializedObject serializedObject){
			paths[serializedObject] = new Dictionary<string, EasySerializedProperty>();
			SerializedProperty property = serializedObject.GetIterator();
			property.NextVisible(true);
			property.NextVisible(true);
			return new EasySerializedProperty(property,property.name);
		}

		private SerializedPropertyNode(SerializedProperty property, string path){
			this.path = path;
			this.serializedObject = property.serializedObject;
			var tempProperty = property.Copy();
			//if(tempProperty == tempProperty.serializedObject.GetIterator()) tempProperty.NextVisible(true);
			//while(true){
			//	try{
			this.property = tempProperty.Copy();
			paths[serializedObject][path] = this;
			
			
			int depth = tempProperty.depth;
			bool keepGoing = tempProperty.NextVisible(true);
			int max = 100;
			while(keepGoing){
				children[tempProperty.name] = new SerializedPropertyNode(tempProperty,path+"."+tempProperty.name);
				keepGoing = tempProperty.NextVisible(false);
				max--;
				if(max<=0) break;
				
			}
			//	}catch{
			//		tempProperty.NextVisible(true);
			//		continue;
			//	}
			//	break;
			//}
			
		}
		public SerializedPropertyNode this[string path]{
			get{
				EasySerializedProperty current = this;
				foreach(string s in path.Split('.')){
					current= current[s];
				}
				return children[variableName];
			}
		}
	}
}

public class EasySerializedProperty {
	private readonly EasySerializedObject eso;
	public readonly string path;


	protected static Dictionary<object, Dictionary<string, EasySerializedProperty>> paths = new Dictionary<object, Dictionary<string, EasySerializedProperty>>();
	public static EasySerializedProperty GetESPByPath(SerializedObject serializedObject, string path){
		EasySerializedProperty esp = FromSerializedObject(serializedObject);
		foreach(string key in path.Split('.')){
			esp = esp.children[key];
		}
		return esp;
	}

	public SerializedProperty SerializedProperty{
		get{
			return property.Copy();
		}
	}
	Dictionary<string,EasySerializedProperty> children = new Dictionary<string,EasySerializedProperty>();
	public EasySerializedProperty this[string path]{
		get{
			EasySerializedProperty current = this;
			foreach(string s in path.Split('.')){
				current= current[s];
			}
			return children[variableName];
		}
	}
	public static EasySerializedProperty FromSerializedObject (SerializedObject serializedObject){
		paths[serializedObject] = new Dictionary<string, EasySerializedProperty>();
		SerializedProperty property = serializedObject.GetIterator();
		property.NextVisible(true);
		property.NextVisible(true);
		return new EasySerializedProperty(property,property.name);
	}
	public EasySerializedProperty CreateFrom(SerializedProperty property){
		switch (property.propertyType){

		case SerializedPropertyType.ObjectReference:
			//if(

			break;

	}
	private EasySerializedProperty(SerializedProperty property, string path){
		this.path = path;
		this.serializedObject = property.serializedObject;
		var tempProperty = property.Copy();
		//if(tempProperty == tempProperty.serializedObject.GetIterator()) tempProperty.NextVisible(true);
		//while(true){
		//	try{
		this.property = tempProperty.Copy();
		paths[serializedObject][path] = this;

		
		int depth = tempProperty.depth;
		bool keepGoing = tempProperty.NextVisible(true);
		int max = 100;
		while(keepGoing){
			//Debug.Log(tempProperty.depth +"   "+ tempProperty.name);
			//if(tempProperty.isArray){
			//	children[tempProperty.name] = new EasySerializedEnumerableProperty(tempProperty,path+","+tempProperty.name);
			//}else{
				children[tempProperty.name] = new EasySerializedProperty(tempProperty,path+"."+tempProperty.name);
			//}
			//if(SerializedProperty.EqualContents(tempProperty,Property.GetEndProperty())) break;
			//if(SerializedProperty.EqualContents(tempProperty,tempProperty.GetEndProperty())) break;
			//tempProperty = tempProperty.GetEndProperty();
			keepGoing = tempProperty.NextVisible(false);
			max--;
			if(max<=0) break;

		}
		//	}catch{
		//		tempProperty.NextVisible(true);
		//		continue;
		//	}
		//	break;
		//}
		
	}
	public override string ToString ()
	{
		StringBuilder sb = new StringBuilder(Property.name);
		foreach(EasySerializedProperty esp in children.Values){
			sb.AppendLine(esp.ToString(1));
		}
		return sb.ToString();
	}
	
	public string ToString (int indent)
	{
		string tabs = string.Empty;
		for(int i=0;i<indent;i++){
			//tabs+="\t";
		} 
			StringBuilder sb = new StringBuilder(path+"     "+Property.type +(Property.isArray?"[]":"")+"  " +Property.name+"\n");
		foreach(EasySerializedProperty esp in children.Values){
			sb.AppendLine(esp.ToString(indent+1));
		}
		return sb.ToString();
	}
}

public class EasySerializedEnumerableProperty  : EasySerializedProperty{
	private readonly SerializedProperty property;
	public SerializedProperty Property{
		get{
			return property.Copy();
		}
	}
	public int Count{
		get{ 
			return children.Keys.Count;
		}
	}

//	DeleteAt(object key){
//		
//		DeleteArrayElementAtIndex(int)
//	}
//	DeleteArrayElementAtIndex(int)
//		GetArrayElementAtIndex
//			InsertArrayElementAtIndex
//			MoveArrayElement

	new Dictionary<object,EasySerializedProperty> children = new Dictionary<object,EasySerializedProperty>();
	public EasySerializedProperty this[string variableName]{
		get{
			return children[variableName];
		}
	}



	public EasySerializedEnumerableProperty(SerializedProperty property): base(property){
		
		var tempProperty = property.Copy();
		//if(tempProperty == tempProperty.serializedObject.GetIterator()) tempProperty.NextVisible(true);
		//while(true){
		//	try{
		this.property = tempProperty.Copy();
		
		while(tempProperty.NextVisible(true)){
			Debug.Log(tempProperty.depth +"   "+ tempProperty.name);
		}
		
		int depth = tempProperty.depth;
		while(tempProperty.NextVisible(true) && tempProperty.depth > depth){
			Debug.Log(tempProperty.depth +"   "+ tempProperty.name);
			if(tempProperty.depth == depth+1){
				children[tempProperty.name] = new EasySerializedProperty(tempProperty);
			}
		}
		//	}catch{
		//		tempProperty.NextVisible(true);
		//		continue;
		//	}
		//	break;
		//}
		
	}

//	public override string ToString ()
//	{
//		StringBuilder sb = new StringBuilder(Property.name);
//		foreach(EasySerializedProperty esp in children.Values){
//			sb.AppendLine(esp.ToString(1));
//		}
//		return sb.ToString();
//	}
//	
//	public string ToString (int indent)
//	{
//		string tabs = string.Empty;
//		for(int i=0;i<indent;i++){
//			//tabs+="\t";
//		}
//		StringBuilder sb = new StringBuilder(tabs+Property.name);
//		foreach(EasySerializedProperty esp in children.Values){
//			sb.AppendLine(esp.ToString(indent+1));
//		}
//		return sb.ToString();
//	}
}

public class ESP_Dictionary  : EasySerializedProperty{
	private readonly SerializedProperty property;
	public SerializedProperty Property{
		get{
			return property.Copy();
		}
	}
	public int Count{
		get{ 
			return children.Keys.Count;
		}
	}

	//DeleteAt(object key){
	//	
	//	DeleteArrayElementAtIndex(int)
	//}
	//DeleteArrayElementAtIndex(int)
	//	GetArrayElementAtIndex
	//		InsertArrayElementAtIndex
	//		MoveArrayElement
	//		
	Dictionary<object,EasySerializedProperty> children = new Dictionary<object,EasySerializedProperty>();


	public EasySerializedProperty this[object key]{
		get{
			if(!baseCollection.ContainsKey(key)){
				Property.InsertArrayElementAtIndex(0);
			}

			return children[variableName];
		}
	}
	Dictionary<object,EasySerializedProperty> baseCollection = new Dictionary<object, EasySerializedProperty>();

	

	public bool ContainsKey(object value){

	}
	public ESP_Dictionary(SerializedProperty property): base(property){
		if(!property.isArray) Debug.LogError("Serialized Property must an array");
		var tempProperty = property.Copy();
		//if(tempProperty == tempProperty.serializedObject.GetIterator()) tempProperty.NextVisible(true);
		//while(true){
		//	try{
		this.property = tempProperty.Copy();
		
		while(tempProperty.NextVisible(true)){
			Debug.Log(tempProperty.depth +"   "+ tempProperty.name);
		}
		
		int depth = tempProperty.depth;
		while(tempProperty.NextVisible(true) && tempProperty.depth > depth){
			Debug.Log(tempProperty.depth +"   "+ tempProperty.name);
			if(tempProperty.depth == depth+1){
				children[tempProperty.name] = new EasySerializedProperty(tempProperty);
			}
		}
		//	}catch{
		//		tempProperty.NextVisible(true);
		//		continue;
		//	}
		//	break;
		//}
		
	}
	public override string ToString ()
	{
		StringBuilder sb = new StringBuilder(Property.name);
		foreach(EasySerializedProperty esp in children.Values){
			sb.AppendLine(esp.ToString(1));
		}
		return sb.ToString();
	}
	
	public string ToString (int indent)
	{
		string tabs = string.Empty;
		for(int i=0;i<indent;i++){
			tabs+="\t";
		}
		StringBuilder sb = new StringBuilder(tabs+Property.name);
		foreach(EasySerializedProperty esp in children.Values){
			sb.AppendLine(esp.ToString(indent+1));
		}
		return sb.ToString();
	}
}
*/
