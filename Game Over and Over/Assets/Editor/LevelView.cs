﻿//using UnityEngine;
//using UnityEditor;
//using System.Collections;
//using System.Collections.Generic;
//
///*public class LevelView{
//	static Dictionary<object,LevelView> LevelViews = new Dictionary<object, LevelView>();
//	
//
//}*/
//
//public static class EditorCustomGUI{
//
//
//
//
//
//	//static Dictionary<object,Vector2> viewerOffsets = new Dictionary<object,Vector2>();
//	//static Dictionary<object,Level> levels = new Dictionary<object, Level>();
//	private static Dictionary<object,LevelViewInfo> LevelViewInfos = new Dictionary<object, LevelViewInfo>();
//	private static LevelViewInfo GetLevelViewInfo(object key){
//		if(LevelViewInfos.ContainsKey(key)) LevelViewInfos[key] = new LevelViewInfo();
//		return LevelViewInfos[key];
//	}
//
//	private class LevelViewInfo{
//		public Vector2 viewOffset = Vector2.zero;
//		public IntCoord selection;
//		//public SerializedObject serializedObject;
//	}
//
//	public static void TileView(Rect rect, SerializedProperty sourceProperty){
//		sourceProperty = sourceProperty.Copy();
//		int minDepth = sourceProperty.depth;
//		while (sourceProperty.NextVisible(true) && sourceProperty.depth>minDepth) {
//			EditorGUILayout.PropertyField(sourceProperty);
//		}
//	}
//	static SerializedProperty last;
//	public static object LevelView(Rect rect, ref Vector2 viewOffset, object viewId, SerializedProperty LevelProperty = null){
//		if(LevelProperty != null){
//			last = LevelProperty.Copy();
//		}
//		if(last == null) return null;
//		LevelProperty = last.Copy();
//		Dictionary<IntCoord,SerializedProperty> TileProperties = new Dictionary<IntCoord, SerializedProperty>();
//		HashSet<IntCoord> SelectedTiles = new HashSet<IntCoord>();
//		//SerializedProperty TilesProperty = LevelProperty.FindPropertyRelative("Tiles");
//		SerializedProperty TilesProperty = LevelProperty.Copy().serializedObject.GetIterator();
//		//Debug.Log(TilesProperty.name);
//		while (TilesProperty.NextVisible(true) && TilesProperty.name !="Tiles") {
//			
//			//Debug.Log(TilesProperty.name);
//		}
//		//Debug.Log(TilesProperty.name);
//		if(TilesProperty.name !="Tiles"){
//			Debug.LogError("no property called 'Tiles'");
//		}else{
//
//			int TilesPropertyMinDepth = TilesProperty.depth+1;
//			SerializedProperty TileProperty;
//			while(TilesProperty.NextVisible(true) && TilesProperty.depth>TilesPropertyMinDepth){
//				//Debug.Log(TilesProperty.name);
//				if(TilesProperty.depth == TilesPropertyMinDepth+1){
//					int? x = null;
//					int? y = null;
//					if(TilesProperty.name =="key"){
//						
//						while(TilesProperty.NextVisible(true) && TilesProperty.depth>TilesPropertyMinDepth+1){
//							if(TilesProperty.name =="x"){
//								x = TilesProperty.intValue;
//							}
//							if(TilesProperty.name =="y"){
//								y = TilesProperty.intValue;
//							}
//						}
//						if(x.HasValue && y.HasValue){
//							while(TilesProperty.NextVisible(true) && TilesProperty.depth>TilesPropertyMinDepth+1){
//								if(TilesProperty.name =="value"){
//									TileProperties[new IntCoord(x.Value,y.Value)] = TilesProperty.Copy();
//									break;
//								}
//							}
//						}
//
//					}
///*
//					if(TilesProperty.name =="value"){
//						TileProperties[new IntCoord(x,y)] = TilesProperty.Copy();
//					}*/
//					/*
//						while(TilesProperty.NextVisible(true) && TilesProperty.depth>TilesPropertyMinDepth+1){
//							if(TilesProperty.name !="x"){
//								x = TilesProperty.intValue;
//							}
//							if(TilesProperty.name !="y"){
//								y = TilesProperty.intValue;
//							}
//						}
//					}
//						if(TilesProperty.depth == TilesPropertyMinDepth+2){
//					}
//					int x = TilesProperty.FindPropertyRelative("key.x").intValue;
//					int y = TilesProperty.FindPropertyRelative("key.y").intValue;
//					TileProperties[new IntCoord(x,y)] = TilesProperty.Copy();*/
//				}
//			}
//		
//		}
//		if(!LevelViewInfos.ContainsKey(viewId)){
//			LevelViewInfos[viewId] = new LevelViewInfo();
//		}
//		LevelViewInfo pseudoThis = LevelViewInfos[viewId];
//		//if(serializedObject!=null){
//		//	pseudoThis.serializedObject = serializedObject;
//		//}
//		//serializedObject = pseudoThis.serializedObject;
//		//object target = serializedObject.targetObject;
//		GUI.BeginGroup(rect);
//		rect.x = 0;
//		rect.y = 0;
//		//LevelAsset myTarget = (LevelAsset)target;
//
//		SerializedProperty SelectedProperty = null;
//		int viewX = LevelEditor.viewX;
//		int viewY = LevelEditor.viewY;
//		float tilePixels = LevelEditor.tilePixels;
//		Vector2 viewCenter = new Vector2(150,200);
//		Vector2 viewSize = new Vector2(viewX*tilePixels,viewY*tilePixels);
//		int minYIndex =  Mathf.FloorToInt(viewOffset.y/tilePixels)-Mathf.CeilToInt((float)viewY/2f);
//		int maxYIndex =  Mathf.CeilToInt(viewOffset.y/tilePixels)+Mathf.CeilToInt((float)viewY/2f);
//		GUI.Box(rect, "");//new Rect(viewCenter.x-viewSize.x/2f,viewCenter.y-viewSize.y/2f,viewSize.y,viewSize.x),""+minYIndex);
//		//GUI.BeginGroup(new Rect(viewCenter.x-viewSize.x/2f,viewCenter.y-viewSize.y/2f,viewSize.y,viewSize.x));
//		for(int y = minYIndex; y<=maxYIndex;y++){
//			float posY = -viewOffset.y + (float)viewY*tilePixels/2 + y*tilePixels; 
//			for(int x = Mathf.FloorToInt(viewOffset.x/tilePixels-(float)viewX/2f); x<=Mathf.CeilToInt(viewOffset.x/tilePixels+(float)viewX/2f);x++){
//				float posX = -viewOffset.x + (float)viewX*tilePixels/2f + x*tilePixels; 
//				//GUI.Box(new Rect(posX,posY,tilePixels,tilePixels),"a");
//				if(GUI.Button(new Rect(posX,posY,tilePixels,tilePixels),"")){
//					pseudoThis.selection = new IntCoord(x,y);
//					if(!TileProperties.ContainsKey(pseudoThis.selection)){
//
//					}
//					//SelectedProperty = LevelProperty.FindPropertyRelative("Tiles");
//						//GetArrayElementAtIndex(int index)
//				}
//			}
//		}
//		GUI.EndGroup();
//		return SelectedProperty;
//		/*
//
//		//Combatant combatant = prop as NPC_Combatant;
//		//if(myTarget.combatant == null ) myTarget.combatant = new NPC_Combatant(1,1,null);
//		//prop.objectReferenceValue = combatant;
//		
//		prop = serializedObject.GetIterator();
//		while (prop.NextVisible(true)) {
//			EditorGUILayout.PropertyField(prop);
//		}
//		
//		EditorGUILayout.Space();
//
//		
//		if (GUI.changed) {
//			serializedObject.ApplyModifiedProperties();
//		}
//		
//		//EditorGUILayout.PropertyField(prop);
//		//myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
//		//EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
//		*/
//		GUI.EndGroup();
//		
//		return null;
//	}
//	private static Dictionary<object,SerializedProperty[]> SelectedTiles = new Dictionary<object, SerializedProperty[]>();
//
//	public static void TileEditor(Rect rect, object viewId, SerializedProperty serializedProperty = null){
//		GUI.Box(rect,"");
//		GUILayout.BeginArea(rect);
//
//		int minDepth = serializedProperty.depth;
//		EditorGUILayout.PropertyField(serializedProperty);
//		if(serializedProperty.NextVisible(true)){
//			EditorGUILayout.PropertyField(serializedProperty);
//			while (serializedProperty.NextVisible(true)|| serializedProperty.depth>minDepth) {
//				EditorGUILayout.PropertyField(serializedProperty);
//			}
//		}
//
//		GUILayout.EndArea();
//	}
//}
