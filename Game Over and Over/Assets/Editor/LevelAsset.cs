using UnityEngine;
using UnityEditor;
using FullInspector;
using FullSerializer;
using System.Collections.Generic;

public class LevelAsset : BaseScriptableObject{
	[SerializeField]
	public Level level;
	
	//public  Map<PositionedTile> Tiles = new Map<PositionedTile> ();
	
	[MenuItem("Assets/Create/Level")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<LevelAsset> ();
	}
	
}