﻿
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;





public interface ITile:IMapDrawable,IHasGameObject{
	//List<object> entitiesInfulenceingTile;
	//List<object> entitiesOnTile;
	//GameObject GetOrCreateGameObject();
	//GameObject GetGameObject();
	Location Location{get;set;}
	bool IsPassableBy(IsPassableArgs args);
	void OnEnter(OnEnterArgs args); 
	void OnStay(OnStayArgs args);
	void OnExit(OnExitArgs args);
}






public class Tile :  ITile, ISpawns<ITile> {
	public Texture2D MapTexture;

	[NotSerialized]
	Location location;

	public Location Location{
		get{
			return location;
		}
		set{
			location = value;
		}
	}



	[ShowInInspector]
	[SerializeField]
	protected GameObjectReference gameobjectreference  = new GameObjectReference();
	public GameObjectReference Gameobjectreference{
		get{
			return gameobjectreference;
		}
	}

	public GameObject GetGameObject(){
		return gameobjectreference.GetGameObject();
	}
	public GameObject GetOrCreateGameObject(){
		//Debug.Log(this.Location);
		Vector3 position = this.Location.LevelTile.Location.WorldPosition;
		GameObject go = null;
		if(gameobjectreference.prefab != null){
			go = gameobjectreference.GetOrCreateGameObject(position,Quaternion.identity);
			go.transform.parent = LevelMap.MapRoot.transform; 
		}else{
			Debug.LogWarning("missing gameobject prefab");
		}
		return go;
	}
	public virtual void UpdateGameObject(){
		GameObject go = GetGameObject();
		if(go==null){
			Debug.LogError("Attempt made to update an null gameobject");
			return;
		}
		//if(OnUpdateGameObject!=null) OnUpdateGameObject.InvokeAll(go,data);
	}
	//bool hideOnUpdateGameObjectInInspector = false;
	//public PsudoEvent<Action<GameObject,Tile>> OnUpdateGameObject;

	public bool IsPassableByDefault;
	public virtual bool IsPassableBy(IsPassableArgs args){
		bool? result = null;
		if(IsPassableByTest!=null && IsPassableByTest.IsSafeToInvoke){
			result = IsPassableByTest.Invoke(args);
		}
		if(result.HasValue){
			return result.Value;
		}else{
			return IsPassableByDefault;
		}
	}

	public IFunc<IsPassableArgs,bool?> IsPassableByTest;

	public virtual void IMapDraw(Rect rect){
		GUI.Label(rect,MapTexture);
	}
	



	public PseudoEvent<OnEnterArgs> OnEnterEvent = new PseudoEvent<OnEnterArgs>();
	public PseudoEvent<OnStayArgs>  OnStayEvent  = new PseudoEvent<OnStayArgs>();
	public PseudoEvent<OnExitArgs>  OnExitEvent  = new PseudoEvent<OnExitArgs>();
	
	public void OnEnter(OnEnterArgs args){
		OnEnterEvent.InvokeAll(args);
	}
	public void OnStay(OnStayArgs args){
		OnStayEvent.InvokeAll(args);
	}
	public void OnExit(OnExitArgs args){
		OnExitEvent.InvokeAll(args);
	}
	
	public ITile Spawn(int seed){
		
		Debug.Log("tile spawn start");
		Tile spawn = new Tile();
		
		Debug.Log("tile spawn end");
		return spawn;
	}
	public void Populate(ITile spawn){
		
		Debug.Log("tile populating");
		Tile tile = (Tile)spawn;
		tile.MapTexture = MapTexture;
		//spawn.gameobjectreference = new GameObjectReference();
		tile.gameobjectreference.prefab = gameobjectreference.prefab;
		
		tile.IsPassableByDefault = IsPassableByDefault;
		tile.IsPassableByTest = IsPassableByTest;
		tile.OnEnterEvent = new PseudoEvent<OnEnterArgs>(OnEnterEvent);
		tile.OnStayEvent = new PseudoEvent<OnStayArgs>(OnStayEvent);
		tile.OnExitEvent = new PseudoEvent<OnExitArgs>(OnExitEvent);
	}
}




//public class OpenSpace: Tile2{
//	public abstract void IMapDraw(Rect rect){
//		GUI.
//	}
//	public override Color? Color{
//		get{
//			return new Color(.5f,.5f,.5f);
//		}
//	}
//	
//	public override bool IsPassableBy(object o){
//		return true;
//	}
//}
//public class Wall: Tile2{
//	public override Color? Color{
//		get{
//			return new Color(1,1,1);
//		}
//	}
//	
//	public override bool IsPassableBy(object o){
//		return false;
//	}
//}
//public class Door: Tile2{
//	Location destination;
//	public override Color? Color{
//		get{
//			return new Color(0,1,0);
//		}
//	}
//	public Door(Location destination){
//		this.destination = destination;
//	}
//	public override bool IsPassableBy(object o){
//		return true;
//	}
//	public override void OnEnter(Guid id){
//		Save ls = Save.loadedSave;
//		//if(Destination.Tile is Door) Debug.LogError("Potential Door loop");
//		
//		/*ls.Add(
//				ls.CurrentT, 
//				new GameEvent.CharacterSetLocation(
//					characterIndex,
// 					ls.Characters[characterIndex].Location,
//					destination,
//					true
//					)
//			);*/
//	}
//}
//public class EnemyTile: Tile2{
//	public Action Reset;
//
//	EnemyInstance[] enemies;
//
//	public override Color? Color{
//		get{
//			return new Color(0,.5f,0);
//		}
//	}
//	public EnemyTile(){
//		/*Enemy[] enemies = new GreyRat[]{new GreyRat(),new GreyRat(),};
//		Reset = delegate() {
//			foreach(Enemy e in enemies) e.OnResetDelegate();
//			this.enemies = enemies;
//		};
//		Reset();
//		Save.loadedSave.OnReset += OnReset;
//		*/
//	}
//
//	void OnReset (object sender, EventArgs e)
//	{
//		Reset();
//	}
//	public override bool IsPassableBy(object o){
//		return true;
//	}
//	public override void OnEnter(Guid id){
//		Save ls = Save.loadedSave;
//		bool allDead = true;
//		foreach(EnemyInstance enemy in enemies){
//			if(!enemy.IsDead){
//				allDead = false;
//				break;
//			}
//		}
//
//		if(!allDead){
//		/*	ls.Add(
//				ls.CurrentT,
//			    new GameEvent.EnterCombatEvent(
//					characterIndex,
//					enemies,
//					true
//					)
//				);*/
//		}
//	}
//}
/*
public class CustomPassability{
	bool defaultPassability;
	object[] exceptions;

	public bool IsPassableBy(object o){
		return false;
	}
}
*/
