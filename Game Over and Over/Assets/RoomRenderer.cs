using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
public class RoomRenderer : MonoBehaviour {
	
	private static RoomRenderer _inst;
	public static RoomRenderer Inst{
		get{
			return _inst;
		}
	}
	void Awake(){
		_inst = this;


	}
	void Update(){
		
		_inst = this;
		if(Save.LoadedSave.ActiveCharacter != null
		   &&Save.LoadedSave.ActiveCharacter.Entity.Location.Map != null){
			if(Save.LoadedSave.ActiveCharacter.Entity.Location.Map != LevelMap.ActiveMap){
				LevelMap.SetActiveMap(Save.LoadedSave.ActiveCharacter.Entity.Location.Asset);
			}
		}
	}
	public GameObject containerRooms;
	public GameObject prefabWall01; 
	public GameObject prefabFloor01;
	public GameObject meshCombiner;
*/
	/*
	ParticleSystem.Particle[] particles;
	Dungeon lastDungeon;
	Dungeon dungeon{
		get{
			return Dungeon.ActiveDungeon;
		}
	}
	
	int xSize{
		get{
			return dungeon.xSize;
		}
	}
	int ySize{
		get{
			return dungeon.ySize;
		}
	}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(dungeon!=null){
			IntCoord coord = new IntCoord();
			if(dungeon!=lastDungeon){
				particles = new ParticleSystem.Particle[xSize*ySize];
				for(int i=0;i<xSize*ySize;i++){
					coord.x = i%xSize;
					coord.y = Mathf.FloorToInt((float)i/(float)xSize);
					particles[i]= new ParticleSystem.Particle();
					particles[i].size = 1;
					particles[i].position = new Vector3((float)xSize/2f-(float)coord.x,(float)ySize/2f-(float)coord.y,0f);
				}
				lastDungeon = dungeon;
			}
			
			if(!dungeon.isDirty) return;

			var chars = new Dictionary<IntCoord, Color>();
			Character c;
			for(int i=1;i<=Save.loadedSave.OnLifeNumber;i++){
				c = Save.loadedSave.Characters[i];
				if(c==null)continue;
				if(c.Location.dungeon == dungeon){
					//Debug.Log(c.location.coord.x+","+c.location.coord.y);
					if(i!=Save.loadedSave.OnLifeNumber){
						chars[c.Location.coord] = new Color(.75f,.3f,.75f);
					}else{
						chars[c.Location.coord] = Color.magenta;
					}
				}
			}
			//bool drawCharacter = (character.location.room == this);
			for(int i=0;i<xSize*ySize;i++){
				coord.x = i%xSize;
				coord.y = Mathf.FloorToInt((float)i/(float)xSize);
				//IEntityInRoom o;
				//if(drawCharacter && character.location.coord.Equals(coord)){
				//	particles[i].color = Color.magenta;//(Color)o.Color;
				if(chars.ContainsKey(coord)){
					particles[i].color = chars[coord];
				//}else if(objectsInRoom.TryGetValue(coord,out o) && o.Color!=null){
				//	particles[i].color = (Color)o.Color;
				}else{
					particles[i].color = (Color)dungeon[coord.x,coord.y].Color;
				}
				particles[i].lifetime=100;
			}
			particleSystem.SetParticles(particles,xSize*ySize);
		}
	}
	*/
//}