using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;

[AttributeUsage(AttributeTargets.Class, Inherited = true)]
public class PreUserInputPhaseAttribute : Attribute
{
}

[Serializable]
public abstract class GameEvent{
	
	//[SerializeField]
	//public readonly TurnStage stage = TurnStage.Tick;
	[SerializeField]
	public bool isNewEvent = true;
	[SerializeField]
	protected List<Guid> objectIDsCreated = new List<Guid>();
	public Guid[] ObjectIDsCreated{
		get{
			return objectIDsCreated.ToArray();
		}
	}
	protected T ObjectIDTo<T>(Guid id) where T:class{
		return Save.LoadedSave.GetObjectByInstanceId<T>(id);
	}
	//public readonly Tick.Phase phase = Tick.Phase.PostUserInput;

	public GameEvent(){
	}
	public GameEvent( bool isNewEvent){
		this.isNewEvent = isNewEvent; 
	}

	public IEnumerator GetEnumerator(){
		IEnumerator enumerator = Execute();
		isNewEvent = false;
		return enumerator;
	}
	private IEnumerator Execute(){

		IEnumerator ienumerator;
		ienumerator = _Execute();
		while(ienumerator.MoveNext()){
			yield return ienumerator.Current;
		}
		foreach(GameEvent ge in subEvents){
			if(ge == null)continue;
			ienumerator = ge.GetEnumerator();
			while(ienumerator.MoveNext()){
				yield return ienumerator.Current;
			}
		}
	}
	protected virtual IEnumerator _Execute(){ 
		yield break;
	}

	public readonly List<GameEvent> subEvents = new List<GameEvent>();

	public override string ToString ()
	{
		return this.GetType().Name;
	}


	public static class Special{
		public static UserInput UserInput{
			get{
				return UserInput.Default;
			}
		}



	}
	public class UserInput : GameEvent{
		static UserInput _UserInput;
		public static UserInput Default{
			get{
				if(_UserInput == null) _UserInput = new UserInput();
				return _UserInput;
			}
		}
		
		private UserInput():base(true){
		}
		
		protected override IEnumerator _Execute(){
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	public class FrameEnd : GameEvent{
		static FrameEnd _FrameEnd;
		public static FrameEnd Default{
			get{
				if(_FrameEnd == null) _FrameEnd = new FrameEnd();
				return _FrameEnd;
			}
		}
		private FrameEnd():base(true){
		}
	}
	
	#region OutOfCombatEvents
	
	
	public abstract class OutOfCombatEvent:GameEvent{

		protected OutOfCombatEvent(){}
		public OutOfCombatEvent( bool isNewEvent):base(isNewEvent){
		}
	}
	
	/// <summary>
	/// Character walk.
	/// </summary>
	public class CharacterWalk : OutOfCombatEvent{
		
		public Instance<Player> PlayerInstance;
		public Location from;
		public IntCoord movement;

		private CharacterWalk(){}
		public CharacterWalk( Location from, IntCoord movement, bool isNewEvent):base(isNewEvent){
			this.PlayerInstance = Save.LoadedSave.ActiveCharacter;
			this.from = from;
			this.movement = movement;
		}
		
		protected override IEnumerator _Execute(){
			PlayerInstance.ToInstanceOf<ILocated>().MoveTo(from+movement);
			PlayerInstance.Entity.CoolDown+=1;
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}

		}
	}
	
	/// <summary>
	/// sets Character location.
	/// </summary>
	
	[Serializable]
	public class SetLocation : OutOfCombatEvent{
		
		public Instance<ILocated> instance;
		public Location from;
		public Location to;
		//public string SendMessage;
		//public float duration;
		public SetLocation( Instance<ILocated> instance, Location from, Location to, bool isNewEvent):base(isNewEvent){
			this.instance = instance;
			this.from = from;
			this.to = to;
			//this.SendMessage = sendMessage;
			//this.duration = duration;

		}
		
		protected override IEnumerator _Execute(){

			//if(isNewEvent) 
				//from.Tile.OnPlayerExit(characterIndex);
			instance.MoveTo(to);
			/*if(spawn is ILocated &&  spawn is IHasGameObject){
				Location l = ((ILocated)spawn).Location;
				if(l.Map == LevelMap.ActiveMap){
					((IHasGameObject)(spawn)).GetOrCreateGameObject().transform.position = l.LevelTile.WorldPosition;
				}
			}
			try{
				moveable.Location = to;
			}catch(Exception e){
				//Debug.Log(Save.loadedSave.ToString());
				throw e; 
			}*/
			//if(isNewEvent) to.Tile.OnCharacterEnter(characterIndex);
			
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
		public override string ToString ()
		{
			return base.ToString();
		}
	}
	[Serializable]
	public class Spawn<T> : GameEvent where T:class{
		[SerializeField]
		ISpawns<T> o;
		[SerializeField]
		Guid id;
		public Guid IDOfSpawn{
			get{return id;}
		}
		public Spawn():base(){}
		public Spawn( ISpawns<T> o, bool isNewEvent):base(isNewEvent){
			this.o = o;
			this.id = Guid.NewGuid();
			objectIDsCreated.Add(id);
		}
		
		protected override IEnumerator _Execute(){
			Save.LoadedSave.Spawn(o,id);
			
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
		public override string ToString ()
		{
			return o.ToString()+" with a Guid of "+id.ToString();
		}
	}
	/*
	/// <summary>
	/// spawns Character at location.
	/// </summary>
	[Serializable]
	public class SpawnAtLocation : OutOfCombatEvent{
		
		public int characterIndex;
		public Location location;
		
		public SpawnAtLocation( Location location, bool isNewEvent):base(isNewEvent){
			this.characterIndex = Save.loadedSave.OnLifeNumber;
			this.location = location;
		}
		
		protected override void _Execute(){
			Debug.Log("Life "+characterIndex+" Spawned");
			Save.loadedSave.characters[characterIndex]=new PlayerInstance(location,characterIndex,Save.loadedSave.seed+characterIndex);
			//if(isNewEvent) location.Tile.OnCharacterEnter(characterIndex);
			base._Execute();
		}
		public override string ToString ()
		{
			return base.ToString();
		}
	}
	*/
	#endregion
	
	
	#region InCombatEvents
	
	public abstract class InCombatEvent:GameEvent{
		
		public InCombatEvent( bool isNewEvent):base(isNewEvent){
		}
	}
	public class NPCAttackPlayer:InCombatEvent{
		public OnUseArgs onUseArgs;
		public IFunc<OnUseArgs,IEnumerable<Instance<Combatant>>,GameEvent> EventGenerator;


		public NPCAttackPlayer(OnUseArgs onUseArgs, IFunc<OnUseArgs,IEnumerable<Instance<Combatant>>,GameEvent> EventGenerator, bool isNewEvent):base(isNewEvent){
			if(!EventGenerator.IsSafeToInvoke) throw new System.Exception("unsafe EventGenerator");
			this.EventGenerator = EventGenerator;
			this.onUseArgs = onUseArgs;
		}
		protected override IEnumerator _Execute(){
			if(!onUseArgs.combatant.Entity.IsDead && Save.LoadedSave.ActiveCharacter.Exists){
				if(onUseArgs.combatant.Entity.Foes.Contains(Save.LoadedSave.ActiveCharacter.Entity)){
					subEvents.Add(EventGenerator.Invoke(onUseArgs,new Instance<Combatant>[]{Save.LoadedSave.ActiveCharacter.ToInstanceOf<Combatant>(),}));
				}
			}

			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	public class Attack:InCombatEvent{
		public IEnumerable<Instance<Combatant>> targets;
		public Instance<Combatant> attacker;
		public AttackDamage attackDamage;
		
		public Attack( Instance<Combatant> target,Instance<Combatant> attacker,  AttackDamage attackDamage, bool isNewEvent):base(isNewEvent){
			this.targets = new List<Instance<Combatant>>(){target,};
			this.attacker = attacker;
			this.attackDamage = attackDamage;
		}
		public Attack( IEnumerable<Instance<Combatant>> targets,Instance<Combatant> attacker,  AttackDamage attackDamage, bool isNewEvent):base(isNewEvent){
			this.targets = targets;
			this.attacker = attacker;
			this.attackDamage = attackDamage;
		}
		protected override IEnumerator _Execute(){
			foreach(Instance<Combatant> target in targets){
				target.Entity.TakeAttack(new OnAttackedArgs(target,attacker,attackDamage)); 
			}
			
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	
	
	#endregion
	
	
	#region other game actions
	public class EasyGameEvent:GameEvent{
		//HACK this class opens game to potential hacking, but will also allow faster dev
		public Dictionary<string, ValueType> data = new Dictionary<string, ValueType>();

		//public Expression< Action< Dictionary<string, ValueType> > > action = ;
			
		protected override IEnumerator _Execute(){
			//action.Compile()(data);
			
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}

	public class EnterCombatEvent:GameEvent{
		
		public Guid characterId;
		public Guid[] enemyIds;
		
		public EnterCombatEvent( Guid characterId, IEnumerable<Guid> enemyIds, bool isNewEvent):base(isNewEvent){
			this.characterId = characterId;
			this.enemyIds = enemyIds.ToArray();
		}
		
		protected override IEnumerator _Execute(){
			//if(characterIndex == Save.loadedSave.OnLifeNumber){
			IEnumerable<Combatant> enemies = enemyIds.Select( id=>(Save.LoadedSave.GetObjectByInstanceId(id) as Combatant) );
			if(!enemies.AllDead()){
				(Save.LoadedSave.GetObjectByInstanceId(characterId) as Player).EnterCombat(enemies);
				//Debug.Log(Save.loadedSave.Characters[characterIndex].Allies.Count);
			}
			/*Game.State = 
			OutOfCombatView.Inst.enabled = false;
			CombatView.Inst.enabled = true;*/
			//}
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	public class ExitCombatEvent:GameEvent{
		
		public int characterIndex;
		
		public ExitCombatEvent( int characterIndex, bool isNewEvent):base(isNewEvent){
			this.characterIndex = characterIndex;
		}
		
		protected override IEnumerator _Execute(){
			if(characterIndex == Save.LoadedSave.OnLifeNumber){
				/*OutOfCombatView.Inst.enabled = true;
			CombatView.Inst.enabled = false;*/
			}
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	public class GearChangeEvent:GameEvent{
		
		public int characterIndex;
		
		public GearChangeEvent( int characterIndex, bool isNewEvent):base(isNewEvent){
			this.characterIndex = characterIndex;
		}
		
		protected override IEnumerator _Execute(){
			
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	public class ItemPickupEvent:GameEvent{
		
		//public int characterIndex;
		public IEnumerable<Item> items;
		public ItemPickupEvent( IEnumerable<Item> items, bool isNewEvent):base(isNewEvent){
			//this.characterIndex = characterIndex;
		}
		
		protected override IEnumerator _Execute(){
			Save.LoadedSave.AvalableItems.UnionWith(items);
			
			IEnumerator baseIE = base._Execute();
			while(baseIE.MoveNext()){
				yield return baseIE.Current;
			}
		}
	}
	
	
	#endregion
}


/*
public class GameEventComparer : IComparer<GameEvent>
{
	private static GameEventComparer _default;
	public static GameEventComparer Default{
		get{
			if(_default == null) _default = new GameEventComparer();
			return _default;
		}
	}
	int IComparer<GameEvent>.Compare(GameEvent a, GameEvent b)
	{
		if (a.t == b.t){
			return a.phase.CompareTo(b.phase);
		}
		else
		{
			return decimal.Compare(a.t,b.t);
		}
	}
}*/
