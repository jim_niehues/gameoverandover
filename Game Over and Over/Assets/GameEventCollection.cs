using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;

/*
public class GameEventComparer : IComparer<GameEvent>
{
	private static GameEventComparer _default;
	public static GameEventComparer Default{
		get{
			if(_default == null) _default = new GameEventComparer();
			return _default;
		}
	}
	int IComparer<GameEvent>.Compare(GameEvent a, GameEvent b)
	{
		if (a.t == b.t){
			return a.phase.CompareTo(b.phase);
		}
		else
		{
			return decimal.Compare(a.t,b.t);
		}
	}
}*/
public class GameEventCollection: IEnumerator{
	public GameEventCollection(){
		baseCollection = new SortedDictionary<decimal,Dictionary<TurnStage,PerTurnStageCollection>>();
		baseIEnumerator = Update();
	}
	protected virtual IEnumerator Update(){
		yield return null;
		IEnumerator stageEnumerator;
		while(true){

			if( CurrentT<0 ){ 
				foreach(TurnStage stage in Enum.GetValues(typeof(TurnStage))){
					CurrentStage = stage;
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					OnStageExit();
				}
			}else{
				IEnumerable<Combatant> combatants = GetAllSpawnedObjects().Where(o=>o is Combatant).Select<object,Combatant>(o=>(Combatant)o);
				
				IEnumerable<Combatant> OnTurnCombatants = combatants.Where(c=>c.IsTurn(CurrentT));


				{
					CurrentStage = TurnStage.Tick;

					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					foreach(Combatant combatant in combatants){
						combatant.OnTimeStep(new OnTimeStepArgs(CurrentT,Instance<Combatant>.Get(combatant)));
						if(combatant.WarmUp>0)combatant.WarmUp-=Tick.length;
						if(combatant.CoolDown>0)combatant.CoolDown-=Tick.length;
					}
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					OnStageExit();
				}
				{
					CurrentStage = TurnStage.TurnStart;

					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					foreach(Combatant combatant in OnTurnCombatants){
						combatant.OnTurnStart(CurrentT);
					}
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					OnStageExit();
				}

				/*foreach(Combatant combatant in OnTurnCombatants.Where(otc=>!(otc.IsActiveCharacter()))){
					combatant.OnTurn();
				}*/
				{
					CurrentStage = TurnStage.Turn;
					
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					foreach(Combatant combatant in OnTurnCombatants){
						if(combatant.WarmUp==0 && combatant.DoWhenWarmupComplete.Count>0){
							Action[] toDo = combatant.DoWhenWarmupComplete.ToArray();
							combatant.DoWhenWarmupComplete.Clear();
							foreach(Action action in toDo){
								if(action!=null) action();
							}
						}
					}
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}

					if(Save.LoadedSave.ActiveCharacter!=null && Save.LoadedSave.ActiveCharacter.Exists){
						if(Save.LoadedSave.ActiveCharacter.Entity.IsTurn(CurrentT)){
							do{
								Player p = Save.LoadedSave.ActiveCharacter.Entity;
								if(p.WarmUp==0 && p.DoWhenWarmupComplete.Count>0){
									Action[] toDo = p.DoWhenWarmupComplete.ToArray();
									p.DoWhenWarmupComplete.Clear();
									foreach(Action action in toDo){
										if(action!=null) action();
									}
								}
								stageEnumerator = CurrentEvents.Execute();
								while(stageEnumerator.MoveNext()){
									yield return stageEnumerator.Current;
								}
								IEnumerator OnTurnEnumerator = Save.LoadedSave.ActiveCharacter.Entity.OnTurn(CurrentT);
								while(OnTurnEnumerator.MoveNext()){
									yield return OnTurnEnumerator.Current;
								}
								yield return null;
							} while(Save.LoadedSave.ActiveCharacter.Entity.IsTurn(CurrentT));
							Debug.Log("Player Turn at "+CurrentT+" is Done");
						}
					}
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					OnStageExit();
				}
				{
					CurrentStage = TurnStage.TurnEnd;
					
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					foreach(Combatant combatant in OnTurnCombatants){
						combatant.OnTurnEnd(CurrentT);
					}
					stageEnumerator = CurrentEvents.Execute();
					while(stageEnumerator.MoveNext()){
						yield return stageEnumerator.Current;
					}
					OnStageExit();
				}
				yield return false;
			}
			CurrentT+= Tick.length;
			Debug.Log("Time is now "+CurrentT);
		}
	}
	[NonSerialized]
	IEnumerator baseIEnumerator;
	/*bool IEnumerator<bool>.Current
	{
		get{
			return baseIEnumerator.Current;
		}
	}*/
	object IEnumerator.Current
	{
		get{
			return baseIEnumerator.Current;
		}
	}
	void IEnumerator.Reset(){
		Reset();
	}
	/*void IDisposable.Dispose(){
		baseIEnumerator.Dispose();
	}*/
	public bool MoveNext (){
		return baseIEnumerator.MoveNext();
	}

	protected virtual void Reset (){
		CurrentT=-1;
		ObjectsByGuid.Clear();
		GuidsByObject.Clear();
		baseIEnumerator = Update();
	}

	[SerializeField]
	public readonly HashSet<Guid> reservedObjectID = new HashSet<Guid>();

	[SerializeField]
	public readonly Dictionary<Guid,Type> TypeByGuid = new Dictionary<Guid, Type>();

	public Type GetTypeByGuid(Guid id){
		if(TypeByGuid.ContainsKey(id)){
			return TypeByGuid[id];
		}
		return null;
	}

	//clones
	[NonSerialized]
	Dictionary<Guid,object> ObjectsByGuid = new Dictionary<Guid, object>();
	[NonSerialized]
	Dictionary<object,Guid> GuidsByObject = new Dictionary<object, Guid>();

	public object[] GetAllSpawnedObjects(){
		return ObjectsByGuid.Values.ToArray();
	}
	public T Spawn<T>(ISpawns<T> spawner, Guid id) where T:class{
		//Debug.Log("Spawning a "+typeof(T).PrettyName());
		//if(!o.IsOriginal) throw new Exception("You can't spawn from a clone");
		if(ObjectsByGuid.ContainsKey(id)) throw new Exception("Object was already spawned");
		T spawn = spawner.Spawn(id.GetHashCode());
		TypeByGuid[id]= spawn.GetType();
		ObjectsByGuid.Add(id,spawn);
		GuidsByObject.Add(spawn,id);
		spawner.Populate(spawn);
		//Instance<T> instance = Instance<T>.Get(id);
		
		Debug.Log("spawned "+spawn.GetType().PrettyName());
		return spawn;
	}
	/*
	public Instance<T> CreateNewInstance<T>(T o) where T:class,ISpawnable{
		Guid newId = Guid.NewGuid();
		int i = 1000;
		while(TypeByGuid.ContainsKey(newId)){
			if(i<0){
				throw new Exception("Had trouble finding an unused GUID");
			}
			newId = Guid.NewGuid();
			i--;
		}
		TypeByGuid.Add(newId,o.GetType());
		
		return new Instance<T>(newId);
	}*/

	public bool HasBeenSpawned(Guid id){
		return ObjectsByGuid.ContainsKey(id);
	}
	public System.Object GetObjectByInstanceId(Guid id){
		if(ObjectsByGuid.ContainsKey(id)){
			return ObjectsByGuid[id];
		}else if(TypeByGuid.ContainsKey(id)){
			throw new System.Exception("Attepted to access a(n) "+TypeByGuid[id].PrettyName()+" before Object was spawned");
		}else{
			throw new System.Exception("Unknown ObjectID");
		}
	}

	public Guid GetInstanceIdFromObject(object o){
		if(GuidsByObject.ContainsKey(o)){
			return GuidsByObject[o];
		}
		throw new System.Exception("Unknown Object");
	}

	public T GetObjectByInstanceId<T>(Guid id) where T : class{
		return (T)GetObjectByInstanceId(id);
	}

	public bool waitingForInput;
	private decimal _t;
	private TurnStage _stage;
	public virtual TurnStage CurrentStage{
		get{
			return _stage;
		}
		protected set{
			//Debug.Log(value);
			OnStageExit();

			_stage = value;
		}
	}
	public virtual decimal CurrentT{
		get{
			return _t;
		}
		protected set{
			//Debug.Log(value);
			OnStageExit();
			CurrentStage = (TurnStage)0;
			_t = value;
		}
	}
	
	[SerializeField]
	protected SortedDictionary<decimal,Dictionary<TurnStage,PerTurnStageCollection>> baseCollection;

	protected PerTurnStageCollection CurrentEvents{
		get{
			return this[CurrentT, CurrentStage];
		}
	}
	protected PerTurnStageCollection this[decimal time, TurnStage stage]
	{
		get
		{
			if(baseCollection ==null) baseCollection = new SortedDictionary<decimal,Dictionary<TurnStage,PerTurnStageCollection>>();
			Dictionary<TurnStage,PerTurnStageCollection> perTurnCollection;
			if(!baseCollection.TryGetValue(time, out perTurnCollection)){
				perTurnCollection = new Dictionary<TurnStage,PerTurnStageCollection>();
				baseCollection[time] = perTurnCollection;
			}
			PerTurnStageCollection perStageCollection;
			if(!perTurnCollection.TryGetValue(stage, out perStageCollection)){
				perStageCollection = new PerTurnStageCollection();
				perTurnCollection[stage] = perStageCollection;
			}
			return perStageCollection;
		}
	}

	public void Add(decimal time,TurnStage stage, GameEvent gameEvent){
		if(gameEvent==null){
			throw new System.Exception("A null GameEvent cannot be Added");
		}else{
			if(time<CurrentT||(time==CurrentT && ((int)stage<(int)CurrentStage))) throw new Exception("Can't insert an new GameEvent into the past!");

			this[time,stage].AddLast(gameEvent);
			if(gameEvent.ObjectIDsCreated !=null){
				foreach(Guid id in gameEvent.ObjectIDsCreated){
					reservedObjectID.Add(id);
				}
			}
		}
	}

	/// <summary>
	/// Play current turn.
	/// Returns true if waiting for user input;
	/// </summary>
	/*public virtual void ExecuteEvents(TurnStage stage){
		
		if(this[CurrentT].Done) CurrentT+= Tick.length;
		//Debug.Log("play() time "+CurrentT);
		this[CurrentT].Play();
		//yield return waitingForInput;
	}*/

	private void  OnStageExit(){
		this[CurrentT,CurrentStage].OnExit();
	}
	protected class PerTurnStageCollection:LinkedList<GameEvent>{
		LinkedListNode<GameEvent> lastExecutedEvent;

		public bool Done{
			get{
				return First == null || ( lastExecutedEvent!=null && lastExecutedEvent.Next == null );
			}
		}

		/// <summary>
		/// Play this instance.
		/// Returns true if waiting for user input;
		/// </summary>
		public IEnumerator Execute(){
			//HashSet<GameEvent> playedEvents= new HashSet<GameEvent>();
			LinkedListNode<GameEvent> currentEvent;
			if(lastExecutedEvent==null){
				currentEvent = First;
			}else{
				currentEvent = lastExecutedEvent.Next;
			}
			while(true){
				if(currentEvent==null){
					yield break;
				}
				//if(playedEvents.Contains(currentEvent)) throw new Exception("Infinite loop of GameEvents detected");
				
				lastExecutedEvent = currentEvent; // setting as last event prior to executing in case execution adds events
				IEnumerator ienumerator = lastExecutedEvent.Value.GetEnumerator();
				while(ienumerator.MoveNext()){
					yield return ienumerator.Current;
				}
				
				currentEvent = lastExecutedEvent.Next;
			}
		}
		public void OnExit(){
			lastExecutedEvent = null;
		}
	}
	/*
	protected class PerTurnCollection{
		HashSet<GameEvent> newEvents = new HashSet<GameEvent>();
		GameEvent lastEvent;
		Dictionary<TurnStage,PerTurnStageCollection> perStageCollection = new Dictionary<TurnStage, PerTurnStageCollection>();
		[SerializeField]
		public GameEvent FirstEvent{get;private set;}
		public bool Done{get;private set;}

		public PerTurnCollection(){
			foreach(TurnStage stage in Enum.GetValues(typeof(TurnStage))){
				perStageCollection
			}
		}

		/// <summary>
		/// Add the specified gameEvent.
		/// will not add sequentialy. To Add Sequential gameEvents use Add(IEnumerable<GameEvent> gameEvents).
		/// </summary>
		/// <param name="gameEvent">Game event.</param>
		public void Add(GameEvent gameEvent){
			newEvents.Add(gameEvent);
			if(lastEvent == null){
				gameEvent.Next = FirstEvent;
				FirstEvent = gameEvent;
			}else{
				gameEvent.Next = lastEvent.Next;
				lastEvent.Next = gameEvent;
			}
		}
		/// <summary>
		/// Add the specified gameEvents sequentialy.
		/// </summary>
		/// <param name="gameEvents">Game events.</param>
		public void Add(IEnumerable<GameEvent> gameEvents){
			GameEvent lastLink = null;
			GameEvent relinkAtEnd;
			if(lastEvent == null){
				relinkAtEnd = FirstEvent;
			}else{
				relinkAtEnd = lastEvent.Next;
				lastLink = lastEvent;
			}

			foreach(GameEvent gameEvent in gameEvents){
				newEvents.Add(gameEvent);
				if(lastLink == null){ // if first link
					FirstEvent = gameEvent;
				}else{
					lastLink.Next = gameEvent;
				}
				lastLink = gameEvent;
			}
			lastLink.Next = relinkAtEnd;
		}
		/// <summary>
		/// Play this instance.
		/// Returns true if waiting for user input;
		/// </summary>
		public void Play(TurnStage stage){
			if(!perStageCollection.ContainsKey(stage)) return;
			else{
				perStageCollection[stage].Play();
			}
			HashSet<GameEvent> playedEvents= new HashSet<GameEvent>();
			GameEvent currentEvent;
			if(lastEvent==null){
				currentEvent = FirstEvent;
			}else{
				currentEvent = lastEvent.Next;
			}
			while(true){
				if( Save.LoadedSave.NextPlayerTurn <= Save.LoadedSave.CurrentT && (currentEvent==null||!newEvents.Contains(currentEvent)) ){
					//if it is still the player's turn only run until the next gameEvent that would be Executed is not a newEvent
					Done = false;
					return true; 
				}else if(currentEvent==null){
					Done = true;
					return false;
				}
				if(playedEvents.Contains(currentEvent)) throw new Exception("Infinite loop of GameEvents detected");
				
				lastEvent = currentEvent; // setting as last event prior to executing in case execution adds events
				lastEvent.GetEnumerator();
				playedEvents.Add(lastEvent);
				
				currentEvent = lastEvent.Next;
			}
		}
		public void OnExit(){
			newEvents = new HashSet<GameEvent>();
			lastEvent = null;
			Done = false;
		}
	}*/
}
