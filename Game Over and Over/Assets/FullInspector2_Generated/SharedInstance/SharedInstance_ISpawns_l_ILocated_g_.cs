// This is an automatically generated script that is used to remove the generic 
// parameter from SharedInstance<T, TSerializer> so that Unity can properly serialize it.

using System;

namespace FullInspector.Generated.SharedInstance {
    public class SharedInstance_ISpawns_l_ILocated_g_ : SharedInstance<ISpawns<ILocated>> {}
}
