﻿using UnityEngine;
using System.Collections;
using FullInspector;


public class LocationMutable{
	public LevelMapAsset MapAsset;
	public IntCoord Coord;
	public Location Location{
		get{
			return new Location(MapAsset,Coord);
		}
	}
}
