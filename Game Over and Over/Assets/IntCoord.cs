﻿using UnityEngine;
using System.Collections;
using System;


[Serializable]
public struct IntCoord {

	public static readonly IntCoord Origin = new IntCoord(0,0);

	[SerializeField]
	public int x;
	[SerializeField]
	public int y;
	//[SerializeField]
	//public int elevation;

	

	//public IntCoord(){}
	public IntCoord(int x, int y){
		this.x = x;
		this.y = y;
	}
	public Vector2 AsVector2{
		get{
			return new Vector2(x,-y);
		}
	}
	/*
	public IntCoord(){
		this(0,0);
	}*/

	
	public static readonly IntCoord North = new IntCoord(0,1);
	public static readonly IntCoord South = new IntCoord(0,-1);
	public static readonly IntCoord East = new IntCoord(1,0);
	public static readonly IntCoord West = new IntCoord(-1,0);
	
	public IntCoord NorthNeighbor{
		get{
			return this + North;
		}
	}
	public IntCoord SouthNeighbor{
		get{
			return this + South;
		}
	}
	public IntCoord EastNeighbor{
		get{
			return this + East;
		}
	}
	public IntCoord WestNeighbor{
		get{
			return this + West;
		}
	}
	public IntCoord[] Neighbors{
		get{
			return new IntCoord[]{NorthNeighbor,SouthNeighbor,EastNeighbor,WestNeighbor};
		}
	}
	public static IntCoord[] Directions{
		get{
			return new IntCoord[]{North,South,East,West};
		}
	}

	public static int DistanceSqr (IntCoord a, IntCoord b)
	{
		return ((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
	}
	
	public int CompareTo(object obj){
		if (!(obj is IntCoord))
			return 1;
		
		IntCoord otherIntCoord = (IntCoord) obj;
		if(x!=otherIntCoord.x) return 1;
		if(y!=otherIntCoord.y) return 1;
		return 0;
	}

	public override bool Equals(System.Object obj)
	{
		if (!(obj is IntCoord))
			return false;
		
		IntCoord otherIntCoord = (IntCoord) obj;
		if(x==otherIntCoord.x && y==otherIntCoord.y){
			return true;
		}else{
			return false;
		}
	}
	
	public bool Equals(IntCoord otherIntCoord)
	{
		if(x==otherIntCoord.x && y==otherIntCoord.y){
			return true;
		}else{
			return false;
		}
	}
	
	public override int GetHashCode()
	{
		return (x%Int16.MaxValue)*Int16.MaxValue + (y%Int16.MaxValue);
	}

	public static bool operator ==(IntCoord a, IntCoord b)
	{
		if(a.x==b.x && a.y==b.y){
			return true;
		}else{
			return false;
		}
		/*
		// If both are null, or both are same instance, return true.
		if (System.Object.ReferenceEquals(a, b))
		{
			return true;
		}
		
		// If one is null, but not both, return false.
		if (((object)a == null) || ((object)b == null))
		{
			return false;
		}
		
		// Return true if the fields match:
		return a.x == b.x && a.y == b.y;
		*/
	}

	public static IntCoord operator +(IntCoord a, IntCoord b)
	{
		return new IntCoord(a.x+b.x,a.y+b.y);
	}

	public static IntCoord operator -(IntCoord a, IntCoord b)
	{
		return new IntCoord(a.x-b.x,a.y-b.y);
	}
	
	public static IntCoord operator *(IntCoord a, int b)
	{
		return new IntCoord(a.x*b,a.y*b);
	}

	public static bool operator !=(IntCoord a, IntCoord b)
	{
		return !(a == b);
	}


	public override string ToString ()
	{
		return string.Format ("IntCoord(x="+x+",y="+y+")");
	}
}
