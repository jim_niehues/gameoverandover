using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OutOfCombatView : MonoBehaviour {
	private static OutOfCombatView _inst;
	public static OutOfCombatView Inst{
		get{
			if(_inst==null){
				_inst = FindObjectOfType<OutOfCombatView>();
				if(_inst == null){
					GameObject OutOfCombatViewGameObject = new GameObject("OutOfCombatView");
					_inst = OutOfCombatViewGameObject.AddComponent<OutOfCombatView>();
					//DontDestroyOnLoad(_inst);
				}
			}
			return _inst;
		}
	}
	ParticleSystem.Particle[] particles;
	LevelMap lastMap;
	bool isDirty = true;
	LevelMap map{
		get{
			//if(lastRoom!=Room.ActiveRoom){
			LevelMap m = null;

			if(Save.LoadedSave != null
				&& Save.LoadedSave.ActiveCharacter != null
				&& Save.LoadedSave.ActiveCharacter.Entity.Location.Map != null
			   ){
				m = Save.LoadedSave.ActiveCharacter.Entity.Location.Map;
			}
			//	lastRoom = Room.ActiveRoom;
      			//}

			if(m!=lastMap){
				isDirty = true;
			}
			lastMap = m;
			return m;
		}
	}
	/*
	int xSize{
		get{
			return dungeon.xSize;
		}
	}
	int ySize{
		get{
			return dungeon.ySize;
		}
	}
*/
	void Awake(){
		if(_inst==null) _inst = this;
		/*for(int i=0;i<10;i++){
			string binString = System.Convert.ToString(i, 2);
			Debug.Log(binString);
		}*/

	}
	// Use this for initialization
	void Start () {
	}
	/*void OnGUI(){
		Vector2 ScreenCenter = new Vector2(Screen.width/2f,Screen.height/2f);
		int size = 10;
		if(dungeon!=null){
			foreach(IntCoord coord in dungeon.Keys){
				Tile tile = dungeon[coord];
				GUI.Box(new Rect(ScreenCenter.x+coord.x*size,Screen.height - (ScreenCenter.y+coord.y*size),size,size),new GUIContent("",coord.ToString()));

			}
		}
		GUI.Box(new Rect(0,0,300,300),GUI.tooltip);
	}*/


	// Update is called once per frame
	void Update () {
		if(_inst != this){
			Destroy(this);
			return;
		}
		if(map!=null){
			if(map!=lastMap||isDirty){
				particles = new ParticleSystem.Particle[map.Count];
				int i = 0;
				foreach(IntCoord coord in map.Keys){
					particles[i]= new ParticleSystem.Particle();
					particles[i].size = 1;
					particles[i].position = new Vector3(coord.x,coord.y,0f);
					i++;
				}
				lastMap = map;
			}
			
			if(!isDirty) return;
			
			var chars = new Dictionary<IntCoord, Color>();
			Player p;
			/*for(int i=1;i<=Save.loadedSave.OnLifeNumber;i++){
				p = Save.loadedSave.Characters[i];
				if(p==null)continue;
				if(p.Location.map == map){
					//Debug.Log(c.location.coord.x+","+c.location.coord.y);
					if(i!=Save.loadedSave.OnLifeNumber){
						chars[p.Location.coord] = new Color(.75f,.3f,.75f);
					}else{
						chars[p.Location.coord] = Color.magenta;
					}
				}
			}*/
			//bool drawCharacter = (character.location.room == this);
			{
				int i =0;
				foreach(IntCoord coord in map.Keys){
					/*
					//IEntityInRoom o;
					//if(drawCharacter && character.location.coord.Equals(coord)){
					//	particles[i].color = Color.magenta;//(Color)o.Color;
					if(chars.ContainsKey(coord)){
						particles[i].color = chars[coord];
						//}else if(objectsInRoom.TryGetValue(coord,out o) && o.Color!=null){
						//	particles[i].color = (Color)o.Color;
					}else{
						particles[i].color = (Color)map[coord].Color;
					}*/
					particles[i].lifetime=100;
					i++;
				}
			}
			particleSystem.SetParticles(particles,map.Count);
		}
	}
}
