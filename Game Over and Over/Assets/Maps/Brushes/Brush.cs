﻿using UnityEngine;
using System.Collections;
using FullInspector;

public abstract class Brush<T>: BaseScriptableObject{
	public abstract T Apply(T o);



}

public abstract class Overwrite<T> : Brush<T> {
	public T overwriteWith;
	public override T Apply(T o){
		return overwriteWith;
	}
}



