using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

public interface ICombatPassive {
	string Name{get;}
	string Description{get;}
//	void Apply()
//	void Remove()
}

public interface ICombatAbility {
	string Name{get;}
	string Description{get;}
	IFunc<OnUseArgs,IEnumerator> OnUse{get;}
	IFunc<OnUseArgs,bool> CanBeUsedNow{get;}
}

public class BasicCombatAbility:ICombatAbility{
	static readonly RawFunc<OnUseArgs,bool> CanBeUsedNowDefault = new RawFunc<OnUseArgs, bool>(delegate(OnUseArgs EventArgs){return true;});


	public string name;
	public string description;
	public string Name{get{return name;}}
	public string Description{get{return description;}}
	[SerializeField]
	private IFunc<OnUseArgs,IEnumerator> _OnUse;
	public IFunc<OnUseArgs,IEnumerator> OnUse{
		get{
			return _OnUse;
		}
	}
	public IFunc<OnUseArgs,bool> CanBeUsedNowOverride = null;
	public IFunc<OnUseArgs,bool> CanBeUsedNow{
		get{
			if(CanBeUsedNowOverride==null) return CanBeUsedNowDefault;
			else return CanBeUsedNowOverride;
		}
	}
}
public class DoSequence:IFunc<OnUseArgs,IEnumerator>{
	public List<IFunc<OnUseArgs,IEnumerator>> sequence = new List<IFunc<OnUseArgs, IEnumerator>>();
	public IEnumerator Invoke(OnUseArgs args){
		foreach(IFunc<OnUseArgs,IEnumerator> action in sequence){
			if(!action.IsSafeToInvoke) continue;
			IEnumerator ie = action.Invoke(args);
			while(ie.MoveNext()){
				yield return ie.Current;
			}
		}
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}

/*public abstract class TargetRequirer : IFunc<IEnumerable<Instance<Combatant>>,OnUseArgs,IEnumerator> {
	public GameState.Combat.Targeting.TargetCriteria criteria = new GameState.Combat.Targeting.TargetCriteria();
	public bool requiresUserSelection;
	

}*/
public class GetTargets{
	public GameState.Combat.Targeting.TargetCriteria criteria;

}
public class WarmupAndCooldown:IFunc<OnUseArgs,IEnumerator>{
	public decimal warmup;
	public decimal cooldown;
	public IEnumerator Invoke(OnUseArgs args){
		args.OnConfirmation.Add(delegate() {
			args.combatant.Entity.WarmUp = warmup; 
			args.combatant.Entity.DoWhenWarmupComplete.Add(delegate(){args.combatant.Entity.CoolDown = cooldown;});
		});
		yield break;
	}
	
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}




public class AddToHeat:IFunc<OnUseArgs,IEnumerator>{
	public int amount;
	public IEnumerator Invoke(OnUseArgs args){
		Save.LoadedSave.ActiveCharacter.Entity.heat += amount;
		yield break;
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}
public class SetHeat:IFunc<OnUseArgs,IEnumerator>{
	public int amount;
	public IEnumerator Invoke(OnUseArgs args){
		Save.LoadedSave.ActiveCharacter.Entity.heat = amount;
		yield break;
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}
/*public class AttackPlayer:IFunc<OnUseArgs,IEnumerator>{
	public IEnumerator Invoke(OnUseArgs args){
		yield break;
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}
*/
public class Targeted:IFunc<OnUseArgs,IEnumerator>{
	public List<IFunc<OnUseArgs,IEnumerable<Instance<Combatant>>,IEnumerator>> sequence = new List<IFunc<OnUseArgs,IEnumerable<Instance<Combatant>>,IEnumerator>>();
	public GameState.Combat.Targeting.TargetCriteria criteria;
	public IEnumerator Invoke(OnUseArgs args){
		IEnumerable<Instance<Combatant>> targets = null;
		bool targetsConfirmed = false;
		bool canceled = false;
		Action<IEnumerable<Instance<Combatant>>> confirm = 
			delegate(IEnumerable<Instance<Combatant>> confirmedTargets){
			targets = confirmedTargets;
			targetsConfirmed = true;
		};
		Action abort = delegate(){
			canceled = true;
		};
		IEnumerator getTargets = criteria.GetTargets(args.combatant,confirm,abort);
		while(getTargets.MoveNext()){
			yield return getTargets.Current;
		}
		
		while(!targetsConfirmed && !canceled){
			Debug.LogError("");
			yield return null;
		}
		if(canceled) yield break;
		else foreach(IFunc<OnUseArgs,IEnumerable<Instance<Combatant>>,IEnumerator> action in sequence){
			if(!action.IsSafeToInvoke) continue;
			IEnumerator ie = action.Invoke(args,targets);
			while(ie.MoveNext()){
				yield return ie.Current;
			}
		}
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}
public abstract class AttackTargets:IFunc<OnUseArgs,IEnumerable<Instance<Combatant>>,IEnumerator>{
	public abstract AttackDamage Damage{
		get;
	}
	public List<IFunc<OnUseArgs,IEnumerator>> sequence = new List<IFunc<OnUseArgs, IEnumerator>>();
	public IEnumerator Invoke(OnUseArgs args, IEnumerable<Instance<Combatant>> targets){

		args.combatant.Entity.DoWhenWarmupComplete.Add(
			delegate() {
				Save.LoadedSave.Add(Save.LoadedSave.CurrentT,TurnStage.Turn,new GameEvent.Attack(targets,args.combatant,Damage,true));
			}

		);
		yield break;
	}
	public bool IsSafeToInvoke{
		get{
			return true;
		}
	}
}