using UnityEngine;


public class Effect:ScriptableObject{

	public string description;
	public bool stacking;
	
	public ScriptedAction<OnApplyArgs> OnApply;
	public class OnApplyArgs{
		int instanceId;
	}
	
	public ScriptedAction<OnRemoveArgs> OnRemove;
	public class OnRemoveArgs{
		int instanceId;
	}
}