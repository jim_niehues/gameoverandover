using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;


public enum EquipmentSlot{IceGauntlet, FireGauntlet, PhysArmor, ElecCastingRifle, BioPotionBelt, AlBackpack, ArcRunesHead}

public struct OnEquipArgs{
	public readonly Equipment Equipment;
	public readonly Combatant CombatantInstance;
	public OnEquipArgs(Equipment Equipment,Combatant CombatantInstance){
		this.Equipment = Equipment;
		this.CombatantInstance = CombatantInstance;
	}
}
public struct OnUnequipArgs{
	public readonly Equipment Equipment;
	public readonly Combatant CombatantInstance;
	public OnUnequipArgs(Equipment Equipment,Combatant CombatantInstance){
		this.Equipment = Equipment;
		this.CombatantInstance = CombatantInstance;
	}
}
public class Equipment : BaseScriptableObject {
	
	public Texture2D inventoryIcon;
	public string toolTip;

	public readonly EquipmentSlot slot;
	public PseudoEvent OnEquip = new PseudoEvent();
	public PseudoEvent OnUnequip = new PseudoEvent();


	/*
	private bool? isPassive = null;
	bool IsPassive{
		get{
			if(!isPassive.HasValue){
				isPassive = true;
				foreach(Effect e in effect.GetOrderedEffectsToApply()){
					if(e is Attack_Effect){
						isPassive = false;
						break;
					}
				}
			}
			isPassive.Value();
		}
	}
	*/


	/*
	public static Equipment FireballTrigger = new Equipment.Active()
	{
		slot =  EquipmentSlot.FireGauntlet,
		effects = new Effect[]
		{
			new AttackEffect.OnAttackWithThisEffect.BasicAttack()
			{
				attackDamge = new AttackDamage(ElementType.Fire,200-Battle.Heat),
			},
			new AttackEffect.OnUseOfThisActiveEffect.AddSubtractHeat(10),
		},
		cooldown = 10,
	};
	public static Equipment IceLance = new Equipment.Active()
	{
		slot =  EquipmentSlot.IceGauntlet,
		effects = new Effect[]
		{
			new AttackEffect.OnAttackWithThisEffect.BasicAttack()
			{
				attackDamge = new AttackDamage(ElementType.Ice,200+Battle.Heat),
			},
			new AttackEffect.OnUseOfThisActiveEffect.AddSubtractHeat(-10),
		},
		warmup = 10,
	};
	public static Equipment Wrench = new Equipment.Active()
	{
		slot =  EquipmentSlot.PhysArmor,
		effects = new Effect[]
		{
			new AttackEffect.OnAttackWithThisEffect.BasicAttack()
			{
				attackDamge = new AttackDamage(ElementType.Phys,100),
			},
		},
		cooldown = 5,
	};
	*/
	/*public static Equipment LightningTrap = new Equipment.Active()
	{
		slot =  EquipmentSlot.ElecCastingRifle,
		effects = new Effect[]
		{
			new AttackEffect.OnAttackWithThisEffect.BasicAttack()
			{
				attackDamge = new AttackDamage(ElementType.Elec,100),
			},
		},
		cooldown = 5,
	};*/
	/*public static Equipment RegenerativeElixir = new Equipment.Active()
	{
		slot =  EquipmentSlot.BioPotionBelt,
		effects = new Effect[]
		{
			new Effect.BasicAttack()
			{
				attackDamge = new AttackDamage(ElementType.Elec,100),
			},
		},
		cooldown = 5,
	};*/



	public class Active: Equipment{
		public decimal warmup;
		public decimal cooldown;
		public IFunc<bool> CanUseNow;
		public PseudoEvent Use = new PseudoEvent();
	}
}

