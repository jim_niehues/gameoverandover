using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public interface IPseudoEventArg{
	void SetPseudoEvent(IPseudoEvent pseudoEvent);
}
public struct PseudoEventArg<T>:IPseudoEventArg{
	public readonly T args;
	private IPseudoEvent _PseudoEvent;

	public PseudoEventArg(PseudoEvent PseudoEvent,T args){
		this.args = args;
		this._PseudoEvent = PseudoEvent;
	}

	public IPseudoEvent PseudoEvent{get{return _PseudoEvent;}}
	void IPseudoEventArg.SetPseudoEvent(IPseudoEvent pseudoEvent){
		if(_PseudoEvent == null) _PseudoEvent = pseudoEvent;
		else throw new System.Exception("PseudoEvent cannot be set more than once");
	}
}

public struct OnStayArgs{
	public readonly Location thisLocation;
	public readonly ILocated movable;
	public OnStayArgs( Location thisLocation,ILocated movable){
		this.thisLocation = thisLocation;
		this.movable = movable;
	}
}
public struct OnEnterArgs{
	public readonly Location thisLocation;
	public readonly Instance<ILocated> movableInstance;
	public readonly Location fromLocation;
	public OnEnterArgs( Location thisLocation, Instance<ILocated> movableInstance, Location fromLocation){
		this.thisLocation = thisLocation;
		this.movableInstance = movableInstance;
		this.fromLocation = fromLocation;
	}
}
public struct OnExitArgs{
	public readonly Location thisLocation;
	public readonly Instance<ILocated> movableInstance;
	public readonly Location toLocation;
	public OnExitArgs( Location thisLocation, Instance<ILocated> movableInstance, Location toLocation){
		this.thisLocation = thisLocation;
		this.movableInstance = movableInstance;
		this.toLocation = toLocation;
	}
}
public struct IsPassableArgs{
	public readonly Location thisLocation;
	public readonly Instance<ILocated> movableInstance;
	public readonly Location fromLocation;
	public IsPassableArgs(Location thisLocation, Instance<ILocated> movableInstance, Location fromLocation){
		this.thisLocation = thisLocation;
		this.movableInstance = movableInstance;
		this.fromLocation = fromLocation;
	}
}
public struct OnSpawnArgs{
	public readonly object spawned;
	public OnSpawnArgs(object spawned){
		this.spawned = spawned;
	}
}

public struct OnSpawnArgs<T>{
	public readonly T spawned;
	public OnSpawnArgs(T spawned){
		this.spawned = spawned;
	}
	public static implicit operator OnSpawnArgs(OnSpawnArgs<T> other)
	{
		return new OnSpawnArgs<T>(other.spawned);
	}
}
public struct OnDeathArgs{
	public readonly Instance<Combatant> combatant;
	public readonly Instance<Combatant> attacker;
	
	public OnDeathArgs(Instance<Combatant> combatant,Instance<Combatant> attacker){
		this.combatant = combatant;
		this.attacker = attacker;
	}
}
public class OnAttackedArgs{
	public readonly Instance<Combatant> combatant;
	public readonly Instance<Combatant> attacker;
	public AttackDamage attackDamage;
	public OnAttackedArgs(Instance<Combatant> combatant,Instance<Combatant> attacker, AttackDamage attackDamage){
		this.combatant = combatant;
		this.attacker = attacker;
		this.attackDamage = attackDamage;
	}
}
public struct OnDamageTakenArgs{
	public readonly Instance<Combatant> combatant;
	public readonly Instance<Combatant> attacker;
	public readonly AttackDamage attackDamage;

	public OnDamageTakenArgs(Instance<Combatant> combatant,Instance<Combatant> attacker,AttackDamage attackDamage){
		this.combatant = combatant;
		this.attacker = attacker;
		this.attackDamage = attackDamage;
	}
}

public struct OnTimeStepArgs{
	public readonly decimal time;
	public readonly Instance<Combatant> combatant;

	public OnTimeStepArgs(decimal timeStep, Instance<Combatant> combatant){
		this.time = timeStep;
		this.combatant = combatant;
	}
}
public struct OnTurnArgs{
	public readonly Instance<Combatant> combatant;
	public readonly decimal time;
	public readonly int turnNumber;
	public readonly int seed;
	public OnTurnArgs(decimal time,int turnNumber,  Instance<Combatant> combatant, int seed){
		this.time = time;
		this.turnNumber = turnNumber;
		this.combatant = combatant;
		this.seed = seed;
	}
	public override bool Equals(System.Object obj) 
	{
		return obj is OnTurnArgs && this == (OnTurnArgs)obj;
	}
	public override int GetHashCode() 
	{
		return unchecked(time.GetHashCode() + turnNumber.GetHashCode()+combatant.GetHashCode()+seed.GetHashCode());
	}
	public static bool operator ==(OnTurnArgs x, OnTurnArgs y) 
	{
		return x.time == y.time && x.turnNumber == y.turnNumber && x.combatant == y.combatant && x.seed == y.seed;
	}
	public static bool operator !=(OnTurnArgs x, OnTurnArgs y) 
	{
		return !(x == y);
	}
}


public struct OnUseArgs{
	public static Dictionary<OnUseArgs,List<Action>> OnConfirmationSyncronizer = new Dictionary<OnUseArgs, List<Action>>();
	public readonly OnTurnArgs onTurnArgs;

	public Instance<Combatant> combatant{
		get{return onTurnArgs.combatant;}
	}
	public decimal time{
		get{return onTurnArgs.time;}
	}
	public int turnNumber{
		get{return onTurnArgs.turnNumber;}
	}
	public int seed{
		get{return onTurnArgs.seed;}
	}
	public List<Action> OnConfirmation{
		get{
			List<Action> onConfirmation = null;
			if(!OnConfirmationSyncronizer.TryGetValue(this,out onConfirmation)){
				onConfirmation = new List<Action>();
				OnConfirmationSyncronizer.Add(this,onConfirmation);
			}
			return onConfirmation;
		}
	}
	public void Confirm(){
		List<Action> onConfirmation = null;
		if(OnConfirmationSyncronizer.TryGetValue(this,out onConfirmation)){
			foreach(Action action in onConfirmation){
				if(action != null){
					action();
				}
			}
			OnConfirmationSyncronizer.Remove(this);
		}

	}
	public OnUseArgs(OnTurnArgs onTurnArgs){
		this.onTurnArgs = onTurnArgs;
	}
	public override bool Equals(System.Object obj) 
	{
		return obj is OnUseArgs && this == (OnUseArgs)obj;
	}
	public override int GetHashCode() 
	{
		return onTurnArgs.GetHashCode();
	}
	public static bool operator ==(OnUseArgs x, OnUseArgs y) 
	{
		return x.onTurnArgs == y.onTurnArgs;
	}
	public static bool operator !=(OnUseArgs x, OnUseArgs y) 
	{
		return !(x == y);
	}
}