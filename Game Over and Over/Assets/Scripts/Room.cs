//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//
//
//
//[System.Serializable]
//public class RoomDesign : IDictionary<IntCoord, Tile>{
//
//
//	private Dictionary<IntCoord, Tile> tiles = new Dictionary<IntCoord, Tile>();
//	protected List<IntCoord> doors = new List<IntCoord>();
//
//	public IntCoord[] Doors{
//		get{
//			return doors.ToArray();
//		}
//	}
//
//
//
//
//
//	public int Count{
//		get{
//			return tiles.Count;
//		}
//	}
//	
//	public bool IsReadOnly { 
//		get{
//			//return tiles.IsReadOnly;
//
//			#if UNITY_EDITOR
//			return false;
//			#else	
//			return true;
//			#endif
//		}
//	}
//	
//	public Tile this[IntCoord key] { 
//		get{
//			return tiles[key];
//		} 
//		set{
//
//			if(value.IsDoor())doors.Add(key);
//			else doors.Remove(key);
//			if(value == null) tiles.Remove(key);
//			else tiles[key] = value;
//		} 
//	}
//	
//	public ICollection<IntCoord> Keys { 
//		get{
//			return tiles.Keys;
//		} 
//	}
//	
//	public ICollection<Tile> Values { 
//		get{
//			return tiles.Values;
//		}
//	}
//	
//	void ICollection<System.Collections.Generic.KeyValuePair<IntCoord,Tile>>.Add(KeyValuePair<IntCoord,Tile> item){
//		throw new System.NotImplementedException();
//	}
//	
//	public void Add(IntCoord key,Tile value){
//		if(value is Door) doors.Add(key);
//		tiles.Add(key, value);
//	}
//	
//	public void Clear(){
//		tiles.Clear();
//	}
//	
//	bool ICollection<System.Collections.Generic.KeyValuePair<IntCoord,Tile>>.Contains(KeyValuePair<IntCoord,Tile> item){
//		throw new System.NotImplementedException();
//	}
//	public bool ContainsKey(IntCoord key){
//		return tiles.ContainsKey(key);
//	}
//	
//	public void CopyTo(KeyValuePair<IntCoord,Tile>[] array,int arrayIndex){
//		
//	}
//	
//	bool ICollection<System.Collections.Generic.KeyValuePair<IntCoord,Tile>>.Remove(KeyValuePair<IntCoord,Tile> item){
//		throw new System.NotImplementedException();
//	}
//	
//	public bool Remove(IntCoord key){
//		if(tiles[key] is Door) doors.Remove(key);
//		return tiles.Remove(key);
//	}
//	
//	public bool TryGetValue(IntCoord key, out Tile value){
//		return tiles.TryGetValue(key,out value);
//	}
//	public IEnumerator<KeyValuePair<IntCoord,Tile>> GetEnumerator(){
//		return tiles.GetEnumerator();
//	}
//	IEnumerator System.Collections.IEnumerable.GetEnumerator(){
//		return GetEnumerator();
//	}
//}
//class UltraSimpleRoomDesign : RoomDesign{
//
//	static UltraSimpleRoomDesign(){
//		/*int totalDoors = 4;
//		List<BitArray>[] combos = new List<BitArray>[totalDoors-1];
//		for(int i = 1; i<(int)Mathf.Pow(2,totalDoors);i++){
//			int ii = 0;
//			foreach(bool bit in (BitArray)i){
//				ii++;
//			}
//			BitArray fullLengthBitArray = new BitArray(totalDoors);
//			for(int iii = 0; iii <fullLengthBitArray.Count;iii++){
//				fullLengthBitArray[iii] = ii[iii];
//			}
//			combos[ii-1].Add(fullLengthBitArray);
//		}
//
//		doorCombos[ii] = combos.ToArray();
//	*/
//	}
//
//	public static Dictionary<int,List<RoomDesign>> Generator(int size, System.Random rand){
//		Dictionary<int,List<RoomDesign>> roomDesigns = new Dictionary<int, List<RoomDesign>>();
//
//		//bool preformReverse = System.Convert.ToString(1, 2)[0] == System.Convert.ToString(2, 2)[0];
//		for(int xMult=1;xMult<=2;xMult++){
//			for(int yMult=1;yMult<=1;yMult++){
//				
//				int maxDoors = xMult*2+yMult*2;
//				int maxDoorPatternInt = (int)Mathf.Pow(2,maxDoors);
//				HashSet<int> intsUsed = new HashSet<int>();
//				for(int i=0;i<System.Math.Min(20,maxDoorPatternInt-1);i++){
//					int doorPatternInt = rand.Next(1,maxDoorPatternInt);
//					if(intsUsed.Contains(doorPatternInt)){
//						if(1>=maxDoorPatternInt) throw new System.Exception();
//						int altNumber = doorPatternInt;
//						int direction = 0;
//						while(direction == 0){
//							altNumber = rand.Next(1,maxDoorPatternInt);
//							direction = doorPatternInt.CompareTo(altNumber);
//						}
//						
//						if(intsUsed.Contains(altNumber)){
//							altNumber = doorPatternInt;
//							altNumber+=direction;
//							while(altNumber>0 && altNumber<maxDoorPatternInt && intsUsed.Contains(altNumber)){
//								altNumber+=direction;
//								//try to find an open spot moving one way
//							}
//							if(!(altNumber>0 && altNumber<maxDoorPatternInt)){
//								altNumber = doorPatternInt;
//								altNumber-=direction;
//								while(altNumber>0 && altNumber<maxDoorPatternInt && intsUsed.Contains(altNumber)){
//									altNumber-=direction;
//									//try to find an open spot moving the other way
//								}
//								if(!(altNumber>0 && altNumber<maxDoorPatternInt)){
//									//Debug.Log("no open spots")
//									break;
//									//no open spots
//								}
//							}
//						}
//						doorPatternInt =altNumber;
//					}
//					intsUsed.Add(doorPatternInt);
//				//for(int doorPatternInt = 1; doorPatternInt< maxDoorPatternInt; doorPatternInt++){
//					//bool[] doorPattern = new bool[maxDoors];
//					int numerOfDoors = 0;
//					//var sb = new System.Text.StringBuilder();
//					var doorPattern = new BitArray(new int[]{doorPatternInt});
//					foreach (bool bit in doorPattern)
//					{
//						//sb.Append(bit ? 1 : 0);
//						if(bit) numerOfDoors++;
//					}
//						//Debug.Log(sb.ToString());
//					//int numerOfDoors = 0;
//					//bool[] doorPattern = new BitArray(new int[]{doorPatternInt}).Cast<bool>();
//
//
//
//					//bool[] doorPattern = new BitArray(System.Convert.ToString(doorPatternInt, 2).PadLeft(xMult*2+yMult*2,'0').Reverse().Select(c => c=='1').ToArray();
//					//int numerOfDoors = doorPattern.Count(b => b==true);
//					if(numerOfDoors<1) continue;
//					if(!roomDesigns.ContainsKey(numerOfDoors)) roomDesigns[numerOfDoors] = new List<RoomDesign>();
//					//if(preformReverse)doorPattern
//					roomDesigns[numerOfDoors].Add( new UltraSimpleRoomDesign(xMult, yMult, size, doorPattern));
//				
//					
//				}
//
//			}
//		}
//		return roomDesigns;
//	}
//	public UltraSimpleRoomDesign( int xMult, int yMult,int nominalSize, BitArray doors){
//		int doorIndex = 0;
//		for(int y=0;y<nominalSize*yMult;y++){
//			for(int x=0;x<nominalSize*xMult;x++){
//				if( (y==0 || y==nominalSize*yMult-1) ||(x==0 || x==nominalSize*xMult-1 )){
//					if( x%nominalSize == nominalSize/2 || y%nominalSize == nominalSize/2){
//						if(doors[doorIndex]){
//							Tile.Door newDoor = new Tile.Door();
//
//							if(y == nominalSize*yMult) newDoor = new Tile.Door(Rotation._0);
//							else if(x == nominalSize*xMult) newDoor = new Tile.Door(Rotation._90);
//							else if(y == 0) newDoor = new Tile.Door(Rotation._180);
//							else if(x == 0) newDoor = new Tile.Door(Rotation._270);
//							else newDoor = new Tile.Door();
//							this[new IntCoord(x,y)] = newDoor;
//							doorIndex++;
//							continue;
//						}
//						doorIndex++;
//					}
//					this[new IntCoord(x,y)] = new Tile.Wall();
//				}else{
//					this[new IntCoord(x,y)] = new Tile.Floor();
//				}
//			}
//		}
//	}
//}
//
//
//[System.Serializable]
//public class Room {
//	public RoomDesign roomDesign;
//
//	//var doorMetaData = new Dictionary<Door,object>();
//	Door[] doors;
//
//	//public Transformation transformation;
//	/*
//	class ExitDoor{
//		Intcoord door;
//		Room[] RoomsDependantOnToUnlock;
//
//	}
//	class :DoorInstance{
//		Room[] RoomsDependantOnToUnlock;
//			
//	}
//
//	class RoomAccessPoint{
//		Room[] RoomsDependantOnToUnlock
//		
//	}
//	*/
//	class Grid{
//
//
//
//
//		class Transformation{
//			Rotation rotation;
//			IntCoord center;
//
//
//			/// <summary>
//			/// Applies the transformation. (normally toward local space)
//			/// </summary>
//			IntCoord ApplyTransformation(IntCoord coord){
//				return rotation.Rotate(coord-center);
//			}
//			/// <summary>
//			/// Applies the inverse transformation. (normally toward global space)
//			/// </summary>
//			IntCoord ApplyInverseTransformation(IntCoord coord){
//				return rotation.RotateInverse(coord)+center;
//			}
//			
//			IntCoord RotateAround(IntCoord coord, Rotation rotation, IntCoord around){
//				return rotation.Rotate(coord-around)+around;
//			}
//		}
//	}
//
//	
//	public Room ()
//	{
//	}
///*
//	PlacedTile<Tile> PlaceTile(Tile tile){
//
//
//	}
//*/
//
//}
