using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
public class CombatView : MonoBehaviour {
	PlayerInstance ActiveCharacter{
		get{return Save.loadedSave.ActiveCharacter;}
	}
	private static CombatView _inst;
	public static CombatView Inst{
		get{
			if(_inst==null){
				_inst = FindObjectOfType<CombatView>();
				if(_inst == null){
					GameObject CombatViewGameObject = new GameObject("CombatView");
					_inst = CombatViewGameObject.AddComponent<CombatView>();
				}
				//DontDestroyOnLoad(_inst);
			}
			return _inst;
		}
	}
	public HashSet<CombatantInstance> Enemies{
		get{
			return ActiveCharacter.Foes;
		}
	}
	public HashSet<CombatantInstance> Allies{
		get{
			return ActiveCharacter.Allies;
		}
	}
	EnemyInstance _targetOfActiveCharacter;
	EnemyInstance TargetOfActiveCharacter{
		get{
			if(_targetOfActiveCharacter==null || _targetOfActiveCharacter.HP<=0) return null;
			foreach(EnemyInstance e in Enemies){
				if(e == _targetOfActiveCharacter) return e;
			}
			_targetOfActiveCharacter=null;
			return null;
		}
		set{
			if(value.HP<=0) return;
			_targetOfActiveCharacter = value;
		}
	}
	bool AllEnemiesAreDead{
		get{
			foreach(EnemyInstance e in Enemies){
				if(e.HP>0) return false;
			}
			return true;
		}
	}
	
	void Awake(){
		if(_inst==null) _inst = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_inst != this){
			Destroy(this);
			return;
		}
		
	}
	void OnGUI(){
		if(ActiveCharacter==null)return;
		int i;
		i =-1;
		foreach(CombatantInstance combatant in Allies){

			i++;
			GUI.enabled = !combatant.IsDead;
			if(GUI.Button(new Rect(75*(i%5),Screen.height-(50*(1+Mathf.FloorToInt(((float)i)/5f))),75,50),new GUIContent(combatant.Name +"\nHP "+combatant.HP+"\n"+combatant.GetType().ToString()) )){

			}
			GUI.enabled = true;
		}
		i =-1;
		foreach(EnemyInstance enemy in Enemies){
			i++;
			GUI.enabled = !enemy.IsDead;
			if(GUI.Button(new Rect(Screen.width -100,50*i,100,50),"")){
				TargetOfActiveCharacter = enemy;
			}
			if(enemy == TargetOfActiveCharacter) 
				GUI.Box(new Rect(Screen.width -100,50*i,100,50),enemy.Name +"\nHP: "+enemy.HP);
			GUI.Box(new Rect(Screen.width -100,50*i,100,50),enemy.Name +"\nHP: "+enemy.HP);
			GUI.enabled = true;
		}
		bool targetChosen = (TargetOfActiveCharacter!=null);
		GUI.enabled = targetChosen && Save.loadedSave.waitingForInput;
		if(GUI.Button(new Rect(Screen.width-50,Screen.height-50,50,50),"Attack")){
			if(TargetOfActiveCharacter!=null){
				//attack targeted enemy
				Save ls = Save.loadedSave;
				ls.Add(ls.CurrentT, new GameEvent.AttackEnemy(TargetOfActiveCharacter,new AttackDamage(ElementType.Phys,10),true));
				ls.NextPlayerTurn += 1;
				//TargetOfActiveCharacter.HP--;

			}
		}
		GUI.enabled = true;
	}
	public void OnDisable(){
		_targetOfActiveCharacter = null;
	}
}
*/