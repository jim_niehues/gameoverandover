using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnGUI(){
		if(Save.LoadedSave!=null){
			if(Save.LoadedSave.ActiveCharacter.Exists){
				Player activeCharacter = Save.LoadedSave.ActiveCharacter.Entity;
				GUILayout.BeginVertical("box");
				{
					GUILayout.Label("Turn : " + Save.LoadedSave.CurrentT );
					GUILayout.Label("HP : " + activeCharacter.HP);// + "/" + activeCharacter.baseClass.MaxHP);
					GUILayout.Label("EXP : " + Save.LoadedSave.EXP );
				}
				GUILayout.EndVertical();

			}
		}
	}
}
