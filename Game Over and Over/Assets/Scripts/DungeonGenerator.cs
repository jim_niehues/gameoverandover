////http://xdavidleon.tumblr.com/post/51104205836/roguelike-development-in-unity-part-i
//
//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//
//public class Dungeon_xdavidleon : Dungeon {
//
//	
//	// Room Parameters
//	public int ROOM_MAX_SIZE = 24;
//	public int ROOM_MIN_SIZE = 4;
//	public int ROOM_WALL_BORDER = 1;
//	public bool ROOM_UGLY_ENABLED = false;
//	public float ROOM_MAX_RATIO = 5.0f;
//	
//	// Generation Parameters
//	public int MAX_DEPTH = 10;
//	public int CHANCE_STOP = 50;
//	public int SLICE_TRIES = 10;
//	public int CORRIDOR_WIDTH = 1;
//
//
//
//	
//
//}
//class RoomDesignCollection{
//	List<RoomDesign> startingRooms = new List<RoomDesign>();
//	Dictionary<int,List<RoomDesign>> rooms = new Dictionary<int, List<RoomDesign>>();
//	void Add(RoomDesign room){
//		int doorCount = 0;
//		foreach(Tile tile in room.Values){
//			if(tile.IsEntranceDoor()||tile.IsExitDoor()){
//				doorCount++;
//			}
//		}
//		if(rooms[doorCount] == null) rooms[doorCount] = new List<RoomDesign>();
//		rooms[doorCount].Add(room);
//	}
//}
//public interface IDungeonGenerator{
//	Dungeon GenerateDungeon(int numberOfRooms,int seed);
//	RoomDesign[] StartingRooms{get;}
//	Dictionary<int,List<RoomDesign>> rooms{get;}
//
//}
//public class DungeonGenerator_Alpha: IDungeonGenerator{
//	static int roomSize = 5;
//	public RoomDesign[] StartingRooms{
//		get{
//			return _rooms[1].ToArray();
//		}
//	}
//	Dictionary<int,List<RoomDesign>> _rooms = UltraSimpleRoomDesign.Generator(5,new System.Random(1));
//
//	public Dictionary<int,List<RoomDesign>> rooms{
//		get{
//			return _rooms;
//		}
//	}
//
//
//
//	public Dungeon GenerateDungeon (int numberOfRooms,int seed){
//		System.Random rand = new System.Random(seed);
//		int roomsAdded = 0;
//		Dictionary<IntCoord,Tile.Door> doors = new Dictionary<IntCoord,Tile.Door>();
//		
//		
//
//		//add a starting Room
//
//		//add doors to door list
//
//		
//		
//		Dungeon dungeon = new Dungeon();
//
//
//
//		{
//			int tries = 0;
//			int index = rand.Next(0,StartingRooms.Length);
//
//			while(!TryToAddToDungeon(ref dungeon, ref doors, new IntCoord(0,0), StartingRooms[index],rand)){
//				index = (index+1)%StartingRooms.Length;
//				tries++;
//				if(tries>StartingRooms.Length){
//					throw new System.Exception("Endless loop when adding first room to dungeon");
//					return dungeon;
//				}
//
//			}
//			roomsAdded++;
//			Debug.Log("Starting Room Generated");
//		}
//		
//
//		while(true){
//			Debug.Log(""+doors.Count+" doors now open to the void");
//			if(doors.Count<=0) break;
////			IntCoord doorToAddTo;
////			{
////				IntCoord[] doorKeys = doors.Keys.ToArray();
////				doorToAddTo = doorKeys[rand.Next(0,doors.Count)];
////			}
//
//			int unusedDoorCount = doors.Count;
//			int doorRand = rand.Next(0,unusedDoorCount);
//			IntCoord existingDoorCoord = doors.Keys.ElementAt((doorRand)%unusedDoorCount);
//			
//			int failCounter = 0;
//			bool roomAdded = false;
//			for(int i=0;i<2;i++){
//
//				RoomSelectionMagicBox roomSelector;
//				if(i==0){
//					roomSelector = new RoomSelectionMagicBox(
//						rooms,
//						roomsAdded,
//						doors.Count,
//						numberOfRooms,
//						rand.Next()
//						);
//				}else{
//					Debug.Log("Resorting to one door rooms");
//					roomSelector = new RoomSelectionMagicBox(
//						rooms[1],
//						rand.Next()
//						);
//				}
//				while(roomSelector.MoveNext()){
//					RoomDesign design = roomSelector.Current;
//					roomAdded = TryToAddToDungeon(
//						ref dungeon, 
//						ref doors, 
//						existingDoorCoord,
//						design, 
//						rand
//						);
//					if(roomAdded){
//						break;
//					}else{
//						failCounter++;
//					}
//				}
//				if(roomAdded){
//					break;
//				}
//				
//				Debug.Log("Failed To fit any room to a door");
//			}
//			if(roomAdded){
//				roomsAdded++;
//				Debug.Log("Room Generated");
//				if(numberOfRooms+10<roomsAdded){
//					Debug.Log("Generating too many rooms("+roomsAdded+", goal was "+numberOfRooms+")... Aborting");
//					break;
//				}
//			}else{
//				doors.Remove(existingDoorCoord);
//			}
//
//		}
//		//dungeon.GenerateGameObjects();
//		return dungeon;
//
////		while(doorlist is not empty
////			
////			pick a random door and attach a room to it 
////			if unable try to swap out room attached to door with one lacking said door
////			remove door from list
////			add doors from newly attached room
////		as number of rooms increase, increase the likelyhood a newly chosen room has fewer doors
////		
////		Door 
//
//	}
//	bool TryToAddToDungeon(ref Dungeon dungeon, ref Dictionary<IntCoord,Tile.Door> doors, IntCoord existingDoorCoordInDungeon, RoomDesign roomDesign,System.Random rand){
//		//Debug.Log("Trying to add room with "+roomDesign.Doors.Length+" Doors");
//
//
//		Tile.Door parentDoor = null;
//		if(doors.ContainsKey(existingDoorCoordInDungeon)){
//			parentDoor = doors[existingDoorCoordInDungeon];
//		}
//
//
//
//		Rotation rotRand = new Rotation()+rand.Next(0,4);
//		for(int r = 0;r<4;r++){
//			rotRand.QuarterTurns++;
//			int doorCount = roomDesign.Doors.Length;
//			int doorRand = rand.Next(0,doorCount);
//			
//			for(int doorIndexOffset = 0;doorIndexOffset<doorCount;doorIndexOffset++){
//				doorRand = (doorRand+1)%doorCount;
//				IntCoord designDoorCoord = roomDesign.Doors[doorRand];
//
//				Tile.Door designDoor = (Tile.Door)roomDesign[designDoorCoord];
//				if(parentDoor!=null){
//					if(rotRand+designDoor.rotation + Rotation._180 != parentDoor.rotation) continue;
//				}
//				var toAdd = new List<KeyValuePair<IntCoord,Tile>>();
//				var doorsToAdd = new List<IntCoord>();
//				var doorsToRemove = new List<IntCoord>();
//				IntCoord coordDoorIsToBeAddedTo = new IntCoord();
//				if(parentDoor != null){
//					coordDoorIsToBeAddedTo = (existingDoorCoordInDungeon+parentDoor.rotation.North);
//				}
//				IntCoord offset = coordDoorIsToBeAddedTo - rotRand.Rotate(designDoorCoord);
//				bool invalid = false;
//				foreach(IntCoord roomCoord in roomDesign.Keys){
//					IntCoord transformedCoord = offset + rotRand.Rotate(roomCoord);
//
//					if(dungeon.ContainsKey(transformedCoord)){
//						invalid = true;
//						break;
//					}
//					Tile tile = roomDesign[roomCoord].Clone();
//					tile.rotation += rotRand;
//
//					if(tile.IsDoor()){
//						IntCoord coordDoorExitsTo = transformedCoord+tile.rotation.North;
//						if(dungeon.ContainsKey(coordDoorExitsTo)){
//							if(dungeon[coordDoorExitsTo].IsDoor()){
//								doorsToRemove.Add(coordDoorExitsTo);
//							}else{
//								invalid = true;
//								break;
//							}
//						}else{
//							doorsToAdd.Add(transformedCoord);
//						}
//					}
//
//					if(invalid)break;
//
//					toAdd.Add(new KeyValuePair<IntCoord, Tile>(transformedCoord,tile));
//				}
//				
//				if(invalid)break;
//				
//				foreach(KeyValuePair<IntCoord,Tile> pair in toAdd){
//					dungeon[pair.Key] = pair.Value;
//				}
//				foreach(IntCoord doorCoord in doorsToAdd){
//
//					doors[doorCoord] = (Tile.Door)dungeon[doorCoord];
//				}
//				foreach(IntCoord doorCoord in doorsToRemove){
//					doors.Remove(doorCoord);
//				}
//				return true;
//			}
//		}
//
//		return false;
//	}
//	class RoomSelectionMagicBox:IEnumerator<RoomDesign>{
//		System.Random rand;
//		int minRooms;
//		int roomGoal;
//		RoomDesign current;
//		int unusedDoors;
//		Dictionary<int,List<RoomDesign>> possibleRooms = new Dictionary<int, List<RoomDesign>>();
//		public RoomSelectionMagicBox(Dictionary<int,List<RoomDesign>> rooms,int roomsPlaced, int unusedDoors, int goalRoomCount,int seed){
//			this.rand = new System.Random(seed);
//			this.unusedDoors = unusedDoors;
//			this.minRooms = roomsPlaced+unusedDoors;
//			roomGoal = goalRoomCount;
//			
//			foreach(int i in rooms.Keys){
//				
//				if
//				(
//					((i-1) + minRooms < roomGoal)
//					&& !(i==1 && (minRooms !=roomGoal)) // don't add any terminating rooms until forced to
//				)
//				{
//					possibleRooms[i] = new List<RoomDesign>();
//					foreach(RoomDesign roomDesign in rooms[i]){
//						possibleRooms[i].AddRange(rooms[i]);
//					}
//				}
//			}
//		}
//		public RoomSelectionMagicBox(List<RoomDesign> rooms,int seed){
//			this.rand = new System.Random(seed);
//			possibleRooms[0] = new List<RoomDesign>(rooms);
//		}
//		public bool MoveNext()
//		{
//			while(possibleRooms.Count>0){
//				int[] doorCounts = possibleRooms.Keys.ToArray();
//				int doors = doorCounts[rand.Next(0,doorCounts.Length)];
//				//List<RoomDesign> designs = possibleRooms[doors];
//				if(possibleRooms[doors] == null || possibleRooms[doors].Count == 0){
//					possibleRooms.Remove(doors);
//					continue;
//				}
//				int designIndex = rand.Next(0,possibleRooms[doors].Count);
//				current = possibleRooms[doors][designIndex];
//				possibleRooms[doors].RemoveAt(designIndex);
//				return true;
//			}
//			return false;
//		}
//		public void Reset() { throw new System.NotImplementedException(); }
//		
//		void System.IDisposable.Dispose() { }
//		
//		public RoomDesign Current
//		{
//			get { return current; }
//		}
//		
//		object IEnumerator.Current
//		{
//			get { return Current; }
//		}
//	}
//}
//
//
////// DungeonGenerator class. Singleton.
////public class DungeonGenerator : MonoSingleton <DungeonGenerator> {
////
////
////
////
////
////
////
////
////
////	// Tilemap
////	public Tile[,] tiles;
////
////	
////	// Player	
////	public GameObject player;
////
////	
////	// List of rooms
////	public List<Room> rooms;
////	
////	// Auxiliar vars
//////	private GameObject floor;
////	private Texture2D dungeonTexture;
////	
////	// On Awake
////	public override void Init()
////	{
////		// Initialize the tilemap
////		tiles = new Tile[MAP_HEIGHT,MAP_WIDTH];
////		for (int i = 0; i < MAP_HEIGHT; i++) 
////			for (int j = 0; j < MAP_WIDTH; j++) 
////				tiles[i,j] = new Tile(Tile.TILE_EMPTY);
////		
////		// Init QuadTree
////		quadTree = new QuadTree(new AABB(new XY(MAP_WIDTH/2.0f,MAP_HEIGHT/2.0f),new XY(MAP_WIDTH/2.0f, MAP_HEIGHT/2.0f)));
////
////		// List of rooms
////		rooms = new List<Room>();
////		
////	}
////	
////	// On Start
////	void Start () 
////	{
////
////	}
////	
////	// Each frame
////	void Update () {
////		// Generate a new Test Dungeon
////		if (Input.GetButtonDown("Jump"))
////		{
////			// Generate a new Seed
////			seed = System.DateTime.Now.Millisecond*1000 + System.DateTime.Now.Minute*100;
////			
////			// Camera on middle and looking down
////			Camera.mainCamera.transform.position = new Vector3(MAP_WIDTH/2,100,MAP_HEIGHT/2);
////			
////			// Set the randome seed
////			Random.seed = seed;
////			
////			// Generate Dungeon
////			Debug.Log ("Dungeon Generation Started");
////			
////			GenerateDungeon(seed);
////		}
////	}
////	
////	// Clean everything
////	public void ResetDungeon()
////	{
////		// Disable player
////		player.SetActive(false);
////		
////		// Reset tilemap
////		for (int i = 0; i < MAP_HEIGHT; i++) 
////			for (int j = 0; j < MAP_WIDTH; j++) 
////				tiles[i,j] = new Tile(Tile.TILE_EMPTY);
////		
////		// Reset QuadTree
////		quadTree = new QuadTree(new AABB(new XY(MAP_WIDTH/2.0f,MAP_HEIGHT/2.0f),new XY(MAP_WIDTH/2.0f, MAP_HEIGHT/2.0f)));
////		
////		// Reset rooms
////		rooms.Clear();
////		
////		// Destroy tile GameObjects
////		foreach (Transform t in containerRooms.transform) GameObject.Destroy(t.gameObject);
////	}
////	
////	// Generate a new dungeon with the given seed
////	public void GenerateDungeon(int seed)
////	{
////		Debug.Log ("Generating QuadTree");
////			
////		// Clean
////		ResetDungeon ();
////		
////		// Place a temporary floor to see progress
//////		floor = GameObject.Instantiate(prefabFloor01,new Vector3(MAP_WIDTH/2,-0.5f,MAP_HEIGHT/2), Quaternion.identity) as GameObject;
//////		floor.transform.localScale = new Vector3(MAP_WIDTH,1,MAP_HEIGHT);
////		
////		// Generate QuadTree
////		GenerateQuadTree (ref quadTree);
////		
////		// Export texture
////		Texture2D quadTreeTexture = quadTree.QuadTreeToTexture();
//////		floor.renderer.material.mainTexture = quadTree.QuadTreeToTexture();
////		TextureToFile(quadTreeTexture,seed + "_quadTree");
////
////		Debug.Log ("Generating Rooms");
////		
////		// Generate Rooms
////		GenerateRooms (ref rooms, quadTree);
////		
////		// Export texture
////		dungeonTexture = DungeonToTexture();
//////		floor.renderer.material.mainTexture = dungeonTexture;
////		TextureToFile(dungeonTexture,seed + "_rooms");
////		
////		Debug.Log ("Generating Corridors");
////		
////		// Generate Corridors
////		GenerateCorridors ();
////		
////		// Export texture
////		dungeonTexture = DungeonToTexture();
//////		floor.renderer.material.mainTexture = dungeonTexture;
////		TextureToFile(dungeonTexture,seed + "_corridors");
////		
////		
////		Debug.Log ("Generating Walls");
////		
////		GenerateWalls();
////		
////		// Export texture
////		dungeonTexture = DungeonToTexture();
//////		floor.renderer.material.mainTexture = dungeonTexture;
////		TextureToFile(dungeonTexture,seed + "_walls");
////		
////		Debug.Log ("Generating GameObjects, this may take a while..");
////		
////		// Instantiate prefabs
////		GenerateGameObjects(quadTree);
////			
////		// Place Player
////		int r = Random.Range(0,rooms.Count-1);
////		Room room = rooms[r];
////		player.SetActive(true);
////		player.transform.position = new Vector3(room.boundary.center.x,1.0f,room.boundary.center.y);
////		
//////		GameObject.DestroyImmediate(floor);
////		
////	}
////	
////	// Generate the quadtree system
////	void GenerateQuadTree(ref QuadTree _quadTree)
////	{
////		_quadTree.GenerateQuadTree(seed);
////	}
////	
////	// Generate the list of rooms and dig them
////	public void GenerateRooms(ref List<Room> _rooms, QuadTree _quadTree)
////	{
////		// Childless node
////		if (_quadTree.northWest == null && _quadTree.northEast == null && _quadTree.southWest == null && _quadTree.southEast == null)
////		{
////			_rooms.Add(GenerateRoom(_quadTree));
////			return;
////		}
////		
////		// Recursive call
////		if (_quadTree.northWest != null) GenerateRooms (ref _rooms,_quadTree.northWest);
////		if (_quadTree.northEast != null) GenerateRooms (ref _rooms,_quadTree.northEast);
////		if (_quadTree.southWest != null) GenerateRooms (ref _rooms,_quadTree.southWest);
////		if (_quadTree.southEast != null) GenerateRooms (ref _rooms,_quadTree.southEast);
////	}
////	
////	// Generate a single room
////	public Room GenerateRoom(QuadTree _quadTree)
////	{
////		// Center of the room
////		XY roomCenter = new XY();
////		roomCenter.x = Random.Range(ROOM_WALL_BORDER + _quadTree.boundary.Left() + ROOM_MIN_SIZE/2.0f, _quadTree.boundary.Right() - ROOM_MIN_SIZE/2.0f - ROOM_WALL_BORDER);
////		roomCenter.y = Random.Range(ROOM_WALL_BORDER + _quadTree.boundary.Bottom() + ROOM_MIN_SIZE/2.0f, _quadTree.boundary.Top() - ROOM_MIN_SIZE/2.0f - ROOM_WALL_BORDER);		
////		
////		// Half size of the room
////		XY roomHalf = new XY();
////		
////		float halfX = (_quadTree.boundary.Right() - roomCenter.x - ROOM_WALL_BORDER);
////		float halfX2 =(roomCenter.x - _quadTree.boundary.Left() - ROOM_WALL_BORDER);
////		if (halfX2 < halfX) halfX = halfX2;
////		if (halfX > ROOM_MAX_SIZE/2.0f) halfX = ROOM_MAX_SIZE/2.0f;
////		
////		float halfY = (_quadTree.boundary.Top() - roomCenter.y - ROOM_WALL_BORDER);
////		float halfY2 =(roomCenter.y - _quadTree.boundary.Bottom() - ROOM_WALL_BORDER);
////		if (halfY2 < halfY) halfY = halfY2;
////		if (halfY > ROOM_MAX_SIZE/2.0f) halfY = ROOM_MAX_SIZE/2.0f;
////		
////		roomHalf.x = Random.Range((float)ROOM_MIN_SIZE/2.0f,halfX);
////		roomHalf.y = Random.Range((float)ROOM_MIN_SIZE/2.0f,halfY);
////
////		// Eliminate ugly zones
////		if (ROOM_UGLY_ENABLED == false) 
////		{
////			float aspect_ratio = roomHalf.x / roomHalf.y;
////			if (aspect_ratio > ROOM_MAX_RATIO || aspect_ratio < 1.0f/ROOM_MAX_RATIO) return GenerateRoom(_quadTree); 
////		}
////		
////		// Create AABB
////		AABB randomAABB = new AABB(roomCenter, roomHalf);
////		
////		// Dig the room in our tilemap
////		DigRoom (randomAABB.BottomTile(), randomAABB.LeftTile(), randomAABB.TopTile()-1, randomAABB.RightTile()-1);
////		
////		// Return the room
////		return new Room(randomAABB,_quadTree);
////	}
////	
////	void GenerateCorridors()
////	{
////		quadTree.GenerateCorridors();
////	}
////
////	// Generate walls when there's something near
////	public void GenerateWalls()
////	{
////		// Place walls
////		for (int i = 0; i < MAP_HEIGHT; i++)
////		{
////			for (int j = 0; j < MAP_WIDTH; j++)
////			{
////				bool room_near = false;
////				if (IsPassable(i,j)) continue;
////				if (i > 0) if (IsPassable(i-1,j)) room_near = true;
////				if (i < MAP_HEIGHT-1) if (IsPassable(i+1,j)) room_near = true;
////				if (j > 0) if (IsPassable(i,j-1)) room_near = true;
////				if (j < MAP_WIDTH-1) if (IsPassable(i,j+1)) room_near = true;
////				if (room_near) SetWall(i,j);
////			}
////		}
////	}
////	
////
////	
////	void PaintDungeonTexture(ref Texture2D t)
////	{
////		for (int i = 0; i < MAP_WIDTH; i++) for (int j = 0; j < MAP_HEIGHT; j++) 
////		{
////			switch (tiles[j,i].id)
////			{
////			case Tile.TILE_EMPTY:
////				t.SetPixel(i,j,Color.black);
////				break;
////			case Tile.TILE_ROOM:
////				t.SetPixel(i,j,Color.white);
////				break;
////			case Tile.TILE_CORRIDOR:
////				t.SetPixel(i,j,Color.grey);
////				break;
////			case Tile.TILE_WALL:
////				t.SetPixel(i,j,Color.blue);
////				break;
////			}
////		}
////
////	}
////	
////	Texture2D DungeonToTexture()
////	{
////		Texture2D texOutput = new Texture2D((int) (MAP_WIDTH), (int) (MAP_HEIGHT),TextureFormat.ARGB32, false);
////		PaintDungeonTexture(ref texOutput);
////		texOutput.filterMode = FilterMode.Point;
////		texOutput.wrapMode = TextureWrapMode.Clamp;
////		texOutput.Apply();
////		return texOutput;
////	}
////	
////	// Helper Methods
////	public bool IsEmpty(int row, int col) { return tiles[row,col].id == Tile.TILE_EMPTY; }
////	
////	public bool IsPassable(int row, int col) { return tiles[row,col].id == Tile.TILE_ROOM || tiles[row,col].id == Tile.TILE_CORRIDOR; }
////	
////	public bool IsPassable(XY xy) { return IsPassable((int) xy.y, (int) xy.x);}
////	
////	public void SetWall(int row, int col)
////	{
////		tiles[row,col].id = Tile.TILE_WALL;
////	}
////
////	// Dig a room, placing floor tiles
////	public void DigRoom(int row_bottom, int col_left, int row_top, int col_right)
////	{
////		// Out of range
////		if ( row_top < row_bottom ) 
////		{
////		    int tmp = row_top;
////		    row_top = row_bottom;
////		    row_bottom = tmp;
////		}
////		
////		// Out of range
////		if ( col_right < col_left ) 
////		{
////		    int tmp = col_right;
////		    col_right = col_left;
////		    col_left = tmp;
////		}
////		
////		if (row_top > MAP_HEIGHT-1) return;
////		if (row_bottom < 0) return;
////		if (col_right > MAP_WIDTH-1) return;
////		if (col_left < 0) return;
////		
////		// Dig floor
////	    for (int row = row_bottom; row <= row_top; row++) 
////	        for (int col = col_left; col <= col_right; col++) 
////	            DigRoom (row,col);
////	}
////	
////	public void DigRoom(int row, int col)
////	{
////		 tiles[row,col].id = Tile.TILE_ROOM;
////	}
////	
////	public void DigCorridor(int row, int col)
////	{
////		 tiles[row,col].id = Tile.TILE_CORRIDOR;
////	}
////	
////	public void DigCorridor(XY p1, XY p2)
////	{
////		int row1 = Mathf.RoundToInt(p1.y);
////		int row2 = Mathf.RoundToInt(p2.y);
////		int col1 = Mathf.RoundToInt(p1.x);
////		int col2 = Mathf.RoundToInt(p2.x);
////		
////		DigCorridor(row1,col1,row2,col2);
////	}
////	
////	public void DigCorridor(int row1, int col1, int row2, int col2)
////	{		
////		if (row1 <= row2)
////		{
////			for (int col = col1; col < col1 + CORRIDOR_WIDTH; col++)
////				for (int row = row1; row <= row2; row++)
////					DigCorridor(row,col);
////		}
////		else 
////		{
////			for (int col = col1; col < col1 + CORRIDOR_WIDTH; col++)
////				for (int row = row2; row <= row1; row++)
////					DigCorridor(row,col);
////		}
////		
////		if (col1 <= col2)
////		{
////			for (int row = row2; row < row2 + CORRIDOR_WIDTH; row++)
////				for (int col = col1; col <= col2; col++)
////					DigCorridor(row,col);
////		}
////		else 
////		{
////			for (int row = row2; row < row2 + CORRIDOR_WIDTH; row++)
////				for (int col = col2; col <= col1; col++)
////					DigCorridor(row2,col);
////		}
////	}
////	
////	// Export a texture to a file
////	public void TextureToFile(Texture2D t, string filename)
////	{
////		return;
////		byte[] bytes = t.EncodeToPNG();
////		FileStream myFile = new FileStream(Application.dataPath + "/Resources/Generated/" + filename + ".png",FileMode.OpenOrCreate,System.IO.FileAccess.ReadWrite);
////		myFile.Write(bytes,0,bytes.Length);
////		myFile.Close();
////	}
////	
////}
