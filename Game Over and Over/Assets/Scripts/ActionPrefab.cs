﻿using UnityEngine;
using System.Collections;
using FullInspector;

public class ActionPrefab<T> : BaseScriptableObject, IAction<T> {
	[SerializeField]
	public IAction<T> action;
	public void Invoke(T param1){
		action.Invoke(param1);
	}
	public bool IsSafeToInvoke{
		get{
			return action.IsSafeToInvoke;
		}
	}
}

public class OnSpawnPrefab : BaseScriptableObject, IAction<OnSpawnArgs> {
	[SerializeField]
	public IAction<OnSpawnArgs> action;
	public void Invoke(OnSpawnArgs param1){
		action.Invoke(param1);
	}
	public bool IsSafeToInvoke{
		get{
			return action.IsSafeToInvoke;
		}
	}
}
public class OnEnterPrefab : BaseScriptableObject, IAction<OnSpawnArgs> {
	[SerializeField]
	public IAction<OnSpawnArgs> action;
	public void Invoke(OnSpawnArgs param1){
		action.Invoke(param1);
	}
	public bool IsSafeToInvoke{
		get{
			return action.IsSafeToInvoke;
		}
	}
}
public class DoorEffect : IAction<OnEnterArgs> {
	[ShowInInspector]
	[SerializeField]
	LocationMutable destination = new LocationMutable();
	[SerializeField]
	void OnEnter(OnEnterArgs args){
		Save.LoadedSave.Add(Save.LoadedSave.CurrentT,Save.LoadedSave.CurrentStage,new GameEvent.SetLocation(args.movableInstance,args.movableInstance.GetLocation(),destination.Location,true));
	}
	public void Invoke(OnEnterArgs param1){
		OnEnter(param1);
	}
	public bool IsSafeToInvoke{
		get{
			if(destination.MapAsset == null) throw new System.Exception("DoorEffect cannot link to a location without a MapAsset");
			return true;
		}
	}
}