using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReadonlyDictionary<K,V> : IDictionary<K,V>{
	private readonly IDictionary<K,V> dict;

	public V this[K key] { 
		get{
			return dict[key];
		}
	}
	V IDictionary<K,V>.this[K key] { 
		
		get{
			return dict[key];
		}
		set{}
	}


	public ReadonlyDictionary(IDictionary<K,V> dict){
		this.dict = dict;
	}


	void IDictionary<K,V>.Add(K key, V value){
	}
	bool IDictionary<K,V>.Remove(K key){
		return false;
	}
	public int Count{
		get{
			return dict.Count;
		}
	}
	
	public bool IsReadOnly { 
		get{
			return true;
		}
	}
	
	public ICollection<K> Keys { 
		get{
			return dict.Keys;
		} 
	}
	
	public ICollection<V> Values { 
		get{
			return dict.Values;
		}
	}
	
	void ICollection<KeyValuePair<K,V>>.Add(KeyValuePair<K,V> item){
		((IDictionary<K,V>)this).Add(item.Key,item.Value);
	}


	void ICollection<KeyValuePair<K,V>>.Clear(){
		dict.Clear();
	}

	bool ICollection<KeyValuePair<K,V>>.Contains(KeyValuePair<K,V> item){
		return ((IDictionary)dict).Contains(item);
	}
	public bool ContainsKey(K key){
		return dict.ContainsKey(key);
	}
	
	void ICollection<KeyValuePair<K,V>>.CopyTo(KeyValuePair<K,V>[] array,int arrayIndex){
		
	}
	
	bool ICollection<KeyValuePair<K,V>>.Remove(KeyValuePair<K,V> item){
		return false;
	}
	
	public bool TryGetValue(K key, out V value){
		return dict.TryGetValue(key,out value);
	}
	
	public IEnumerator<System.Collections.Generic.KeyValuePair<K,V>> GetEnumerator(){
		return dict.GetEnumerator();
	}
	IEnumerator IEnumerable.GetEnumerator(){
		return null;
	}
}


/*
class TileMap : IDictionary<IntCoord,Tile>{
	private readonly Dictionary<IntCoord,Tile> tileMap = new Dictionary<IntCoord, Tile>();

	public Tile this[IntCoord key] { 
		get{
			return tileMap[key];
		} 
		set{
			Remove(key);
			Add(key,value);
		} 
	}

	public virtual void Add(IntCoord coord, Tile tile){
		if(this.ContainsKey(coord)) throw new System.ArgumentNullException("Key can't be null");
		if(this.ContainsKey(coord)) throw new System.ArgumentException("Key can't already Exists");
		this[coord] = tile;
	}
	public virtual bool Remove(IntCoord coord){
		if(this.ContainsKey(coord)){
			this[coord] = null;
			return true;
		}
		return false;
	}
	public void Remove(IEnumerable<IntCoord> coords){
		foreach(IntCoord coord in coords){
			this.Remove(coord);
		}
	}
	public void Add(IEnumerable<KeyValuePair<IntCoord,Tile>> map){
		foreach(KeyValuePair<IntCoord,Tile> pair in map){
			this.Add(pair.Key,pair.Value);
		}
	}
	public int Count{
		get{
			return tileMap.Count;
		}
	}
	
	public bool IsReadOnly { 
		get{
			return true;
		}
	}
	
	public ICollection<IntCoord> Keys { 
		get{
			return tileMap.Keys;
		} 
	}
	
	public ICollection<Tile> Values { 
		get{
			return tileMap.Values;
		}
	}
	
	void ICollection<KeyValuePair<IntCoord,Tile>>.Add(KeyValuePair<IntCoord,Tile> item){
		this.Add(item.Key,item.Value);
	}

	
	public void Clear(){
		tileMap.Clear();
	}
	
	bool ICollection<KeyValuePair<IntCoord,Tile>>.Contains(KeyValuePair<IntCoord,Tile> item){
		return ((IDictionary)tileMap).Contains(item);
	}
	public bool ContainsKey(IntCoord key){
		return tileMap.ContainsKey(key);
	}
	
	void ICollection<KeyValuePair<IntCoord,Tile>>.CopyTo(KeyValuePair<IntCoord,Tile>[] array,int arrayIndex){
		
	}
	
	bool ICollection<KeyValuePair<IntCoord,Tile>>.Remove(KeyValuePair<IntCoord,Tile> item){
		if(((IDictionary)tileMap).Contains(item)){
			return tileMap.Remove(item.Key);
		}
		return false;
	}
	
	public bool TryGetValue(IntCoord key, out Tile value){
		return tileMap.TryGetValue(key,out value);
	}
	
	public IEnumerator<System.Collections.Generic.KeyValuePair<IntCoord,Tile>> GetEnumerator(){
		return tileMap.GetEnumerator();
	}
	IEnumerator IEnumerable.GetEnumerator(){
		return null;
	}

}
*/
/*
public class Dungeon: IDictionary<IntCoord,Tile>{


	//Dictionary<Room,HashSet<IntCoord>> 
	//public readonly xSize;
	//public readonly ySize;

	Dictionary<IntCoord,Tile> map = new Dictionary<IntCoord,Tile>();
	
	Dictionary<IntCoord,ILocationDecorator> locationDecorators = new Dictionary<IntCoord,ILocationDecorator>();
	
	public void AddLocationDecorator(ILocationDecorator locationDecorator){
		if(locationDecorator.GetLocation()!=this){
			locationDecorator.SetLocation(this);
		}
	}
	public HashSet<EnemyInstance> enemyMap = new HashSet<EnemyInstance>();

	public GameObject DungeonGameObject;

	public class DungeonTile{
		public readonly RoomNode room;
		public Tile tile;
		public Dungeon Dungeon{
			get{ return room.Dungeon; }
		}
		public DungeonTile(){

		}
	}

	public RoomNode StartingRoomNode;


	public class RoomNode{
		public readonly Dungeon Dungeon;
		private RoomNode _parent;
		public RoomNode Parent{
			get{return _parent;}
			set{
				if(_parent!=null)_parent.children.Remove(this);
				if(value!=null && value.Parent!=null && !value.Parent.children.Contains(value)){
					value.Parent.children.Add(value);
				}
				_parent = value;
			}
		}
		public HashSet<RoomNode> Descendants{
			get{
				HashSet<RoomNode> descendants = new HashSet<RoomNode>(GetChildren());
				
				foreach(RoomNode node in children){
					descendants.UnionWith(node.Descendants);
				}
				return descendants;
			}
		}


		#region children
		private HashSet<RoomNode> children = new HashSet<RoomNode>();
		public HashSet<RoomNode> GetChildren(){
			return new HashSet<RoomNode>(children);
		}
		public bool AddChild(RoomNode child){
			if(!children.Contains(child)){
				child.Parent = this;
			}
			return children.Add(child);
		}
		public bool RemoveChild(RoomNode child){
			if(children.Contains(child)){
				child.Parent = null;
			}
			return children.Remove(child);
		}
		public bool ContainsChild(RoomNode child){
			return children.Contains(child);
		}
		#endregion children


		public Dictionary<IntCoord,RoomNode> connectedRooms;

		private ReadonlyDictionary<IntCoord,Tile> tileMap;
		public ReadonlyDictionary<IntCoord,Tile> TileMap{
			get{
				return tileMap;
			}
			set{
				tileMap = value;
				foreach(IntCoord coord in value.Keys){
					Tile.Door door = value[coord] as Tile.Door;
					if(door != null){

					}

				}

			}
		}

		public RoomNode(IDictionary<IntCoord,Tile> tileMap){
			this.tileMap = new ReadonlyDictionary<IntCoord, Tile>(tileMap);
		}
	}

	// Dungeon Parameters
	//public readonly int width = 128;
	//public readonly int height = 128;
	
	
	// Prefabs and Instance Management
	public GameObject containerRooms{
		get{
			return RoomRenderer.Inst.containerRooms;
		}
	}
	public GameObject prefabWall01{
		get{
			return RoomRenderer.Inst.prefabWall01;
		}
	}
	public GameObject prefabFloor01{
		get{
			return RoomRenderer.Inst.prefabFloor01;
		}
	}
	public GameObject meshCombiner{
		get{
			return RoomRenderer.Inst.meshCombiner;
		}
	}
	
	
	// The Random Seed
	public int seed = -1;
	
	// QuadTree for dungeon distribution
	//public QuadTree quadTree;

	public bool CanPlayerMoveHere(IntCoord coord){
		if(map[coord].IsPassableBy(null)){
			//Debug.Log("passible");
			return true;
		}
		//Debug.Log("unpassible");
		return false;
	}
	
	// Read tilemap and instantiate GameObjects
	public void GenerateGameObjects(){
		if(DungeonGameObject != null) GameObject.Destroy(DungeonGameObject);
		DungeonGameObject = new GameObject("Dungeon");
		foreach(IntCoord coord in Keys){
			Tile t = this[coord];
			
			GameObject prefab = null;
			if (t.IsFloor()||t.IsEntranceDoor() ||t.IsExitDoor()){
				prefab = prefabFloor01;
			}else if (t.IsWall()){
				prefab = prefabWall01;
			}
			if(prefab!=null){
				GameObject go = GameObject.Instantiate(prefab,new Vector3(coord.x,coord.y,0.0f),prefab.transform.rotation) as GameObject;
				go.transform.parent = DungeonGameObject.transform;
			}
		}
	}

	
	

	
	public int Count{
		get{
			return map.Count;
		}
	}
	
	public bool IsReadOnly { 
		get{
			return true;
		}
	}
	
	public Tile this[IntCoord key] { 
		get{
			return map[key];
		} 
		set{
			if(value == null) return;
			map[key] = value;
		} 
	}
	
	public ICollection<IntCoord> Keys { 
		get{
			return map.Keys;
		} 
	}
	
	public ICollection<Tile> Values { 
		get{
			return map.Values;
		}
	}
	
	void ICollection<KeyValuePair<IntCoord,Tile>>.Add(KeyValuePair<IntCoord,Tile> item){
		map.Add(item.Key,item.Value);
	}
	
	public void Add(IntCoord key,Tile value){
		if(value == null) return;
		map.Add(key, value);
	}
	
	public void Clear(){
		map.Clear();
	}
	
	bool ICollection<KeyValuePair<IntCoord,Tile>>.Contains(KeyValuePair<IntCoord,Tile> item){
		return ((IDictionary)map).Contains(item);
	}
	public bool ContainsKey(IntCoord key){
		return map.ContainsKey(key);
	}
	
	public void CopyTo(KeyValuePair<IntCoord,Tile>[] array,int arrayIndex){
		
	}
	
	bool ICollection<KeyValuePair<IntCoord,Tile>>.Remove(KeyValuePair<IntCoord,Tile> item){
		if(((IDictionary)map).Contains(item)){
			return map.Remove(item.Key);
		}
		return false;
	}
	
	public bool Remove(IntCoord key){
		return map.Remove(key);
	}
	
	public bool TryGetValue(IntCoord key, out Tile value){
		return map.TryGetValue(key,out value);
	}
	
	public IEnumerator<System.Collections.Generic.KeyValuePair<IntCoord,Tile>> GetEnumerator(){
		return null;
	}
	IEnumerator IEnumerable.GetEnumerator(){
		return null;
	}

}
*/