//
//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//
//public class DungeonGenerator_Beta: IDungeonGenerator{
//	static int roomSize = 5;
//	public RoomDesign[] StartingRooms{
//		get{
//			return _rooms[1].ToArray();
//		}
//	}
//	Dictionary<int,List<RoomDesign>> _rooms = UltraSimpleRoomDesign.Generator(5,new System.Random(1));
//	
//	public Dictionary<int,List<RoomDesign>> rooms{
//		get{
//			return _rooms;
//		}
//	}
//	
//	
//	class CoordInRoom{
//		public readonly HashSet<IntCoord> room;
//		public readonly IntCoord coord;
//
//		public CoordInRoom(HashSet<IntCoord> room,IntCoord coord){
//			this.coord = coord;
//			this.room = room;
//		}
//	}
//	public Dungeon GenerateDungeon (int numberOfRooms,int seed){
//		System.Random rand = new System.Random(seed);
//
//		/*
//		add a starting Room
//
//		add doors to door list
//		*/
//
//		var usedSlots = new HashSet<IntCoord>();
//		var unusedSlots = new List<KeyValuePair<CoordInRoom,IntCoord>>();
//		var rooms = new Dictionary<HashSet<IntCoord>, List<HashSet<IntCoord>>>();
//		var doors = new List<HashSet<IntCoord>>();
//
//		System.Action<HashSet<IntCoord>,HashSet<IntCoord>> AddRoom = delegate(HashSet<IntCoord> coords, HashSet<IntCoord> parent)
//		{
//
//			foreach(IntCoord coord in coords){
//				unusedSlots.RemoveAll(pair => pair.Value == coord);
//				usedSlots.Add(coord);
//				
//				foreach(IntCoord neigboringCoord in coord.Neighbors){
//					if(!usedSlots.Contains(neigboringCoord)){
//						unusedSlots.Add(new KeyValuePair<CoordInRoom, IntCoord>(new CoordInRoom(coords,coord),neigboringCoord));
//					}
//				}
//				if(!rooms.ContainsKey(coords)) rooms.Add(coords,new List<HashSet<IntCoord>>());
//				if(parent!=null){
//					rooms[parent].Add(coords);
//				}
//			}
//		};
//		HashSet<IntCoord> startingRoom = new HashSet<IntCoord>(){new IntCoord(0,0),};
//		AddRoom(startingRoom,null);
//		while(rooms.Count<numberOfRooms){
//			HashSet<IntCoord> newRoom = new HashSet<IntCoord>();
//			var next = unusedSlots[rand.Next(0,unusedSlots.Count)];
//			doors.Add(new HashSet<IntCoord>(){next.Key.coord, next.Value});
//			int i = rand.Next(0,rand.Next(0,numberOfRooms-doors.Count));
//			newRoom.Add(next.Value);
//			IntCoord possibleExtra = newRoom.ElementAt(rand.Next(0,newRoom.Count)).Neighbors[rand.Next(0,4)];
//			while(!usedSlots.Contains(possibleExtra)&& i>0){
//				newRoom.Add(possibleExtra);
//				possibleExtra = newRoom.ElementAt(rand.Next(0,newRoom.Count)).Neighbors[rand.Next(0,4)];
//				i--;
//			}
//
//
//
//			AddRoom(newRoom,next.Key.room);
//		}
//		
//		Dungeon dungeon = new Dungeon();
//
//		dungeon.StartingRoomNode = AddRoomAndDescendants(ref dungeon, ref rooms, ref doors, startingRoom);
//
//		List<IntCoord> floorTiles = new List<IntCoord>();
//		foreach(IntCoord coord in dungeon.Keys){
//			if(dungeon[coord].IsFloor() && !dungeon.StartingRoomNode.TileMap.ContainsKey(coord)){
//				floorTiles.Add(coord);
//			}
//		}
//		for(int i = 0;i<20;i++){
//			IntCoord coord = floorTiles[(rand.Next(0,floorTiles.Count))];
//			floorTiles.RemoveAll(v => v == coord);
//			//dungeon.enemyMap.Add(new GreyRat(new Location(dungeon,coord),rand.Next()));
//			dungeon[coord].OnEnter += delegate(object sender, Tile.TileEnterArgs args){
//				Save ls = Save.loadedSave;
//				bool allDead = true;
//				EnemyInstance[] enemies = dungeon.enemyMap.Where(e => e.GetLocation().coord ==coord).ToArray();
//				foreach(EnemyInstance enemy in enemies){
//					if(!enemy.IsDead){
//						allDead = false;
//						break;
//					}
//				}
//				
//				if(!allDead){
//					ls.Add(
//						ls.CurrentT,
//						new GameEvent.EnterCombatEvent(
//						ls.OnLifeNumber,
//						enemies,
//						true
//						)
//						);
//				}
//			};
//		}
//
//		return dungeon;
//	}
//
//
//	Dungeon.RoomNode AddRoomAndDescendants(ref Dungeon dungeon, ref Dictionary<HashSet<IntCoord>,List<HashSet<IntCoord>>> rooms,ref List<HashSet<IntCoord>> doors, HashSet<IntCoord> room){
//		int size = 5;
//		int middle = (size - 1)/2;
//
//		var roomMap = new Dictionary<IntCoord, Tile>();
//		foreach(IntCoord roomCoord in room){
//			bool hasNorthNeighbor = room.Contains(roomCoord.NorthNeighbor);
//			bool hasSouthNeighbor = room.Contains(roomCoord.SouthNeighbor);
//			bool hasEastNeighbor  = room.Contains(roomCoord.EastNeighbor);
//			bool hasWestNeighbor  = room.Contains(roomCoord.WestNeighbor);
//
//			/*bool extendsNorth = room.Contains(roomCoord.NorthNeighbor);
//			bool extendsSouth = room.Contains(roomCoord.SouthNeighbor);
//			bool extendsEast  = room.Contains(roomCoord.EastNeighbor);
//			bool extendsWest  = room.Contains(roomCoord.WestNeighbor);*/
//			
//			for(int _x=0; _x<size;_x++){
//				int x = _x+size*roomCoord.x-(middle+1);
//				
//				for(int _y=0; _y<size;_y++){
//					int y = _y+size*roomCoord.y-(middle+1);
//					
//					bool isSouthWall = (_y == 0);
//					bool isNorthWall = (_y == size-1);
//					bool isWestWall  = (_x == 0);
//					bool isEastWall  = (_x == size-1);
//					
//					Tile tile = null;
//					
//					if((isNorthWall||isSouthWall) && (isEastWall||isWestWall)){
//						tile = new Tile.Wall();
//					}else if(
//							(isNorthWall && !hasNorthNeighbor)
//							||(isSouthWall && !hasSouthNeighbor)
//							||(isEastWall && !hasEastNeighbor)
//							||(isWestWall && !hasWestNeighbor)
//						){
//						bool isDoor = false;
//
//
//						IntCoord? other = null;
//						if(isNorthWall && _x==middle){
//							other = roomCoord.NorthNeighbor;
//						}else if(isSouthWall && _x==middle){
//							other = roomCoord.SouthNeighbor;
//						}else if(isEastWall && _y==middle){
//							other = roomCoord.EastNeighbor;
//						}else if(isWestWall && _y==middle){
//							other = roomCoord.WestNeighbor;
//						}
//						if(other.HasValue){
//							foreach(HashSet<IntCoord> door in doors){
//								isDoor = door.Contains(roomCoord) && door.Contains(other.Value);
//								if(isDoor) break;
//							}
//						}
//						if(!isDoor){
//							tile = new Tile.Wall();
//						}else{
//							tile = new Tile.Floor();
//						}
//					}else{
//						tile = new Tile.Floor();
//					}
//					dungeon[new IntCoord(x,y)] = tile;
//					roomMap[new IntCoord(x,y)] = tile;
//				}
//			}
//		}
//		
//		Dungeon.RoomNode newRoom = new Dungeon.RoomNode(roomMap);
//		foreach(HashSet<IntCoord> childRoom in rooms[room]){
//			newRoom.AddChild( AddRoomAndDescendants(ref dungeon, ref rooms, ref doors, childRoom) );
//		}
//
//		return newRoom;
//	}
//}