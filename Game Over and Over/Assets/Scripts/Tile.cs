using UnityEngine;
using System;
using System.Collections;
/*
[System.Serializable]
public abstract class Tile : System.Object{

	public Rotation rotation;
	
	public class TileEnterArgs: EventArgs{
		public readonly IMovable entity;
		public TileEnterArgs(IMovable entity){
			this.entity = entity;
		}
	}
	public class TileExitArgs: EventArgs{
		public readonly IMovable entity;
		public TileExitArgs(IMovable entity){
			this.entity = entity;
		}
	}

	public abstract Color? Color{get;}
	public abstract bool IsPassableBy(object o);
	
	public event EventHandler<TileEnterArgs> OnEnter;
	public void TriggerOnEnter(IMovable entity){
		if(OnEnter != null) OnEnter(null,new Tile.TileEnterArgs(entity));
	}
	//public event EventHandler<int> OnPlayerStay;
	public event EventHandler<TileExitArgs> OnExit;
	public void TriggerOnExit(IMovable entity){
		if(OnExit != null) OnExit(null,new Tile.TileExitArgs(entity));
	}



	public bool IsExitDoor(){
		return Door.IsExitDoor(this);
	}
	public bool IsEntranceDoor(){
		return Door.IsEntranceDoor(this);
	}
	public bool IsDoor(){
		return Door.IsDoor(this);
	}
	public bool IsFloor(){
		Floor f = this as Floor;
		if(f == null) return false;
		return true;
	}
	public bool IsWall(){
		Wall w = this as Wall;
		if(w == null) return false;
		return true;
	}

	public Tile Clone(){
		return (Tile)this.MemberwiseClone();
	}



	[System.Serializable]
	public class Door:Tile{
		
		public override Color? Color{
			get{
				return new Color(.5f,.5f,.5f);
			}
		}
		
		public override bool IsPassableBy(object o){
			return true;
		}


		public enum Type{Standard,EntanceOnly,ExitOnly}
		public Type type;
		
		public Door(Rotation rotation){
			this.rotation = rotation;
		}
		public Door(){
		}

		public static bool IsDoor(Tile tile){
			return IsEntranceDoor(tile)|| IsExitDoor(tile);
		}
		public static bool IsEntranceDoor(Tile tile){
			Door d = tile as Door;
			if(d == null) return false;
			return d.type == Type.EntanceOnly ||d.type == Type.Standard;
		}
		public static bool IsExitDoor(Tile tile){
			Door d = tile as Door;
			if(d == null) return false;
			return d.type == Type.ExitOnly ||d.type == Type.Standard;
		}
	}
	[System.Serializable]
	public class Floor:Tile{
		
		public override Color? Color{
			get{
				return new Color(.5f,.5f,.5f);
			}
		}
		
		public override bool IsPassableBy(object o){
			return true;
		}
	}
	[System.Serializable]
	public class Wall:Tile{
		
		public override Color? Color{
			get{
				return new Color(1f,1f,1f);
			}
		}
		
		public override bool IsPassableBy(object o){
			return false;
		}
	}

}
*/



