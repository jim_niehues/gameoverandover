using System.Collections.Generic;
using FullInspector;
using UnityEngine;

public abstract class CombatantClass : BaseScriptableObject{

	public Color color;

	/*
	Resistances
		Ice: N
		Fire: N
		Phys: N
		Elec: N
		Bio: S
		Arc: N
*/
	
	//public Attack[] Attacks{get;protected set;}
	public Resistances baseResistance;
	
	
	[Resettable]
	public int MaxHP{get;protected set;}
	//protected int _exp;
	
	public string nameOverride = "";
	public string Name{
		get{
			if(nameOverride==null || nameOverride =="") return nameOverride;
			else return name;
		}

	}
	
	public virtual void OnSpawn(){
		
	}
	
	
	public abstract Combatant CreateCombatantInstance(Location location, int seed);
	



	protected virtual void OnDeath(){
		//if(OnDeathDelegate!=null) OnDeathDelegate();
	}



	protected virtual void OnDamageTaken(int damage){
		//if(OnDamageTakenDelegate!=null) OnDamageTakenDelegate(damage);
	}
	protected virtual void OnAttacked(){
		//if(OnAttackedDelegate!=null) OnAttackedDelegate();
	}
	protected virtual void Initialize(){
	}
	
	
	
	public virtual void OnTimeStep(decimal time){
		/*if(OnTimeStepDelegate!=null){
			OnTimeStepDelegate(time);
		}*/
	}
	
	
	
	/*

	*/

}


