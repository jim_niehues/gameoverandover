using UnityEngine;
using System.Collections;
using FullInspector;
 
public abstract class SpawnerPrefab<T>:BaseScriptableObject, ISpawns<T> where T:class{
	[ShowInInspector]
	[SerializeField]
	public ISpawns<T> spawner;
	
	T ISpawns<T>.Spawn(int seed){
		return spawner.Spawn (seed);
	}
	void ISpawns<T>.Populate(T spawn){
		spawner.Populate ((T)spawn);
	}
}

/*
public abstract class Spawer:ISpawns<object>
{
	public GameObject prefab;
	
	public Spawer(){
	}
	
	public Combatant Spawn(int seed){
		return new Combatant();
	}
	virtual ILocated ISpawns<object>.Spawn(int seed){
		return Spawn (seed);
	}
	virtual void ISpawns<object>.Populate(ILocated spawn){
		Populate ((Combatant)spawn);
	}
	public PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}*/

