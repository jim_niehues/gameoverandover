﻿using UnityEngine;
#if UNITY_EDITOR
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
[InitializeOnLoad]
#endif
public abstract class DelegateBase{
	#if UNITY_EDITOR && false
	static DelegateBase(){

		GenerateFiles();
	}

	public static void GenerateFiles(){
		Dictionary<string,StringBuilder> sBuilders = new Dictionary<string,StringBuilder>();
		Func<string, StringBuilder> GetStringBuilder = delegate(string path){ 
			if(!sBuilders.ContainsKey(path)){
				sBuilders.Add(path,new StringBuilder());
			}
			return sBuilders[path];
		};
		Func<string,int, string> GetTypes = delegate(string type,int i){ 
			StringBuilder _sb = new StringBuilder();
			if(i == 0){
				if(type=="Action" ) return "";
				if(type=="Func" ) return "<TResult>";
			}
			_sb.Append("<");
			for(int j=1;j<=i;j++){
				_sb.AppendFormat("TParam{0}",j);
				if(j!=i){
					_sb.Append(", ");
				}else{
					if(type == "Func"){
						_sb.Append(", TResult");
					}
				}
			}
			_sb.Append(">");
			return _sb.ToString();
		};
		Func<int, string> GetParameterTypesWithNames = delegate(int i){ 
			StringBuilder _sb = new StringBuilder();
			for(int j=1;j<=i;j++){
				_sb.AppendFormat("TParam{0} param{1}",j,j);
				if(j!=i){
					_sb.Append(", ");
				}
			}
			return _sb.ToString();
		};
		Func<int, string> GetParameterNames = delegate(int i){ 
			StringBuilder _sb = new StringBuilder();
			for(int j=1;j<=i;j++){
				_sb.AppendFormat("param{0}",j);
				if(j!=i){
					_sb.Append(", ");
				}
			}
			return _sb.ToString();
		};
		Func<string, string> GetReturnType = delegate(string type){ 
			if(type=="Action"){
				return "void";
			}else if(type=="Func"){
				return "TResult";
			}else{
				throw new Exception("Unknown type "+type);
			}
		};
		Func<string, string> GetReturnCast = delegate(string type){ 
			if(type=="Action"){
				return "";
			}else if(type=="Func"){
				return "return (TResult)";
			}else{
				throw new Exception("Unknown type "+type);
			}
		};
		string folder = Application.dataPath+"/EnhancedDelegates/";



		StringBuilder sb;
		foreach(string type in new string[]{"Action","Func"}){

			
			StringBuilder interfacesStringBuilder = GetStringBuilder(folder+"I"+type+".cs");
			//	StringBuilder wrappersStringBuilder = GetStringBuilder(folder+"Raw"+type+".cs");
			StringBuilder scriptedsStringBuilder = GetStringBuilder(folder+"Scripted"+type+".cs");
			StringBuilder serializedsStringBuilder = GetStringBuilder(folder+"Serialized"+type+".cs");
			scriptedsStringBuilder.AppendLine("using System;");
			serializedsStringBuilder.AppendLine("using FullInspector.Modules.SerializableDelegates;");

			for(int i = 0;i<=4;i++){
				//interfaces
				sb = interfacesStringBuilder;
				sb.AppendFormat("public interface I{0}{1}\n",type,GetTypes(type,i));
				sb.Append("{\n");
				sb.AppendFormat("\t{0} Invoke({1});\n",GetReturnType(type),GetParameterTypesWithNames(i));
				sb.Append("\tbool IsSafeToInvoke{get;}\n");
				sb.Append("}\n");
				//Scripteds
				/*
public class ScriptedFunc<TParam1, TResult> 
	: 
		ScriptedDelegate<Func<TParam1, TResult>>,
		IFunc<TParam1, TResult>
{
	public TResult Invoke(TParam1 param1) {
		return (TResult)DoInvoke(param1);
	}
}
*/
				sb = scriptedsStringBuilder;
				sb.AppendFormat("public class Scripted{0}{1}\n",type,GetTypes(type,i));
				sb.Append("\t:\n");
				sb.AppendFormat("\t\tScriptedDelegate<{0}{1}>,\n",type,GetTypes(type,i));
				sb.AppendFormat("\t\tI{0}{1}\n",type,GetTypes(type,i));

				sb.Append("{\n");
				//line 2
				sb.AppendFormat("\tpublic {0} Invoke({1})\n",GetReturnType(type),GetParameterTypesWithNames(i));
				sb.Append("\t{\n");
				sb.AppendFormat("\t\t{0}Invoke({1});\n",GetReturnCast(type),GetParameterNames(i));
				sb.Append("\t}\n");
				sb.Append("}\n");

				
				sb = serializedsStringBuilder;

				
				sb.AppendFormat("public class Serialized{0}{1}\n",type,GetTypes(type,i));
				sb.Append("\t:\n");
				sb.Append("\t\tBaseSerializedAction,\n");
				sb.AppendFormat("\t\tI{0}{1}\n",type,GetTypes(type,i));
				sb.Append("{\n");

				sb.AppendFormat("\tpublic {0} Invoke({1})\n",GetReturnType(type),GetParameterTypesWithNames(i));
				sb.Append("\t{\n");
				sb.AppendFormat("\t\t{0}DoInvoke({1});\n",GetReturnCast(type),GetParameterNames(i));
				sb.Append("\t}\n");

				sb.AppendFormat("\tpublic bool IsSafeToInvoke\n");
				sb.Append("\t{\n");
				sb.Append("\t\tget{\n");
				sb.AppendFormat("\t\t\treturn CanInvoke;\n");
				sb.Append("\t\t}\n");
				sb.Append("\t}\n");

				sb.Append("}\n");
				/*
				public class SerializedAction<TParam1> : BaseSerializedAction {
					public void Invoke(TParam1 param1) {
						DoInvoke(param1);
					}
				}*/
			}

			//Wrappers
			/*

//Wrapers
public class RawFunc<TResult> : IFunc<TResult>
{
	public Func<TResult> func;
	public TResult Invoke(){
		return func.Invoke();
	}
	public static implicit operator RawFunc<TResult>(Func<TResult> funcDelegate)
	{
		Func<TResult> _func = funcDelegate;
		return new RawFunc<TResult>(){func = _func};
	}
}
*/
			//end Wrappers
			//ScriptedDelegates


		
		}

		foreach(string key in sBuilders.Keys){
			string filePath = key.Replace('/',Path.DirectorySeparatorChar);
			Directory.CreateDirectory(Path.GetDirectoryName(filePath));
			File.WriteAllText(filePath,Regex.Replace(sBuilders[key].ToString(), @"\r\n|\n\r|\n|\r", "\r\n"));
		}
		AssetDatabase.Refresh();
	}


#endif


}


