using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using FullSerializer.Internal;
using FullInspector.Rotorz.ReorderableList;
using System.Reflection;
using System.Linq;
using System.Text;
#if UNITY_EDITOR
using UnityEditorInternal;
using UnityEditor;
using System.IO;
#endif

[InitializeOnLoad]
public class ScriptCatalog : BaseScriptableObject {
	
	public static string AssetPathAndName = "Assets/ScriptCatalog/ScriptCatalog.asset";
	public static string ProjectPath{
		get{ return Application.dataPath.Substring(0,Application.dataPath.Length - "Assets".Length);}
	}
	public static string ScriptFilePathAndNameFromScriptName(string name){
		return "Assets/ScriptCatalog/ScriptFiles/"+name+".cs";
	}


	static ScriptCatalog _main;
	public static bool abort = false;
	[HideInInspector]
	public bool isDirty = false;
	public static ScriptCatalog Main{
		get{
			if(_main!=null && _main.isDirty){
				_main.Save();
			}
			string assetPathAndName = ScriptCatalog.AssetPathAndName;
			if(abort) return null;
			_main = (ScriptCatalog)AssetDatabase.LoadAssetAtPath(assetPathAndName,typeof(ScriptCatalog));
			if(_main == null){
				Debug.LogWarning("ScriptCatalog not found. Creating new ScriptCatalog.");
				_main = ScriptableObject.CreateInstance<ScriptCatalog> ();
				AssetDatabase.CreateAsset (_main, assetPathAndName);
				_main.Save();
			}
			_main = (ScriptCatalog)AssetDatabase.LoadAssetAtPath(assetPathAndName,typeof(ScriptCatalog));

			if(_main == null){
				Debug.LogError("Loading of ScriptCatalog fails");
				abort = true;
			}
			return _main;
		}
	}
	static ScriptCatalog() {
		/*
		Main = Resources.LoadAssetAtPath<ScriptCatalog>("ScriptCatalog.asset");
		if(Main == null){
			Debug.LogWarning("ScriptCatalog not found. Creating new ScriptCatalog.");
			Main = ScriptableObject.CreateInstance<ScriptCatalog> ();
			Main.Save();
		}*/
	}
	/*
	[InspectorOrder(-100)]
	[InspectorButton]
	void Bake(){
		Save();
	}
	[InspectorOrder(-99)]
	[InspectorButton]
	void ClearAllScripts(){
		#region generated code file
		File.WriteAllText("Assets/ScriptCatalog/"+ +".cs", WrapWithClass("",""));
		//AssetDatabase.ImportAsset("Assets/Scripts.cs");
		#if UNITY_EDITOR
		AssetDatabase.Refresh();
		AssetDatabase.SaveAssets ();
		#endif
		
		#endregion
	}
*/

	
	[InspectorOrder(-100)]
	[InspectorButton]
	public void BuildAll(){
		foreach(string name in ScriptDataLookup.Keys){
			ScriptCatalog.Build(name);
		}
	}
	[InspectorOrder(-99)]
	[InspectorButton]
	public void WipeAll(){
		foreach(string name in ScriptDataLookup.Keys){
			ScriptCatalog.Wipe(name);
		}
	}


	[NotSerialized]
	private string _showOnlyMethodName;
	[ShowInInspector]
	[NotSerialized]
	[InspectorOrder(-1)]
	public string ShowOnlyMethod{
		get{
			return _showOnlyMethodName;
		}
		set{
			_showOnlyMethodName = value;
		}
	}
	[InspectorOrder(1)]
	[SerializeField]
	[InspectorCollectionRotorzFlags( Flags = ReorderableListFlags.HideAddButton | ReorderableListFlags.DisableReordering)]
	[ShowInInspector]
	public ScriptRecord[] filteredRecords{
		get{
			return GetFilteredScriptRecords(ShowOnlyMethod);
		}
		set{
			ScriptRecord[] oldRecords = GetFilteredScriptRecords(ShowOnlyMethod);
			HashSet<string> oldNames = new HashSet<string>();
			foreach(ScriptRecord oldRecord in oldRecords){
				oldNames.Add(oldRecord.Name);
			}
			foreach(ScriptRecord incomingRecord in value){
				oldNames.Remove(incomingRecord.Name);
			}
			foreach(string key in oldNames){
				ScriptDataLookup.Remove(key);
				GUIDsReferencedByLookup.Remove(key);
			}
		}
	}


	[HideInInspector]
	[SerializeField]
	Dictionary<string, ScriptData> ScriptDataLookup = new Dictionary<string, ScriptData>();
	[HideInInspector]
	[SerializeField]
	Dictionary<string, List<string>> GUIDsReferencedByLookup = new Dictionary<string, List<string>>();
	

	public List<string> GetGUIDsReferencedBy(string name){
		if(!GUIDsReferencedByLookup.ContainsKey(name)) return new List<string>();
		List<string> GUIDs = GUIDsReferencedByLookup[name];
		for(int i=0;i<GUIDs.Count;i++){
			try{
				string path = AssetDatabase.GUIDToAssetPath(GUIDs[i]);
				if(path != ScriptCatalog.AssetPathAndName){
					path = ProjectPath + path;
					if(!File.ReadAllText(path.Replace('/',Path.DirectorySeparatorChar)).Contains(name)){
						GUIDs.RemoveAt(i);
						i--; // use same i for next loop
						continue;
					}
				}
				
			}catch(Exception e){
				Debug.LogException(e);
			}
		}
		if(GUIDs.Count == 0){

		}
		GUIDsReferencedByLookup[name] = GUIDs;
		return GUIDsReferencedByLookup[name];
	}
	public string GetNameFromScriptData(ScriptData sd){
		foreach(KeyValuePair<string,ScriptData> pair in ScriptDataLookup){
			if(pair.Value == sd) return pair.Key;
		}
		return null;
	}

	public ScriptRecord[] GetFilteredScriptRecords(string filterString){
		if(filterString==null|| filterString =="") return ScriptRecords;
		return ScriptRecords.Where(r=>r.Name.Contains(filterString)).ToArray();
		
	}
	public ScriptRecord[] ScriptRecords{
		get{
			List<ScriptRecord> srs = new List<ScriptRecord>();
			foreach(string name in ScriptDataLookup.Keys){
				srs.Add(new ScriptRecord(){NameSetter=name});
			}
			return srs.ToArray();
		}
		set{
			HashSet<string> oldNames = new HashSet<string>(ScriptDataLookup.Keys);
			foreach(ScriptRecord incomingRecord in value){
				oldNames.Remove(incomingRecord.Name);
			}
			foreach(string key in oldNames){
				ScriptDataLookup.Remove(key);
				GUIDsReferencedByLookup.Remove(key);
			}


		}
	}
	
	public ScriptData GetScriptData(string name){
			return ScriptDataLookup[name];
	}
	public ScriptData GetScriptData(Script script,string name){
		if(ScriptDataLookup.ContainsKey(name)){
			ScriptData sd = ScriptDataLookup[name];
			if(sd.GetType() != script.GetNewScriptData().GetType()){
				throw new NotImplementedException();
				return null;
			}
			return sd;
		}else{
			return null;
		}
	}
	public void AddScriptData(Script script,Action<string> setNameDelegate, string name){
		if(name == "Undefined"){
			do{
				name = "Method_"+ Guid.NewGuid().ToString("N");
			}
			while(ScriptDataLookup.ContainsKey(name));
			setNameDelegate(name);
		}
		if(!ScriptDataLookup.ContainsKey(name)){
			ScriptDataLookup[name] = script.GetNewScriptData();
			script.Wipe();
		}else{
			ScriptData sd = ScriptDataLookup[name];
			if(sd.GetType() != script.GetNewScriptData().GetType()){
				throw new NotImplementedException();
			}
		}
	}
	public string AssetThatReferences(string name){
		List<string> guids = GetGUIDsReferencedBy(name);
		if(guids == null || guids.Count == 0) return "";
		else return guids[0];
	}
	public static void UpdateAssetsThatReference(string name){
		string dataPath = Application.dataPath;
		dataPath = dataPath.Substring(0,dataPath.Length - "Assets".Length);
		if(!ScriptCatalog.Main.GUIDsReferencedByLookup.ContainsKey(name)){
			ScriptCatalog.Main.GUIDsReferencedByLookup.Add(name,new List<string>());
		}
		ScriptCatalog.Main.GUIDsReferencedByLookup[name].Clear();
		foreach(string assetPath in AssetDatabase.GetAllAssetPaths()){
			string ext = Path.GetExtension(assetPath).ToLower();
			if(assetPath != ScriptCatalog.AssetPathAndName && (ext ==".asset"|| ext==".prefab")){
				try{
					string path = dataPath + assetPath;
					if(File.ReadAllText(path.Replace('/',Path.DirectorySeparatorChar)).Contains(name)){
						string guid = AssetDatabase.AssetPathToGUID(assetPath);
						ScriptCatalog.Main.GUIDsReferencedByLookup[name].Add(guid);
					}
					
				}catch{
					
				}
			}
		}
	}
	
	public class ScriptRecord{

		[HideInInspector]
		[SerializeField]
		string name;

		[HideInInspector]
		public string NameSetter{
			set{
				if(name==null || name==""){
					name = value;
				}else{
					throw new NotSupportedException();
				}
			}
		}
		[ShowInInspector]
		public string Name{
			get{
				return name;
			}
		}
		
		[ShowInInspector]
		public ScriptData Data{
			get{
				return ScriptCatalog.Main.GetScriptData(name);
			}
			set{
			}
		}
		[HideInInspector]
		List<string> referencedBy_GUIDs;
		[HideInInspector]
		List<string> ReferencedBy_GUIDs{
			get{
				if(referencedBy_GUIDs==null){
					referencedBy_GUIDs = ScriptCatalog.Main.GetGUIDsReferencedBy(name);
				}
				return referencedBy_GUIDs;
			}
		}
		[ShowInInspector]
		int AssetsReferencedBy{
			get{
				return ReferencedBy_GUIDs.Count;
			}
		}
		[InspectorButton]
		void UpdateAssetsReferencedBy(){
			ScriptCatalog.UpdateAssetsThatReference(Name);
		}
		/*
		[HideInInspector]
		[SerializeField]
		bool _orphaned;
		
		[ShowInInspector]
		bool PossiblyOrphaned{
			get{return _orphaned;}
		}
		//[InspectorButton]
		public void UpdateOrphanedStatus()
		{
			_orphaned = AssetThatReferencesThis == null;
		}*/
		[InspectorButton]
		public void SelectAssetThatReferencesThis(){
			UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(ScriptCatalog.Main.AssetThatReferences(name)), typeof(UnityEngine.Object));
			//Debug.Log(obj.name);
			Selection.activeObject = obj;
		}
		
		[InspectorOrder(-100)]
		[InspectorButton]
		public void Bake(){
			ScriptCatalog.Build(Name);
		}
		/*
		[InspectorOrder(-99)]
		[InspectorButton]
		public void Wipe(){
			ScriptCatalog.Wipe(Name);
		}*/
	}



	/*
	public void OpenScript()
	InternalEditorUtility.OpenFileAtLineExternal(@"C:\1.txt", 1);

	AssetDatabase.OpenAsset(Object,int) 
	*/
	/*
	public void Add(Script script,Action<string> setNameDelegate, Func<string> getNameDelegate){
		if(getNameDelegate() == "Undefined"){
			string name;
			do{
				name = "Method_"+ Guid.NewGuid().ToString("N");
			}
			while(scripts.ContainsKey(name));
			setNameDelegate(name);
			scripts[name] = script.GetNewScriptData();
		}else if(!scripts.ContainsKey(getNameDelegate())){
			scripts[getNameDelegate()] = script.GetNewScriptData();
		}
    	}*/



	/*Parse(){

	}*/
	/*
	public static string WrapWithClass(string usings, string methods){
		StringBuilder sb = new StringBuilder();
		sb.AppendLine("//=============================");
		sb.AppendLine("//      Generated File");
		sb.AppendLine("//=============================");
		sb.AppendLine("// Don't change ANYTHING");// other than what is inside of the \"//<Code>\" and \"//<EndCode>\" tags");
		sb.AppendLine("//=============================");
		sb.AppendLine(usings);
		sb.AppendLine("public static partial class Scripts{");
		sb.AppendLine(methods);
		sb.AppendLine("}");
		return sb.ToString();
	}
	public static string UsingSection(string name){
		StringBuilder sb = new StringBuilder();
		HashSet<string> usings = new HashSet<string>();
		usings.Add("System");
		ScriptData sd  = ScriptCatalog.Main.GetScriptData(name);
		foreach(string use in sd.Usings){
			if(!usings.Contains(use)){
				usings.Add(use);
			}
		}
		foreach(string usingStatment in usings){
			sb.Append("using ");
			sb.Append(usingStatment);
			sb.Append(";\n");
		}
		return sb.ToString();
	}
	public static string MethodSection(string name){
		StringBuilder sb = new StringBuilder();
		ScriptData sd  = ScriptCatalog.Main.GetScriptData(name);
		sb.Append("\n\tpublic static ");
		sb.Append(sd.ReturnType);
		sb.Append(" ");
		sb.Append(name);//method name
		sb.Append("(");
		bool firstParameter = true;
		foreach(Parameter p in sd.Args){
			if(firstParameter){
				firstParameter = false;
			}else{
				sb.Append(", ");
			}
			sb.Append(p.TypeName);
			sb.Append(" ");
			sb.Append(p.Name);
		}
		sb.Append("){\n");
		sb.Append("\t\t//<Code>\n");
		//sb.Append(pair.Key);
		//sb.Append(">\n");
		
		sb.AppendLine();
		
		sb.AppendLine(sd.Content);
		
		sb.AppendLine();
		
		sb.Append("\t\t//<EndCode>\n");
		//sb.Append(pair.Key);
		//sb.Append(">\n");
		
		sb.AppendLine("\t}");
		return sb.ToString();
	}*/
	public static void Build(string name){
		#if UNITY_EDITOR
		string filePath = ProjectPath+ScriptCatalog.ScriptFilePathAndNameFromScriptName(name);
		filePath.Replace('/',Path.DirectorySeparatorChar);
		Directory.CreateDirectory(Path.GetDirectoryName(filePath));
		File.WriteAllText(filePath, ScriptData.BuildCodeFromScriptData(ScriptCatalog.Main.GetScriptData(name)));
		//AssetDatabase.ImportAsset("Assets/Scripts.cs");
		AssetDatabase.Refresh();
		AssetDatabase.SaveAssets ();
		#endif
	}
	public static void Wipe(string name){
		#if UNITY_EDITOR
		string filePath = ProjectPath+ScriptCatalog.ScriptFilePathAndNameFromScriptName(name);
		filePath.Replace('/',Path.DirectorySeparatorChar);
		Directory.CreateDirectory(Path.GetDirectoryName(filePath));
		File.WriteAllText(filePath, "");
		//AssetDatabase.ImportAsset("Assets/Scripts.cs");
		AssetDatabase.Refresh();
		AssetDatabase.SaveAssets ();
		#endif
	}

	public void Save(){

		#if UNITY_EDITOR
		AssetDatabase.Refresh();
		AssetDatabase.SaveAssets ();
		#endif

	}
}









public abstract class ParameterCollection {
	[ShowInInspector]
	public abstract Type ReturnType{get;}

	[HideInInspector]
	[SerializeField]
	string[] argNames;

	[InspectorCollectionRotorzFlags( Flags = ReorderableListFlags.HideAddButton | ReorderableListFlags.DisableReordering | ReorderableListFlags.HideRemoveButtons)]
	[ShowInInspector]
	public Parameter[] Args{
		get{return ArgsGetter();}
		set{
		}
	}
	protected abstract Parameter[] ArgsGetter();

}
[Serializable]
public class ParameterCollection<T> : ParameterCollection{
	[HideInInspector]
	public override Type ReturnType{
		get{
			return typeof(T).GetMethod("Invoke").ReturnType;
		}
	}

	[HideInInspector]
	[SerializeField]
	string[] argNames;


	protected override Parameter[] ArgsGetter(){
		List<Parameter> args = new List<Parameter>();
		ParameterInfo[] _params = typeof(T).GetMethod("Invoke").GetParameters()/*.Where(item => item.IsRetval == false).ToArray()*/;
		if( argNames==null || argNames.Length != _params.Length){
			string[] names = argNames;
			argNames = new string[_params.Length];
			if(names!=null && names.Length>0){
				for(int i=0;i<argNames.Length&&i<names.Length;i++){
					argNames[i] = names[i];
				}
			}
		}
		
		for(int i=0;i<argNames.Length;i++){
			int ii = i;
			args.Add(
				new Parameter()
				{
					HiddenVariable_type = _params[i].ParameterType, 
					HiddenVariable_name = _params[i].Name,
					callback = delegate(string n){argNames[ii] = n;}
				}
			);
		}
		
		return args.ToArray();
	}
}
[Serializable]
public struct Parameter{
	[HideInInspector]
	public bool isReturnValue;
	bool notIsReturnValue{
		get{return !isReturnValue;}
	}
	[HideInInspector]
	public Action<string> callback;
	[HideInInspector]
	public Type HiddenVariable_type;
	[InspectorName("Type")][ShowInInspector]
	public string TypeName{
		get{return HiddenVariable_type.PrettyName();}
	}
	[HideInInspector]
	public  string HiddenVariable_name;
	[InspectorName("Name")][ShowInInspector][InspectorShowIfAttribute("notIsReturnValue")]
	public string Name{
		get{return HiddenVariable_name;}
		set{
			HiddenVariable_name = value;
			callback(value);
		}
	}
}
public static class TypeExtensions{
	
	public static string PrettyName(this System.Type type)
	{
		if(type==null) return "Type Not Specified";
		if (type.GetGenericArguments().Length == 0)
		{
			return type.Name;
		}
		var genericArguments = type.GetGenericArguments();
		var typeDefeninition = type.Name;
		var unmangledName = typeDefeninition.Substring(0, typeDefeninition.IndexOf("`"));
		return unmangledName + "<" + System.String.Join(",", genericArguments.Select<System.Type,System.String>(PrettyName).ToArray()) + ">";
	}
}
