﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;
using FullInspector;
using FullInspector.Rotorz.ReorderableList;

public abstract class ScriptData {
	protected string Name{
		get{
			return ScriptCatalog.Main.GetNameFromScriptData(this);
		}
	}
	[HideInInspector]
	[SerializeField]
	string[] _usings;
	[ShowInInspector]
	public string[] Usings{
		get{return _usings;} 
		set{_usings = value;}
	}
	[ShowInInspector]
	public abstract string ReturnType{get;}
	[ShowInInspector]
	[InspectorCollectionRotorzFlags( Flags = ReorderableListFlags.HideAddButton | ReorderableListFlags.DisableReordering | ReorderableListFlags.HideRemoveButtons)]
	public abstract Parameter[] Args{get;}
	
	[HideInInspector]
	[SerializeField]
	string _content;
	
	[InspectorAutoTextArea]
	[ShowInInspector]
	public string Content{
		get{return _content;}
		set{_content = value;}
	}
	/*void LoadFromCode(){
		string filePathAndName = ScriptCatalog.ProjectPath +ScriptCatalog.ScriptFilePathAndNameFromScriptName(Name);
		string code;
		try{
			code = File.ReadAllText();
		}catch(Exception e){
			Debug.LogException(new System.Exception("Failed to read script file at "+filePathAndName),e);
			return;
		}
		//parsing time
		try{
			List<string> parsedUsings = new List<string>();
			
			
			
			
			
			
			
		}catch{
			
		}
		
	}*/



	public static string BuildCodeFromScriptData(ScriptData sd){
		Parameter[] parameterArgs = sd.Args;
		KeyValuePair<string,string>[] args = new KeyValuePair<string, string>[parameterArgs.Length];
		for(int i=0;i<parameterArgs.Length;i++){
			args[i] = new KeyValuePair<string, string>(parameterArgs[i].Name,parameterArgs[i].TypeName);
		}
		return BuildCode(sd.Usings,sd.ReturnType,sd.Name, args, sd.Content);
	}

	public static string BuildCode(IEnumerable<string> usings,string returnType,string name, IEnumerable<KeyValuePair<string,string>> ArgNameAndTypeEnumerable,   string content){
		StringBuilder sb = new StringBuilder();
		sb.AppendLine("//=============================");
		sb.AppendLine("//      Generated File");
		sb.AppendLine("//=============================");
		sb.AppendLine("// Don't change ANYTHING");// other than what is inside of the \"//<Code>\" and \"//<EndCode>\" tags");
		sb.AppendLine("//=============================");
		/*
		HashSet<string> usings = new HashSet<string>();
		ScriptData sd  = ScriptCatalog.Main.GetScriptData(name);
		foreach(string use in sd.Usings){
			if(!usings.Contains(use)){
				usings.Add(use);
			}
		}*/
		foreach(string usingStatment in usings){
			sb.Append("using ");
			sb.Append(usingStatment);
			sb.Append(";\n");
		}
		sb.AppendLine("public static partial class Scripts{");
		sb.Append("\n\tpublic static ");
		sb.Append(returnType);
		sb.Append(" ");
		sb.Append(name);//method name
		sb.Append("(");
		bool firstParameter = true;
		foreach(KeyValuePair<string,string> p in ArgNameAndTypeEnumerable){
			if(firstParameter){
				firstParameter = false;
			}else{
				sb.Append(", ");
			}
			sb.Append(p.Value);//Arg Type
			sb.Append(" ");
			sb.Append(p.Key);//Arg name
		}
		sb.Append("){\n");
		sb.Append("\t\t//<Code>\n");
		//sb.Append(pair.Key);
		//sb.Append(">\n");
		
		sb.AppendLine();
		
		sb.AppendLine(content);
		
		sb.AppendLine();
		
		sb.Append("\t\t//<EndCode>\n");
		//sb.Append(pair.Key);
		//sb.Append(">\n");
		
		sb.AppendLine("\t}");//end method
		sb.AppendLine("}");//end class
		return sb.ToString();
	}
}
public class ScriptData<T> : ScriptData{
	[HideInInspector]
	[SerializeField]
	ParameterCollection<T> parameterCollection= new ParameterCollection<T>();
	public override string ReturnType{
		get{ return parameterCollection.ReturnType.PrettyName();}
	}
	public override Parameter[] Args{get{return parameterCollection.Args;}}
}
