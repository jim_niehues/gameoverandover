﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FullInspector;
using FullSerializer.Internal;
using FullInspector.Rotorz.ReorderableList;
using System.Reflection;
using System.Linq;
using System.Text;



public abstract class Script{
	
	public abstract string Name{get;}
	public abstract ScriptData GetNewScriptData();

	[ShowInInspector]
	public ScriptData ScriptData{
		get{ 
			return ScriptCatalog.Main.GetScriptData(this,Name);
		}
		set{}//causes inspector to Not make All fields in object readonly
	}
	
	bool ScriptDataIsNotNull{
		get{return ScriptData != null;}
	}


	[InspectorOrder(-100)]
	[InspectorButton]
	[InspectorShowIf("ScriptDataIsNotNull")]
	public void Bake(){
		ScriptCatalog.Build(Name);
	}
	[InspectorOrder(-99)]
	[InspectorButton]
	[InspectorShowIf("ScriptDataIsNotNull")]
	public void Wipe(){
		ScriptCatalog.Wipe(Name);
	}
	
}

public abstract class ScriptedDelegate<T>:Script{
	
	[HideInInspector]
	[SerializeField]
	string _name = "Undefined";
	[ShowInInspector]
	public override string Name{
		get{
			return _name;
		}
	}
	
	
	void SetName(string n){
		_name = n;
	}
	string GetName(){
		return _name;
	}

	bool ShowCreateNewButton {
		get{
			#if UNITY_EDITOR
			string[] guids = UnityEditor.Selection.assetGUIDs;
			return ScriptData == null && guids != null && guids.Length >0;
			#else
			return false;
			#endif
		}
	}
	[InspectorShowIf("ShowCreateNewButton")]
	[InspectorButton]
	void CreateNew(){
		#if UNITY_EDITOR
			string[] guids = UnityEditor.Selection.assetGUIDs;
		if(ScriptData == null && guids != null && guids.Length >0){
			
			ScriptCatalog.Main.AddScriptData(this,SetName,_name);
		}else{
			throw new System.Exception();
		}

		#else
		throw new System.Exception("This is meant for inspector use only");
		#endif
	}
	
	public override ScriptData GetNewScriptData(){
		return new ScriptData<T>();
	}
	
	protected object DoInvoke(params object[] parameters) {
		try{
			MethodInfo method = typeof(Scripts).GetFlattenedMethod(_name);
			if (method == null) {
				throw new InvalidOperationException("Unable to locate method Scripts." + _name);
			}
			
			return method.Invoke(null, parameters);
		}catch(Exception e){
			Debug.LogException(e);
			return null;
		}
	}
	public bool IsSafeToInvoke{
		get{
			return typeof(Scripts).GetFlattenedMethod(_name) != null;
		}
	}
}




/*

//Wrapers
public class RawFunc<TResult> : IFunc<TResult>
{
	public Func<TResult> func;
	public TResult Invoke(){
		return func.Invoke();
	}
	public static implicit operator RawFunc<TResult>(Func<TResult> funcDelegate)
	{
		Func<TResult> _func = funcDelegate;
		return new RawFunc<TResult>(){func = _func};
	}
}
*/
//end Wrapers

/*
public class ScriptedFunc<TParam1, TParam2, TParam3, TParam4, TParam5, TResult> : ScriptedDelegate<Func<TParam1, TParam2, TParam3, TParam4, TParam5, TResult>> {
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5) {
		return (TResult)DoInvoke(param1, param2, param3, param4, param5);
	}
}
public class ScriptedFunc<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TResult> : ScriptedDelegate<Func<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TResult>> {
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6) {
		return (TResult)DoInvoke(param1, param2, param3, param4, param5, param6);
	}
}
public class ScriptedFunc<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TResult> : ScriptedDelegate<Func<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TResult>> {
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6, TParam7 param7) {
		return (TResult)DoInvoke(param1, param2, param3, param4, param5, param6, param7);
	}
}
public class ScriptedFunc<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TParam8, TResult> : ScriptedDelegate<Func<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TParam8, TResult>> {
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6, TParam7 param7, TParam8 param8) {
		return (TResult)DoInvoke(param1, param2, param3, param4, param5, param6, param7, param8);
	}
}
public class ScriptedFunc<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TParam8, TParam9, TResult> : ScriptedDelegate<Func<TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7, TParam8, TParam9, TResult>> {
	public TResult Invoke(TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6, TParam7 param7, TParam8 param8, TParam9 param9) {
		return (TResult)DoInvoke(param1, param2, param3, param4, param5, param6, param7, param8, param9);
	}
}
*/
