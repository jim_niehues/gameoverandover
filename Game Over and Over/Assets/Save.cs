using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FullInspector;




public interface IHasObjectID{
}

public class Save : GameEventCollection{


	/*GameEventCollection gameEvents = new GameEventCollection();
	public void AddGameEvent(decimal time, GameEvent gameEvent){
		gameEvents.Add(time,GameEvent);
	}*/
	//public BattleCollection activeBattles = new BattleCollection();

	public HashSet<NPC_Combatant> NPC_Combatants = new HashSet<NPC_Combatant>();
	
	bool isLoading;
	event EventHandler _OnReset;
	public event EventHandler OnReset{
		add{
			if(isLoading){
				_OnReset += value;
			}
		}
		remove{
			if(isLoading){
				_OnReset -= value;
			}
		}
	}
	//TickHandler tickHandler;
	public static Save LoadedSave{
		get{
			return Game.Inst.currentSave;
		}
		set{
			Game.Inst.currentSave = value;
		}
	}
	//public static Character player;

	public int OnLifeNumber = 0;
	public decimal NextPlayerTurn=1;
	public int seed;


	[SerializeField]
	Dictionary<int,Guid> playerGUIDs = new Dictionary<int,Guid>();


	[HideInInspector]
	Location StartingLocation{
		get{
			return Game.Inst.StartingLocation.Location;
		}
	}

	//public event EventHandler OnTurnChange;
	public int EXP;
	public HashSet<Item> AvalableItems = new HashSet<Item>();
	public HashSet<Book> books = new HashSet<Book>();





	public Instance<Player> ActiveCharacter{
		get{
			if(playerGUIDs.ContainsKey(OnLifeNumber)){
				return Instance<Player>.Get(playerGUIDs[OnLifeNumber]);
			}else return null;
		}
	}

	/*
	each time increment
		pre User actions
		
		userinput
		post user actions
		if(
		npc attacks
		
		
	*/

	void StartNextLife(){
		LevelMap.UnloadAll();
		CurrentT = -1;
		OnLifeNumber++;
		NextPlayerTurn = 0;
		Debug.Log("Starting life # "+OnLifeNumber);
		
		this.Reset();
		ILocatedExtension.Clear();
		GameEvent.Spawn<Player> spawnEvent = new GameEvent.Spawn<Player>(new PlayerSpawner(OnLifeNumber,unchecked(seed+OnLifeNumber)),true);
		playerGUIDs.Add(OnLifeNumber,spawnEvent.IDOfSpawn);
		Add(-1,TurnStage.Tick, spawnEvent);
		Add(-1,TurnStage.Tick, new GameEvent.SetLocation(Instance<ILocated>.Get(spawnEvent.IDOfSpawn), Location.Null, StartingLocation, true));


		if(OnLifeNumber>=1){
			Debug.Log("Loading Map for Life # "+OnLifeNumber);
			LevelMap.SetActiveMap(StartingLocation.Asset);
		}

	}
	protected override IEnumerator Update ()
	{
		yield return null;
		if(OnLifeNumber==0){
			StartNextLife();
		}
		IEnumerator baseIEnumerator = base.Update ();
		while(baseIEnumerator.MoveNext()){
			yield return baseIEnumerator.Current;
		}
	}
	 

	/*

		if(phase == Phase.UserInput) return;



		while(nextAction<actions.Count&&CurrentT>actions[nextAction].t){
			actions[nextAction].Execute();
			nextAction++;
		}

		if(!waitingForInput){
			Debug.Log("FRAME: "+LastT*10);

			if( CurrentT>0){
				foreach(Enemy enemy in Enemy.AllEnemies){
					enemy.OnTimeStep(CurrentT);
				}
			}

			if(ActiveCharacter!=null){
				if(ActiveCharacter.Foes.Count>0||CombatView.Inst.enabled){
					bool allDead = true;
					foreach(Enemy enemy in ActiveCharacter.Foes){
						if(!enemy.IsDead){
							allDead = false;
							break;
						}
					}
					if(allDead){
						ActiveCharacter.ExitCombat(ActiveCharacter.Foes);
						Game.State = GameState.OutOfCombat;
					}else{
						Game.State = GameState.InCombat;
					}
				}
				Room.ActiveRoom = ActiveCharacter.Location.room;
			}

			if(nextAction>=actions.Count||CurrentT>=NextPlayerTurn){
				waitingForInput = true;
			}


			LastT = CurrentT;
			
			CurrentT+=.1m;
			//Debug.Log("T");
		}
	}
	*/
	public Save():base(){
		//Load();
	}
//	IDungeonGenerator dungeonGenerator = new DungeonGenerator_Beta();
	public void Load(){

		//tickHandler = new TickHandler(gameEvents);


		LoadedSave = this;

		_OnReset = null; 
		isLoading = true;
		//Deserialize Save
//		dungeon = dungeonGenerator.GenerateDungeon(30,0);
//		StartingLocation = new Location( dungeon, new IntCoord(0,0));
		//
		isLoading = false;

		Reset();
	}

	protected override void Reset(){
		//Characters = new Character[OnLifeNumber+1];
		//for(int i=1;i<=OnLifeNumber;i++){
		//	characters[i] = new Character(StartingLocation);
		//}
		//nextAction = 0;
		CurrentT = -1;
		NextPlayerTurn = 0;
		//LastT = 0;
		//Game.State = GameState.OutOfCombat;
		if(_OnReset!=null) _OnReset(this,new EventArgs());
		//ResettableExtensions.ResetAllValues();
		base.Reset();
	}
	
	public void OnPlayerDeath(){
		StartNextLife();
		//Add(0, new GameEvent.CharacterSpawnAtLocation(StartingLocation,true));
	}
	public override string ToString ()
	{
		StringBuilder sb = new StringBuilder();

		
		sb.AppendLine("<Save>");
		sb.AppendLine("\t<Characters>");
		/*foreach(Character c in Characters){
			sb.AppendLine("\t\t"+((c!=null)?c.name:"null"));
		}*/
		sb.AppendLine("\t</Characters>");
		
		sb.AppendLine("\t<Events>");
		/*foreach(decimal time in baseCollection.Keys){

			PerTurnCollection ptc = baseCollection[time];
			if(ptc.FirstEvent !=null){
				
				sb.AppendLine("\t\t<Time of "+time+">");
				GameEvent currentEvent = ptc.FirstEvent;
				while(currentEvent!=null){
					sb.AppendLine("\t\t\t"+currentEvent.ToString());
					currentEvent = currentEvent.Next;
				}
				sb.AppendLine("\t\t</Time>");
			}
		}*/
		sb.AppendLine("\t</Events>");
		sb.AppendLine("</Save>");

		return sb.ToString();
	}
}
