using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FullInspector;

public abstract class CombatantSpawnerBase:ISpawns<Combatant>,ISpawns<ILocated>{

	public abstract Combatant Spawn(int seed);
	public abstract void Populate(Combatant spawn);



	ILocated ISpawns<ILocated>.Spawn(int seed){
		return Spawn (seed);
	}
	void ISpawns<ILocated>.Populate(ILocated spawn){
		Populate ((Combatant)spawn);
	}
	public PseudoEvent<OnSpawnArgs> OnSpawn = new PseudoEvent<OnSpawnArgs>();
}

public class CombatantSpawner:CombatantSpawnerBase{
	public int hp;
	public GameObject prefab;
	public Resistances baseResistances = new Resistances();
	public readonly TaggedPseudoEvent<OnTurnArgs> OnTurnStartEvent = new TaggedPseudoEvent<OnTurnArgs>();
	public IFunc<OnTurnArgs,IEnumerator> OnTurnAction;
	public readonly TaggedPseudoEvent<OnTurnArgs> OnTurnEndEvent = new TaggedPseudoEvent<OnTurnArgs>();

	public override Combatant Spawn(int seed){
		return new Combatant();
	}
	public override void Populate(Combatant spawn){
		//if(spawn.gameobjectreference == null) spawn.gameobjectreference = new GameObjectReference();
		spawn.gameobjectreference.prefab = prefab;
		spawn.baseResistances = baseResistances;
		spawn.OnTurnStartEvent.AddRange(OnTurnStartEvent);

		spawn.OnTurnAction = OnTurnAction;
		spawn.OnTurnEndEvent.AddRange(OnTurnEndEvent);

	}
}
public class CombatantSpawnerAuxiliary:CombatantSpawnerBase{

	public CombatantSpawnerBase baseSpawner;

	public override Combatant Spawn(int seed){
		return baseSpawner.Spawn(seed);
	}
	public override void Populate(Combatant spawn){
		baseSpawner.Populate(spawn);
		OnSpawn.InsertRange(0,baseSpawner.OnSpawn);
	}

}
public class Combatant: ILocated, IDamageable, IHasGameObject{
	
	public readonly List<Action> DoWhenWarmupComplete = new List<Action>();

	public readonly Dictionary<object,object> data = new Dictionary<object, object>();

	public decimal last_action_time;
	public decimal next_action_time;


	//public readonly CombatantClass baseClass;
	//public event Action<LocationChangedArgs> OnLocationChanged;
	private int _RNG_Seed;
	protected System.Random rand;
	public int RNG_Seed{
		get{
			return _RNG_Seed;
		}
		set{
			_RNG_Seed = value;
			rand = new System.Random(value);
		}
	}


	public string NameOverride;

	public string name; 
	public string Name{
		get{
			if(NameOverride==null || NameOverride==""){
				return name;
			}else return NameOverride;
		}
	}


	//Dictionary<EquipedEffect,Equipment> ActiveEquipedEffects = new Dictionary<EquipedEffect, Equipment>();
	
	

	
	public int _hp = 1;
	
	
	protected bool dead;
	public virtual bool IsDead{
		get{
			return (HP == 0);
		}
	}
	public virtual Location Location{
		get{
			return Instance<ILocated>.Get(this).GetLocation();
		}
		set{
			Instance<ILocated>.Get(this).MoveTo(value);
		}
	}
	//public int baselineHP
	public virtual int HP{
		get{
			return _hp;
		}
		protected set{
			if(!dead||value>0){
				if(value<=0){
					_hp = 0;
					dead = true;
					UpdateGameObject();
					OnDeath(new OnDeathArgs(Instance<Combatant>.Get(this),null));
				}else {
					_hp = value;
					if(dead){
						dead = false;
						UpdateGameObject();
					}
				}
			}
		}
	}
	public Resistances baseResistances = new Resistances();
	public virtual Resistances GetResistances(){
		return baseResistances;
		
	}

	public virtual void OnAttacked(OnAttackedArgs args){
		OnAttackedEvent.InvokeAll(args);
	}
	
	public virtual void OnDamageTaken(OnDamageTakenArgs args){
		OnDamageTakenEvent.InvokeAll(args);
	}
	public virtual void OnDeath(OnDeathArgs args){
		OnDeathEvent.InvokeAll(args);
	}

	protected int turnNumber = 1;
	protected int turnSeed{
		get{
			return unchecked( _RNG_Seed + turnNumber);
		}
	}
	public bool IsTurn(decimal time){
		return WarmUp==0 && CoolDown == 0;
	}
	public virtual bool IsActiveCharacter(){
		return false;
	}
	public virtual void OnTimeStep(OnTimeStepArgs args){
		OnTimeStepEvent.InvokeAll(args);
		decimal time = args.time;
		//reset last_action_time if 
		if(last_action_time>time){
			last_action_time = 0;
		}
		
		if(IsDead) return; // the dead can't attack
		/*
		string allOpposition = "";
		foreach(Combatant c in OpposingCombatants){
			allOpposition += c.name +"\n";
		}
		Debug.Log(allOpposition);
		*/
	}

	[ShowInInspector]
	[SerializeField]
	public readonly GameObjectReference gameobjectreference = new GameObjectReference();
	public GameObject GetGameObject(){
		return gameobjectreference.GetGameObject();
	}
	public GameObject GetOrCreateGameObject(){
		return gameobjectreference.GetOrCreateGameObject(this.Location.WorldPosition,Quaternion.identity);
	}
	public virtual void UpdateGameObject(){
		GameObject go = GetGameObject();
		if(go != null){
			Renderer[] rends = go.GetComponentsInChildren<Renderer>();
			foreach(Renderer rend in rends){
				foreach(Material mat in rend.materials){
					if(dead){
						mat.color = new Color(.5f,.5f,.5f,.5f);
					}else{
						mat.color = Color.white;
					}
				}
			}
		}
	}
	
	public PseudoEvent<OnDeathArgs> OnDeathEvent = new PseudoEvent<OnDeathArgs>();
	public PseudoEvent<OnAttackedArgs> OnAttackedEvent = new PseudoEvent<OnAttackedArgs>();
	public PseudoEvent<OnDamageTakenArgs> OnDamageTakenEvent = new PseudoEvent<OnDamageTakenArgs>();
	public PseudoEvent<OnTimeStepArgs> OnTimeStepEvent = new PseudoEvent<OnTimeStepArgs>();
	//public PseudoEvent<OnTimeStepArgs> OnModResistancesEvent;
	public readonly TaggedPseudoEvent<OnTurnArgs> OnTurnStartEvent = new TaggedPseudoEvent<OnTurnArgs>();
	public IFunc<OnTurnArgs,IEnumerator> OnTurnAction;
	public readonly TaggedPseudoEvent<OnTurnArgs> OnTurnEndEvent = new TaggedPseudoEvent<OnTurnArgs>();

	//protected event Action OnAttackDelegate;
	
	public virtual void OnTurnStart(decimal currentTime){
		OnTurnArgs args = new OnTurnArgs(currentTime,turnNumber,Instance.Get<Combatant>(this),turnSeed);
		OnTurnStartEvent.InvokeAll(args);
	}
	public virtual IEnumerator OnTurn(decimal currentTime){
		int limit = 0;
		OnTurnArgs args = new OnTurnArgs(currentTime,turnNumber,Instance.Get<Combatant>(this),unchecked(turnSeed+100*limit));
		while(next_action_time<=Save.LoadedSave.CurrentT){

			last_action_time = next_action_time;

				if(OnTurnAction.IsSafeToInvoke){
					OnTurnAction.Invoke(args);
				}else Debug.LogError("null OnTurnAction");
			//Debug.Log(this.GetType().FullName+ "   " + last_action_time);
			if(limit>100){
				CoolDown=decimal.MaxValue;
				Debug.LogError("Cooldown seems to always be 0");
				break;
			}
			if(WarmUp==0 && CoolDown == 0){
				limit++;
			}
		}
		turnNumber++;
		OnTurnStartEvent.InvokeAll(args);
		if(OnTurnAction.IsSafeToInvoke){
			OnTurnAction.Invoke(args);
		}else Debug.LogError("null OnTurnAction");
		return null;
	}
	
		
	public virtual void OnTurnEnd(decimal currentTime){
		OnTurnArgs args = new OnTurnArgs(currentTime,turnNumber,Instance.Get<Combatant>(this),turnSeed);
		OnTurnEndEvent.InvokeAll(args);
	}

	
	[HideInInspector]
	public HashSet<Combatant> _opposingCombatants = new HashSet<Combatant>();
	
	public HashSet<Combatant> OpposingCombatants{
		get{
			//clean out dead opponents
			_opposingCombatants.RemoveWhere( opponent => opponent.IsDead == true);
			return _opposingCombatants;
		}
		set{
			_opposingCombatants = value;
		}
	}
	
	public void EnterCombat( Combatant b){

		if(this.IsActiveCharacter() || b.IsActiveCharacter()){
			Game.Inst.GameStateStack.Push(new GameState.Combat());
		}
		if(!this.OpposingCombatants.Contains(b)){
			this.OpposingCombatants.Add(b);
		}
		if(!b.OpposingCombatants.Contains(this)){
			b.OpposingCombatants.Add(this);
		}
	}
	
	public void EnterCombat(IEnumerable<Combatant> b){
		Combatant[] combatants = b.ToArray();
		foreach(Combatant combatant in combatants){
			this.EnterCombat(combatant);
		}
	}
	
	public void ExitCombat( Combatant b){
		Debug.Log(this.Name +" and "+b.Name +" are no longer fighting");
		if(this.OpposingCombatants.Contains(b)){
			this.OpposingCombatants.Remove(b);
		}
		if(b.OpposingCombatants.Contains(this)){
			b.OpposingCombatants.Remove(this);
		}
	}
	
	public void ExitCombat(IEnumerable<Combatant> b){
		Combatant[] combatants = b.ToArray();
		foreach(Combatant combatant in combatants){
			this.ExitCombat(combatant);
		}
	}
	
	
	
	public HashSet<Combatant> Allies{
		get{
			HashSet<Combatant> allies = new HashSet<Combatant>();
			foreach(Combatant opposition in OpposingCombatants){
				allies.UnionWith(opposition.OpposingCombatants);
			}
			return allies;
		}
	}
	public HashSet<Combatant> Foes{
		get{
			return OpposingCombatants;
		}
	}
	/*public Location Location{
		get{
			return null;
		}
	}*/
	/*public CombatantInstance(Location location, int hp, Resistances baseResistance, int seed){
		this.rand = new System.Random(seed);
		this.MaxHP = maxHP;
		this._hp = maxHP;
		this.baseResistance = baseResistance;
		this.MoveTo(location);
		this.SetResetValues();
	}*/
	


	//IFunc<CombatantInstance,Resistances,Resistances> ModResistnces
	
	public void TakeAttack(OnAttackedArgs args){
		OnAttacked(args);
		AttackDamage attack = args.attackDamage;
		attack = GetResistances().ApplyTo(attack);
		int totalDamage=0;
		foreach(int damage in attack.Values){
			totalDamage += damage;
		}
		Debug.Log(this.Name +" took "+totalDamage +" damage");
		HP-=totalDamage;
		if(totalDamage>0) OnDamageTaken(new OnDamageTakenArgs(args.combatant,args.attacker,attack));
	}
	
	public decimal TimeTillNextTurn{
		get{ 
			return _warmUp + _coolDown;
		}
	}
	private decimal _warmUp;
	public decimal WarmUp{
		get{ 
			return _warmUp;
		}
		set{
			if(value<0){
				Debug.LogError( ""+this.GetType().Name+": Set WarmUp: value cannot be less than 0");
				value = 0;
			}
			_warmUp = value;
		}
	}
	private decimal _coolDown;
	public decimal CoolDown{
		get{ 
			return _coolDown;
		}
		set{
			if(value<0){
				Debug.LogError( ""+this.GetType().Name+": Set CoolDown: value cannot be less than 0");
				value = 0;
			}
			
			_coolDown = value;
		}
	}
	public virtual bool IsHostileTo(Combatant combatant){
		if(this == combatant) return false;
		return true;
	}
	
	private Battle _Battle = null;
	public Battle Battle{
		get{
			return _Battle;
		}
		internal set{
			_Battle = value;
		}
	}

	public static bool AllDead(IEnumerable<Combatant> value){
		foreach(Combatant c in value){
			if(!c.IsDead) return false;
		}
		return true;
	}
}
public static class CombatantInstanceExtensions{
	public static bool AllDead(this IEnumerable<Combatant> value){
		foreach(Combatant c in value){
			if(!c.IsDead) return false;
		}
		return true;
	}
}