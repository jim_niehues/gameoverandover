using UnityEngine;
using System.Collections;
using System;
[System.Serializable]
public class CombatantInstanceConstructor : IEntityOnTile{

	public Color? ColorOverride = null;
	public Color Color{
		get{
			if(ColorOverride.HasValue) return ColorOverride.Value;
			else return combatantClass.color;
		}
	}

	[HideInInspector]
	[SerializeField]
	int seed = UnityEngine.Random.Range(int.MinValue,int.MaxValue);
	int Seed{
		get{
			unchecked{ return Save.LoadedSave.seed + seed; }
		}
	}
	public CombatantClass combatantClass;
	public Location location;

	public IAction<Combatant> CustomModifications;

	public Combatant Construct(Location location){
		Combatant instance = combatantClass.CreateCombatantInstance(location,Seed);
		if(CustomModifications != null){
			//CustomModifications

		}
		return instance;
	}
}
